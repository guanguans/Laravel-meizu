<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //创建表
		Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->char('name', 255);
            $table->char('password', 255);
            $table->char('email', 255);
            $table->char('user_img', 255)->default('default.jpg');
            $table->integer('user_type')->default(2);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //删除表
		Schema::drop('user');
    }
}
