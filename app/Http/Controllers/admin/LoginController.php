<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Gregwar\Captcha\CaptchaBuilder;//使用验证码的类
use DB;

class LoginController extends Controller
{
    //
    public function login ()
    {
    	return view('admin.login');
    }

    public function dologin (Request $request)
    {
    	$code = session()->get('code');

    	if ($code != $request->input('code')) {

    		return back()->with('message','验证码错误');
    	}

    	$name = $request->input('name');
    	$password = $request->input('password');

    	$db = DB::table('user')->where('name','=',$name)->first();

    	if ($db) {
    		if ($db->password == $password) {

    			session()->set('adminuser',$db);
    			return redirect('admin/index');
    		} 
    		return back()->with('message','用户名或密码错误');
    	}
    	return back()->with('message','用户名或密码错误');
    }

    public function captcha($tmp)
    {

    	//生成验证码图片的Builder对象，配置相应属性
        $builder = new CaptchaBuilder;
        //可以设置图片宽高及字体
        $builder->build($width = 200, $height = 50, $font = null);
        //获取验证码的内容
        $phrase = $builder->getPhrase();

        //把内容存入session
        // Session::flash('code', $phrase); //类调用方法
        session()->flash('code',$phrase);

        return response($builder->output())->header('Content-Type','image/jpeg');
    }

    public function logout ()
    {
    	session()->forget('adminuser');

    	return redirect('admin/login');
    }

}
