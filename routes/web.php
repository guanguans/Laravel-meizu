<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('home.index');
});

Route::get('/admin/login','admin\LoginController@login');
Route::post('/admin/dologin','admin\LoginController@dologin');
Route::get('/admin/captcha/{tmp}','admin\LoginController@captcha');

Route::group(["prefix"=>"admin",'middleware'=>'myauth'],function(){
		Route::get("index","Admin\IndexController@index");//后台首页
		Route::get("logout","Admin\LoginController@logout");//执行退出

		Route::resource('user','admin\UserController');
	});


