@extends('admin.base')

@section('content')
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    用户列表
                </h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="dataTables_wrapper form-inline dt-bootstrap" id="example1_wrapper">
                    <div class="row">
                        <div class="col-sm-6">
                            <div id="example1_length" class="dataTables_length">
                                <label>请选择 <select class="form-control input-sm" aria-controls="example1" name="example1_length">
                                    <option value="10">
                                        男
                                    </option>
                                    <option value="25">
                                        女
                                    </option>
                                    
                                </select>性别</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_filter" id="example1_filter">
                                <label>搜索:<input aria-controls="example1" placeholder="" class="form-control input-sm" type="search"></label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table aria-describedby="example1_info" role="grid" id="example1" class="table table-bordered table-striped dataTable">
                                <thead>
                                    <tr role="row">
                                        <th aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" style="width: 181px;" colspan="1" rowspan="1" aria-controls="example1" tabindex="0" class="sorting_asc text-center" >
                                            编号
                                        </th>
                                        <th aria-label="Browser: activate to sort column ascending" style="width: 224px;" colspan="1" rowspan="1" aria-controls="example1" tabindex="0" class="sorting text-center" align='center'>
                                            用户名
                                        </th>
                                        <th aria-label="Platform(s): activate to sort column ascending" style="width: 198px;" colspan="1" rowspan="1" aria-controls="example1" tabindex="0" class="sorting text-center" align='center'>
                                           	邮箱
                                        </th>
                                        <th aria-label="Engine version: activate to sort column ascending" style="width: 155px;" colspan="1" rowspan="1" aria-controls="example1" tabindex="0" class="sorting text-center" align='center'>
                                            级别
                                        </th>
                                        <th aria-label="CSS grade: activate to sort column ascending" style="width: 110px;" colspan="1" rowspan="1" aria-controls="example1" tabindex="0" class="sorting text-center" align='center'>
                                            操作
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                @foreach( $user as $users)
                                    <tr class="odd" role="row">
                               
                                        <td class="sorting_1" align='center'>
                                            {{ $users->id }}
                                        </td>
                                        <td align='center'>
                                            {{ $users->name }}
                                        </td>
                                        <td align='center'>
                                            {{ $users->email }}
                                        </td>
                                        <td align='center'>
                                             {{ $users->user_type }}
                                        </td>
                                        <td align='center'>
                                            <a href='#' class='btn-warning'>修改</a>|
                                            <a href='#' class='btn-danger'>删除</a>
                                        </td>
                                     
                                    </tr>
								@endforeach
                                </tbody>

                            </table>
                        </div>
                        {!! $user->render() !!}
                    </div>
					

					


                    </div>
                </div>
            </div><!-- /.box-body -->
        </div>
@endsection


