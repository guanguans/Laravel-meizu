<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>后台管理系统</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ asset('admins/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('admins/dist/css/AdminLTE.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('admins/plugins/iCheck/square/blue.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page" style="background-color:#1c77ac; background-image:url(1475942130357.jpg); background-repeat:no-repeat; background-position:center top; overflow:hidden;">
    <div class="login-box">
      <div class="login-logo">
        <a href="../../index2.html"><b>后台管理系统</b></a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        @if(session('message'))
          <p class="login-box-msg" style='color:red'>{{  session('message')  }}</p>
        @else
           <p class="login-box-msg">提示信息</p>
        @endif
        <form action="{{ URL('admin/dologin') }}" method="post">
          <input type='hidden' name='_token' value='{{ csrf_token() }}'>
          <div class="form-group has-feedback">
            <input type="text" name='name'class="form-control" placeholder="用户名">
            
          </div>
          <div class="form-group has-feedback">
            <input type="password" name='password' class="form-control" placeholder="密码">
           
          </div>
          <div class="row">
		  
            <div class="form-group has-feedback col-xs-6 ">
				<input type="text" class="form-control" placeholder="验证码" name='code'>
      
			</div>
			<div class="form-group has-feedback col-xs-6 ">
				<img src='{{ URL('admin/captcha/time()') }}' height='35' style='float:right;' onclick="this.src='{{ URL('admin/captcha') }}/'+Math.random()">
			</div>

            <!-- /.col -->
            <div class="col-xs-4" style='float:left;margin-left:40px;'>
              <button type="submit" class="btn btn-primary btn-block btn-flat" >登录</button>
            </div><!-- /.col -->
			<div class="col-xs-4" style='float:right;margin-right:40px;'>
              <button type="reset" class="btn btn-primary btn-block btn-flat" >重置</button>
            </div><!-- /.col -->
            
          </div>
        </form>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="{{ asset('admins/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{ asset('admins/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ asset('admins/plugins/iCheck/icheck.min.js') }}"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
