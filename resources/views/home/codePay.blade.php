<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<meta charset="gb2312">
<meta name="keywords" content="">
<meta name="description" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>支付宝 - 网上支付 安全快速！</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link href="https://a.alipayobjects.com/" rel="dns-prefetch">
<link href="https://app.alipay.com/" rel="dns-prefetch">
<link href="https://my.alipay.com/" rel="dns-prefetch">
<link href="https://lab.alipay.com/" rel="dns-prefetch">
<link href="https://cashier.alipay.com/" rel="dns-prefetch">
<link href="https://financeprod.alipay.com/" rel="dns-prefetch">
<link href="https://shenghuo.alipay.com/" rel="dns-prefetch">


<!-- uitpl:/component/trackerTime.vm -->
    <!-- CMS:全站公共 cms/tracker/iconfont.vm开始:tracker/iconfont.vm -->
<!-- CMS:全站公共 cms/tracker/iconfont.vm结束:tracker/iconfont.vm -->


<!-- CMS:全站公共 cms/tracker/monitor.vm开始:tracker/monitor.vm --><!-- FD:106:alipay/tracker/sai_seer.vm:START -->
<!-- FD:106:alipay/tracker/sai_seer.vm:END --><!-- CMS:全站公共 cms/tracker/monitor.vm结束:tracker/monitor.vm -->
<!-- CMS:全站公共 cms/tracker/seajs.vm开始:tracker/seajs.vm -->	

<!-- monitor 防错代码 -->


<!-- seajs以及插件 -->


<!-- seajs config 配置 -->


<!-- 兼容原有的 plugin-i18n 写法 -->
<!-- https://github.com/seajs/seajs/blob/1.3.1/src/plugins/plugin-i18n.js -->


<!-- 路由旧 ID，解决 seajs.use('select/x.x.x/select') 的历史遗留问题 -->
<!-- CMS:全站公共 cms/tracker/seajs.vm结束:tracker/seajs.vm -->
<!-- FD:106:alipay/tracker/tracker_time.vm:START --><!-- FD:106:alipay/tracker/tracker_time.vm:784:tracker_time.schema:tracker_time-tracker功能:START -->










<!-- FD:106:alipay/tracker/tracker_time.vm:784:tracker_time.schema:tracker_time-tracker功能:END -->
<!-- FD:106:alipay/tracker/tracker_time.vm:END -->




<!-- CMS:外部商户匿名收银台cms/日志入口/全局日志入口开始:excashier/globalLog/log.vm -->  <!-- CMS:外部商户匿名收银台cms/日志入口/全局日志入口结束:excashier/globalLog/log.vm -->
<link rel="stylesheet" type="text/css" href="css/codePay.css" media="all">
</head>
<body style="min-width: 990px;">
<!-- CMS:全站公共 cms/notice/headNotice.vm开始:alipay/notice/headNotice.vm --><!--[if lte IE 7]>
<style>.kie-bar { display: none; height: 24px; line-height: 1.8; font-weight:normal; text-align: center; border:1px solid #fce4b5; background-color:#FFFF9B; color:#e27839; position: relative; font-size: 12px; margin: 5px 0 0 0; padding: 5px 0 2px 0; } .kie-bar a { text-decoration: none; color:#08c; background-repeat: none; } .kie-bar a#kie-setup-IE8,.kie-bar a#kie-setup-taoBrowser { padding: 0 0 2px 20px; *+padding-top: 2px; *_padding-top: 2px; background-repeat: no-repeat; background-position: 0 0; } .kie-bar a:hover { text-decoration: underline; } .kie-bar a#kie-setup-taoBrowser { background-position: 0 -20px; }</style>
<div id="kie-bar" class="kie-bar">您现在使用的浏览器版本过低，可能会导致部分图片和信息的缺失。请立即 <a href="http://www.microsoft.com/china/windows/IE/upgrade/index.aspx" id="kie-setup-IE8" seed="kie-setup-IE8" target="_blank" title="免费升级至IE8浏览器">免费升级</a> 或下载使用 <a href="http://download.browser.taobao.com/client/browser/down.php?pid=0080_2062" id="kie-setup-taoBrowser" seed="kie-setup-taoBrowser" target="_blank" title="淘宝浏览器">淘宝浏览器</a> ，安全更放心！ <a title="查看帮助" target="_blank" seed="kie-setup-help" href="http://help.alipay.com/lab/help_detail.htm?help_id=260579">查看帮助</a></div>
<script type="text/javascript">
(function () {
    function IEMode() {
        var ua = navigator.userAgent.toLowerCase();
        var re_trident = /\btrident\/([0-9.]+)/;
        var re_msie = /\b(?:msie |ie |trident\/[0-9].*rv[ :])([0-9.]+)/;
        var version;
        if (!re_msie.test(ua)) {
            return false;
        }
        var m = re_trident.exec(ua);
        if (m) {
            version = m[1].split(".");
            version[0] = parseInt(version[0], 10) + 4;
            version = version.join(".");
        } else {
            m = re_msie.exec(ua);
            version = m[1];
        }
        return parseFloat(version);
    }
    var ie = IEMode();
    if (ie && ie < 8 && (self.location.href.indexOf("_xbox=true") < 0)) {
        document.getElementById('kie-bar').style.display = 'block';
        document.getElementById('kie-setup-IE8').style.backgroundImage = 'url(https://i.alipayobjects.com/e/201307/jYwARebNl.png)';
        document.getElementById('kie-setup-taoBrowser').style.backgroundImage = 'url(https://i.alipayobjects.com/e/201307/jYwARebNl.png)';
    }
})();
</script>
<![endif]-->

<!-- CMS:全站公共 cms/notice/headNotice.vm结束:alipay/notice/headNotice.vm --><div class="topbar">
    <div class="topbar-wrap fn-clear">
        <a href="https://help.alipay.com/lab/help_detail.htm?help_id=258086" class="topbar-link-last" target="_top" seed="goToHelp">常见问题</a>
        		<span class="topbar-link-first">你好，欢迎使用支付宝付款！</span>
		    </div>
</div>

<div id="header">
    <div class="header-container fn-clear">
        <div class="header-title">
            <div class="alipay-logo"></div>
            <span class="logo-title">我的收银台</span>
        </div>
    </div>
</div>
<div id="container">





<div class="mi-notice mi-notice-success mi-notice-titleonly order-timeout-notice" id="J_orderPaySuccessNotice">
    <div class="mi-notice-cnt">
        <div class="mi-notice-title">
            <i class="iconfont" title="支付成功"></i>

            <h3>支付成功，<span class="ft-orange" id="J_countDownSecond">3</span> 秒后自动返回商户。</h3>
        </div>
    </div>
</div>

<div class="mi-notice mi-notice-error mi-notice-titleonly order-timeout-notice" id="J_orderDeadlineNotice">
    <div class="mi-notice-cnt">
        <div class="mi-notice-title">
            <i class="iconfont" title="交易超时"></i>

            <h3>抱歉，您的交易因超时已失败。</h3>

            <p class="mi-notice-explain-other">
                您订单的最晚付款时间为： <span id="J_orderDeadline"></span>，目前已过期，交易关闭。
            </p>
        </div>
    </div>
</div>

<!-- CMS:全站公共 cms/tracker/um.vm开始:tracker/um.vm -->




<span style="display: inline; width: 1px; height: 1px; overflow: hidden;">






</span>
<!-- CMS:全站公共 cms/tracker/um.vm结束:tracker/um.vm -->

<!-- 页面主体 -->
<div id="content" class="fn-clear">
        
<div id="J_order" class="order-area" data-module="excashier/login/2015.08.01/orderDetail">


        
        
                
        <div id="order" data-role="order" class="order order-bow">
                            <form data-module="excashier/login/2015.08.01/checkTimeout" action="https://excashier.alipay.com/standard/timeOutErrorPage.htm" method="post" id="J_timeoutForm" class="fn-hide">
                    	<input name="_form_token" value="d7e55033e3d24e83a4c7374afe745d6fRZ12" type="hidden">
                    <input name="orderId" value="09991ce84cc44c24bfeb911f256172dd.40" id="orderId" type="hidden">
                    <input name="orderTimeoutRequestInterval" value="" id="J_orderTimeoutRequestInterval" type="hidden">
                    <input name="timeoutCheckUrl" value="https://excashier.alipay.com:443/standard/payOrderClosedCountDown.json" type="hidden">
                    <input name="timeoutMillseconds" value="899670" id="J_timeoutMillseconds" type="hidden">
                    <input name="timeoutUrl" value="https://excashier.alipay.com:443/standard/timeOutPage.htm?payOrderId=09991ce84cc44c24bfeb911f256172dd.40" id="J_timeoutUrl" type="hidden">
                    <input smartracker="on" seed="J_timeoutForm-btn" value="" type="submit">
                </form>
                        <div class="orderDetail-base" data-role="J_orderDetailBase">
                <div class="order-extand-explain fn-clear">
            <span class="fn-left explain-trigger-area order-type-navigator" style="cursor: auto;" data-role="J_orderTypeQuestion">

            <span>正在使用即时到账交易</span>
    
    <span data-role="J_questionIcon" seed="order-type-detail" style="cursor: pointer; color: rgb(0, 136, 204);">[?]</span>
            </span>
                                            <span class="fn-left create-time" id="teDelay">
                <span>交易将在<span id="teOrderDelayText">14分钟</span>后关闭，请及时付款！</span>
            </span>
                                    </div>
                <div class="commodity-message-row">
            <span class="first long-content">
                魅族商城  OC web 21092806101736175011OCN01
            </span>
                                            <span class="second short-content">
                                                                    收款方：珠海市魅族通讯设备有限公司
                            </span>
                                    </div>
                                                <span class="payAmount-area" id="J_basePriceArea">
                                                     <strong class=" amount-font-22 ">1699.00</strong> 元
                
        </span>
            </div>
            
<div class="ui-tip ui-question-tip fn-hide" seed="question-tip" data-role="J_orderTypeTip">
    <div class="ui-dialog-container">
        <div class="ui-dialog-head-text">
            <span>付款后资金直接进入对方账户</span>
        </div>

        <ul class="ui-dialog-content">
            <li>
                若发生退款需联系收款方协商，如付款给陌生人，请谨慎操作。
            </li>
        </ul>
    </div>
    <div class="ui-icon-dialog-arrow">
        ↓
    </div>
</div>


<div class="ui-tip ui-question-tip fn-hide" data-role="J_exchangeTip">
    <div class="ui-dialog-container" style="width: 280px;">
        <ul class="ui-dialog-content">
            <li>
                1、支付宝不收取任何货币兑换手续费。
            </li>
            <li>
                2、最终支付金额为人民币金额，非外币金额。
            </li>
			        </ul>
    </div>
    <div class="ui-icon-dialog-arrow">
        ↓
    </div>
</div>

            
            <a id="J_OrderExtTrigger" class="order-ext-trigger" href="#" seed="order-detail-more" data-role="J_oderDetailMore">
                订单详情
            </a>
            

                <div style="display: none;" class="ui-detail fn-hide" data-role="J_orderDetailCnt" id="J-orderDetail">
                    <div class="ajax-Account od-more-cnt fn-clear">
                        <div class="first  long-content">魅族商城  OC web 21092806101736175011OCN01</div>
                        <ul class="order-detail-container">
                            <li class="order-item">
                                                                                                             <table>
    <tbody>
                <tr>
            <th class="sub-th">收款方：</th>
            <td>
                                    珠海市魅族通讯设备有限公司
                                            </td>
        </tr>
                        <tr>
            <th class="sub-th">订单号：</th>
            <td>21092806101736175011OCN01</td>
        </tr>
                        <tr>
            <th class="sub-th">商品名称：</th>
            <td>
                                    魅族商城  OC web 21092806101736175011OCN01
                            </td>
        </tr>
                                <tr>
            <th class="sub-th">交易金额：</th>
            <td>1699.00</td>
        </tr>
                            </tbody>
</table>

                                                                    
                            </li>
                        </ul>
                    </div>
        <span class="payAmount-area payAmount-area-expand">
                <strong class=" amount-font-22 ">1699.00</strong> 元
        </span>
                    <iframe src="index_1.html" class="ui-detail-iframe-fix" data-role="J_orderDetailFrameFix"></iframe>
                </div>

            <a style="display: none;" id="J_OrderExtTrigger" class="order-ext-trigger fn-hide" href="#" seed="order-detail-more" data-role="J_oderDetailShrink">
                订单详情
            </a>
		        </div>
        <input name="oid" value="09991ce84cc44c24bfeb911f256172dd.40" id="J_orderId" type="hidden">
        <input name="pid" value="2088901932428369" id="J_partnerId" type="hidden">
        <input name="pid" value="21092806101736175011OCN01" id="J_outBizID" type="hidden">
        <input name="qrContextId" value="201609290030005840e29f9aa66684873d" id="J_qrContextId" type="hidden">
        <input name="qrPayLoopCheckUrl" value="https://tradeexprod.alipay.com/fastpay/qrPayLoopCheck.json" id="J_qrPayLoopCheckUrl" type="hidden">
        <input name="qrDiscountText" value="" id="J_qrDiscountText" type="hidden">
        <input name="qrDiscountDesc" value="" id="J_qrDiscountDesc" type="hidden">

</div>




    <!-- 操作区 -->
    <div class="cashier-center-container">
        
        <div data-module="excashier/login/2016.08.01/loginPwdMemberT" id="J_loginPwdMemberTModule" class="cashiser-switch-wrapper fn-clear">

            <!-- 扫码支付页面 -->
            <div class="cashier-center-view view-qrcode fn-left" id="J_view_qr">
                                

<!-- 扫码区域 -->
<div id="hidden-input-area" class="fn-hide">
    <input name="qrCode" value="https://qr.alipay.com/upx09723m2zgbe2qgqzz40f4" id="J_qrCode" type="hidden">
    <input name="qrImgUrl" value="https://mobilecodec.alipay.com/show.htm?code=upx09723m2zgbe2qgqzz40f4" id="J_qrImgUrl" type="hidden">
    <input name="qrUseImage" value="false" id="J_qrUseImage" type="hidden">
    <input name="qrContextId" value="201609290030005840e29f9aa66684873d" id="J_qrContextId" type="hidden">
    <input name="qrRenewalURL" value="https://excashier.alipay.com:443/standard/renewQRCode.json?payOrderId=09991ce84cc44c24bfeb911f256172dd.40" id="J_qrRenewalURL" type="hidden">
    <input name="qrPushCheckURL" value="" id="J_qrPushCheckURL" type="hidden">
    <input name="qrLoopCheckURL" value="https://excashier.alipay.com:443/standard/queryQRStatus.json?payOrderId=09991ce84cc44c24bfeb911f256172dd.40" id="J_qrLoopCheckURL" type="hidden">
    <input name="qrPaySuccGotoURL" value="https://unitradeprod.alipay.com:443/acq/cashierReturn.htm?sign=K1iSL1Db2LfYI7ifTZ7%252Fu8BR1ShlfGgD8V7t5abRk6A8lIqDNhxcx3YHx3LO0netH4n0zd%252F4zDwuedY%253D&amp;outTradeNo=21092806101736175011OCN01&amp;pid=2088901932428369&amp;type=1" id="J_qrPaySuccGotoURL" type="hidden">
    <input name="qrCheckMode" value="LOOP" id="J_qrCheckMode" type="hidden">
    <input name="qrExpirySeconds" value="99" id="J_qrExpirySeconds" type="hidden">
    <input name="qrLogonId" value="" id="J_qrLogonId" type="hidden">
    <input name="qrBizType" value="UNI_PC_MERCHANT" id="J_qrBizType" type="hidden">
    <input name="resultPageStayTime" value="5" id="J_resultPageStayTime" type="hidden">
	<input name="adName" value="" id="J_adName" type="hidden">
    <input name="adInfo" value="" id="J_adInfo" type="hidden">
</div>

<div data-role="qrPayArea" class="qrcode-integration qrcode-area" id="J_qrPayArea">
        <div class="qrcode-header">
        <div class="ft-center">扫一扫付款（元）</div>
        <div class="ft-center qrcode-header-money">1699.00</div>
    </div>

	    	
        <div data-role="qrPayCrash" class="qrcode-img-area qrcode-img-crash fn-hide">
        <div class="qrcode-busy-icon">
            <i class="iconfont qrpay-crash-icon"></i>
        </div>
        <p class="qrcode-busy-text ft-16">二维码太忙了,<br>请稍后再试</p>
        <a href="#" class="mi-button mi-button-lwhite" data-role="qrPayRefreshBtn" seed="NewQr_qrcodeRefreshBtn"><span class="mi-button-text">重试</span></a>
    </div>

        <div class="qrcode-img-wrapper" data-role="qrPayImgWrapper">
        <div data-role="qrPayImg" class="qrcode-img-area">
            <div style="display: none;" class="ui-loading qrcode-loading" data-role="qrPayImgLoading">加载中</div>
        <div style="position: relative; display: inline-block;"><canvas style="float: left;" height="168" width="168"></canvas><img src="images/t1z5xfxdxmxxxxxxxx.png" style="position: absolute; top: 50%; left: 50%; width: 42px; height: 42px; margin-left: -21px; margin-top: -21px;"></div></div>

        <div class="qrcode-img-explain fn-clear">
            <img smartracker="on" seed="qrcodeImgExplain-tImagesT1bdtfXfdiXXXXXXXX" class="fn-left" src="css/t1bdtfxfdixxxxxxxx.png" alt="扫一扫标识">
            <div class="fn-left">打开手机支付宝<br>扫一扫继续付款</div>
        </div>
    </div>

        <div style="display: block;" class="qrcode-foot" data-role="qrPayFoot">
        <div style="display: block;" data-role="qrPayExplain" class="qrcode-explain fn-hide">
            <a href="https://mobile.alipay.com/index.htm" class="qrcode-downloadApp" data-boxurl="https://cmspromo.alipay.com/down/new.htm" data-role="dl-app" target="_top" seed="NewQr_qr-pay-download">首次使用请下载手机支付宝</a>
        </div>

        <div data-role="qrPayScanSuccess" class="mi-notice mi-notice-success mi-notice-titleonly qrcode-notice fn-hide">
            <div class="mi-notice-cnt">
                <div class="mi-notice-title qrcode-notice-title">
                    <i class="iconfont qrcode-notice-iconfont" title="扫描成功"></i>
                    <p class="mi-notice-explain-other qrcode-notice-explain ft-break">
                        <span class="ft-orange fn-mr5" data-role="qrPayAccount"></span>已创建订单，请在手机支付宝上完成付款
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- 优惠 TIP -->
    <div class="ft-center fn-hide" data-role="pr-container">
        <div class="qrcode-discount-privilege">
            <ul class="discount-list">
                <li class="discount-item">
                    <span class="action-rt">
                        <span class="action-rt-type">惠</span>
                        <span class="rt-discount" data-role="pr-name"></span>
                    </span>
                    | <em data-role="pr-explain">使用扫码支付可享受优惠</em>
                </li>
                <li class="discount-desc" data-role="pr-desc"></li>
            </ul>
        </div>
    </div>

</div>



<!-- 指引区域 -->
<div class="qrguide-area" id="J_qrguideArea" seed="NewQr_animationClick">
    <img smartracker="on" seed="J_qrguideArea-qrguideAreaImg" src="css/t13cpgxf8mxxxxxxxx.png" class="qrguide-area-img background">
    <img style="display: block;" smartracker="on" seed="J_qrguideArea-qrguideAreaImgT1" src="images/t1asfgxdtnxxxxxxxx.png" class="qrguide-area-img active">
</div>
	

            </div>

                        
<!-- 点击切换区域 -->
<div style="padding-right: 10px; margin-left: -10px;" class="view-switch qrcode-show fn-left" id="J_viewSwitcher" unselectable="on" onselectstart="return false;" seed="NewQr_viewSwitch">

    <div class="switch-tip switch-qrcode-tip " id="J_tip_qr">
        <div class="switch-tip-font">&nbsp;</div>
        <div class="switch-tip-icon-wrapper">
            <i class="switch-tip-icon iconfont" title="显示器"> &nbsp</i>
            <img smartracker="on" seed="switchTipIconWrapper-switchTipIconImg" class="switch-tip-icon-img" src="images/t1hhfgxxvexxxxxxxx.png" alt="支付宝图标" width="50" height="17">
        </div>
        <a smartracker="on" seed="J_tip_qr-switchTipBtn" class="switch-tip-btn" href="javascript:void(0)">&lt;&nbsp;登录账户付款</a>
    </div>

    <div class="switch-tip switch-pc-tip fn-hide" id="J_tip_pc">
        <div class="switch-tip-font">试试手机支付宝</div>
        <div class="switch-tip-icon-wrapper">
            <i class="switch-tip-icon iconfont" title="手机"></i>
            <img smartracker="on" seed="switchTipIconWrapper-switchTipIconImgT1" class="switch-tip-icon-img" src="images/t1z5xfxdxmxxxxxxxx.png" alt="手机支付宝图标" width="30" height="30">
        </div>
        <a smartracker="on" seed="J_tip_pc-switchTipBtn" class="switch-tip-btn" href="javascript:void(0)">扫一扫付款&nbsp;&gt;</a>
    </div>
</div>
            <!-- PC 付款登录页面 -->
            <div class="cashier-center-view view-pc fn-left" id="J_view_pc">
                <form action="https://excashier.alipay.com/standard/securityPost.json?payOrderId=09991ce84cc44c24bfeb911f256172dd.40&amp;viewModel=payerPwdLoginViewModel" method="post" id="J_TloginForm">
                    <input name="commonAccountIdentiAuthUrl" value="https://excashier.alipay.com:443/standard/securityRender.phtm?payOrderId=09991ce84cc44c24bfeb911f256172dd.40&amp;viewModel=payerPwdLoginViewModel" type="hidden">
                                        	<input name="_form_token" value="bccd4fa6ed164a9583abf41e7ac9af80RZ12" type="hidden">
                        <input name="viewModelId" value="" type="hidden">

                                        


<div class="loginBox">

    <div class="login-title-area">
        <div class="login-title">
            <div class="login-title-left">
                <p class="lt-text ft-yh">登录支付宝账户付款</p>
            </div>
            <div class="login-title-right">
                <a href="javascript:void(0);" class="to-mobile rt-text" id="J_signUp" seed="NewQr_register">新用户注册</a>
            </div>
        </div>
    </div>

    <div class="teForm">
        <div class="login-panel">
            
            <div class="mi-form-item commonAccountContainer quick-empty-parent" id="J_commonAccountContainer">
                <label class="mi-label">账户名：</label>
                <span class="user-login-account fn-hide"></span>
                <input data-explain="" autocomplete="off" name="loginId" class="mi-input mi-input-account" value="" id="J_tLoginId" seed="NewQr_tAccountInput" placeholder="手机号码/邮箱" type="email"><span class="quick-empty" seed="quick-empty">×</span>
                <input name="loginIdValue" id="J_tLoginIdValue" value="" type="hidden">
                <span class="fg-pwd-area">
                    <a class="aside-link" href="https://help.alipay.com/lab/help_detail.htm?help_id=216509" seed="NewQr_forgetAccountLink" target="_top">忘记账户名？</a>
                </span>
                <div class="mi-form-explain"></div>
            </div>

            <div class="mi-form-item commonAccountFixedContainer fn-mb8 fn-hide" id="J_commonAccountFixedContainer">
                <label class="mi-label">账户名：</label>
                <div class="fn-mt8"><span class="user-login-account" id="J_userLoginText"></span><span class="fn-ml5 fn-hide" id="J_userLoginExplain">已创建订单</span></div>
                <div class="mi-form-explain"></div>
            </div>

            <div class="mi-form-item" id="J_passwordContainer">
                <label class="mi-label">支付密码：</label>
                <span>
                    			            <input data-explain="
                    <p>
                        请输入账户的 <span class=&quot;ft-red&quot;>支付密码</span>，不是登录密码。
                    </p>
                " id="J_password" name="password" value="" type="hidden">
	<input name="pwdSecurityId" value="web|excashier_payment_pwd_control|6b583e04-0300-44b5-897a-f270343af201RZ12" type="hidden">
	

    <input value="" smartracker="on" seed="J_passwordContainer-ipt" style="display: none;">
  <input smartracker="on" seed="J_passwordContainer-iptT1" style="display: none;" type="password">
                                  

        
        <span class="standardPwdContainer" style="display: none;">
      <input smartracker="on" seed="standardPwdContainer-payPasswd_input" tabindex="" id="payPasswd_input" name="payPasswd_input" class="ui-input" oncontextmenu="return false" onpaste="return false" oncopy="return false" oncut="return false" autocomplete="off" type="password">
    </span>
    <span class="alieditContainer" id="payPasswd_container"><input tabindex="" id="payPasswd_rsainput" name="payPasswd_rsainput" class="ui-input i-text" oncontextmenu="return false" onpaste="return false" oncopy="return false" oncut="return false" autocomplete="off" value="" type="password"></span>


    




    
    
  
  
    
    
    

<input name="J_aliedit_using" value="true" type="hidden">
<input id="payPasswd" name="payPasswd" value="" type="hidden">
<input name="J_aliedit_key_hidn" value="payPasswd" type="hidden">

<input name="J_aliedit_uid_hidn" value="alieditUid" type="hidden">
<input id="alieditUid" name="alieditUid" value="1cbdda2322bbae639aaac34c303a3211" type="hidden">

<input name="REMOTE_PCID_NAME" value="_seaside_gogo_pcid" type="hidden">
<input name="_seaside_gogo_pcid" value="" type="hidden">
<input name="_seaside_gogo_" value="" type="hidden">
<input name="_seaside_gogo_p" value="" type="hidden">

<input name="J_aliedit_prod_type" value="" type="hidden">

<input name="security_activeX_enabled" value="false" type="hidden">

<input name="J_aliedit_net_info" value="" type="hidden">

<input render="R" id="edit_infor" value="" type="hidden">

            
            

















      

                </span>
                <span class="fg-pwd-area">
                    <a target="_top" class="aside-link" href="https://self.alipay.com/selfhelp/passwordfind/index.htm?type=P" seed="NewQr_forgetPwdLink">忘记密码？</a>
                </span>
                <div class="mi-form-explain">
                    <p>
                        请输入账户的 <span class="ft-red">支付密码</span>，不是登录密码。
                    </p>
                </div>
            </div>
            


            <div data-role="alieditContainer" class="fn-hide"></div>

            <div class="submitContainer fn-clear">
                <div class="t-contract-container fn-left">
									
                    								</div>
                <div class="left-submitContainer fn-left">
                    <a href="javascript:void(0);" class="newBtn-blue newBtn-long" id="J_newBtn" data-role="submitBtn" seed="NewQr_tAccountSubmit" data-defaulttext="下一步" data-submittext="提交中...">
                        <span>下一步</span>
                    </a>
                </div>
            </div>
			<div class="fn-hide" id="J_agreementForexprod">
                <div data-role="J_alipayForexprodAgreement" class="alipay-agreement">
        <div class="alipay-agreement-content" style="text-align: left; overflow-y: scroll; border: 1px solid rgb(225, 225, 225);" smartracker="on">
  
  <h3 style="text-indent: 2em;"><b>一、 概述 </b></h3>
<p style="text-indent: 2em;">本协议中将支付宝（中国）网络技术有限公司简称为“本公司”，对使用本公司服务的用户简称为“您”或“支付宝用户”或“用户”。 
您确认，在您注册成为支付宝用户以接受本公司的服务，或您以其他本公司允许的方式实际使用本公司服务前，您已充分阅读、理解并接受本协议的全部内容，一旦您使用支付宝服务，即表示您同意遵循本协议之所有约定。 
  <b>本公司提醒您认真阅读、充分理解本协议各条款，特别是以粗体标注部分。</b>如您不同意接受本协议的任意内容，或者无法准确理解相关条款含义的，请不要进行后续操作。如果您对本协议的条款有疑问的，请通过本公司客服渠道进行询问，本公司将向您解释条款内容。
  </p> 
  <h3 style="text-indent: 2em;"><b>二、 主体内容</b></h3>
<p>
1、购结汇服务，指本公司与本公司合作银行向您提供的代为购买外汇、接受境外汇入外汇并结汇的服务。购汇服务中，本公司代您向本公司合作银行提交购汇信息并将您的款项交付该银行用于购买外汇。结汇服务中，本公司代您向本公司合作银行提交结汇信息并将您来自于境外的外汇款项交付该银行为您转换成人民币。<br>
2、您声明，您满足本公司网站上公布的《支付宝服务协议》中对支付宝用户的身份要求，且按照本公司要求通过了实名验证，具有签订和履行本协议的资格。<br>
<b>3、您同意，发布在支付宝网站支付页面的外汇牌价或本公司另行展示或安排的外汇牌价是您自愿接受的，而不论该外汇牌价是否是最新的或对您最有利的；一旦您确认使用支付宝的购结汇服务，即代表您同意选择该外汇牌价中相应外汇的汇率折算成人民币资金进行支付或收款。<br>
4、目前，您使用本服务时无需承担外汇牌价中标明的汇率与实际购结汇时或支付时或收款时或发生退货时的汇率之间的汇兑损益（不承担汇兑损失也不享有汇兑收益）。您同意，本公司有权要求您自某一时刻起承担该汇兑损益，届时将以支付宝网站发布的公告为准。<br>
5、您确认，您在本公司网站支付页面点击确认购买或在支持本服务的商家处使用本服务购买商品或服务，您即与本公司合作银行间直接形成外汇购买协议，相应的权利义务即由您与本公司合作银行负责履行与承担，本公司不对此提供任何形式的担保或承担与之相关的其他法律责任。同时，本公司有权从您的资产（包括但不限于支付宝账户余额、余额宝、支付宝账户绑定的银行卡内资金等）中扣划（扣款顺序以本公司的规则为准）与您购汇指令等值的人民币资金支付给本公司合作银行。若您的购汇指令超出本公司或相关法律法规或者监管部门或相关银行的额度限制的，则该购汇指令自动无效。<br>
6、您同意，本公司有权将您的身份信息、交易信息、购结汇明细等信息同步给本公司合作银行或为了向您提供购结汇服务而必须获得以上信息的有权机构。<br>
7、您同意，本公司有权就本服务向您收取服务费，服务费收取时间及标准届时以支付宝网站发布的公告为准。<br>
  8、您同意，您使用本服务购结汇的限额等应符合相关法律法规及监管部门或本公司的规定。<br></b>
</p>
</div>
                    <a smartracker="on" seed="alipayAgreement-miButton" href="javascript:;" class="mi-button mi-button-lorange" data-role="J_forexprodConfirmAgreement">
                        <span class="mi-button-text" seed="NewQr_agreement-confirm-btn">确定</span>
                    </a>

                </div>
            </div>
        </div>
    </div>

</div>

<input value="false" name="pwdLoginFront" id="J_pwdLoginFront" type="hidden">
                </form>
            </div>

        </div>
    </div>
    <!-- 操作区 结束 -->

</div>
<!-- 页面主体 结束 -->

 
    
  
  

   

 <!-- CMS:全站公共 cms/安全cms/外部收银台信息采集开始:security/sensorSdk.vm -->



<!-- CMS:全站公共 cms/安全cms/外部收银台信息采集结束:security/sensorSdk.vm -->

<!--防止钓鱼确认-->
<input name="hasAntiFishingRisk" value="false" type="hidden">
<input name="needCheckIframe" value="true" type="hidden">
<div class="fn-hide" data-role="fishing-popup">
    
    <div class="anti-fishing">
	
	    <p>您购买的商品为：<span class="ft-break ft-bold ft-red">魅族商城  OC web 21092806101736175011OCN01</span>，收款方为：<strong><font color="red">珠海市魅族通讯设备有限公司</font></strong>，请确认。</p>
	    <p>建议您付款前<a smartracker="on" seed="antiFishing-link" href="http://bbs.taobao.com/catalog/thread/154504-251045688.htm" target="_top">先查看防骗案例</a></p>

    <h3>是否仍继续付款？</h3>

    <ul>
        <li><label for="J_antiFishingStop"><input name="pay" value="N" id="J_antiFishingStop" seed="excashier-antiFishing-cancelPay" type="radio">否，我不想继续付款了</label></li>
        <li><label for="J_antiFishingPay"><input name="pay" value="Y" id="J_antiFishingPay" seed="excashier-antiFishing-confirmPay" type="radio">是，我还要继续付款，自担风险。</label></li>
    </ul>
    <div id="J_antiFishingViewCase" class="fn-clear fn-hide view-case">
        <span class="btn btn-ok">
            <input tabindex="3" seed="excashier-antiFishing-viewCase" value="查看相关案例" type="button">
            <input id="J_openUrl" value="https://bbs.taobao.com/catalog/thread/154504-251045688.htm" type="hidden">
        </span>
    </div>
    </div>
</div>


<input name="commonAgreementUrl" value="https://excashier.alipay.com:443/standard/agreementDetail.phtm?payOrderId=09991ce84cc44c24bfeb911f256172dd.40&amp;viewModel=standard%3AcommonAgreementViewModel.vm" type="hidden">
<input name="memoryPayAgreementUrl" value="https://excashier.alipay.com:443/standard/agreementDetail.phtm?payOrderId=09991ce84cc44c24bfeb911f256172dd.40&amp;viewModel=standard%3AmemoryPayAgreementViewModel.vm" type="hidden">

<!--[if lt IE 10]>
<script src="https://as.alipayobjects.com/component/??console-polyfill/0.2.2/index.js,es5-shim/4.1.14/es5-shim.js,es5-shim/4.1.14/es5-sham.js,html5shiv/3.7.2/html5shiv.js"></script>
<![endif]-->

<!-- FD:174:alipay/foot/cliveService.vm:START --><!-- FD:174:alipay/foot/cliveService.vm:1261:cliveService.schema:在线客服配置:START -->
    <div style="display: none;">onlineServer</div>
    
<!-- FD:174:alipay/foot/cliveService.vm:1261:cliveService.schema:在线客服配置:END -->

<!-- FD:174:alipay/foot/cliveService.vm:END -->


<div id="footer">
    <!-- CMS:全站公共 cms/foot/copyright.vm开始:foot/copyright.vm -->
<div class="copyright">
      支付宝版权所有 2004-2016 <a smartracker="on" seed="copyright-link" href="http://fun.alipay.com/certificate/jyxkz.htm" target="_top">ICP证：沪B2-20150087</a>
  </div>
<div class="server" id="ServerNum">
  excashier-30-9 &nbsp; 0a3727c514750799240836762_0
</div><!-- CMS:全站公共 cms/foot/copyright.vm结束:foot/copyright.vm --></div>
</div><!-- /container -->
<div id="partner"><img smartracker="on" seed="partner-iE2013032R3cKfrKqS" alt="合作机构" src="css/2r3ckfrkqs.png"></div>

 
<!-- uitpl:/component/tracker.vm -->
<!-- FD:106:alipay/tracker/tracker.vm:START --><!-- FD:106:alipay/tracker/tracker.vm:785:tracker.schema:tracker-性能监控及自动化埋点启动:START -->







<!-- FD:106:alipay/tracker/sai.vm:START -->
  <!-- FD:106:alipay/tracker/sai.vm:END -->
<!-- FD:106:alipay/tracker/cmsbuffer.vm:START --><!-- FD:106:alipay/tracker/cmsbuffer.vm:997:cmsbuffer.schema:main-CMS全站修复:START -->
	
			
			
			
			
			
			
			
			
			










<!-- FD:106:alipay/tracker/cmsbuffer.vm:997:cmsbuffer.schema:main-CMS全站修复:END -->
<!-- FD:106:alipay/tracker/cmsbuffer.vm:END --><!-- FD:106:alipay/tracker/tracker.vm:785:tracker.schema:tracker-性能监控及自动化埋点启动:END -->
<!-- FD:106:alipay/tracker/tracker.vm:END -->

<div class="ui-poptip ui-poptip-white qrpay-discount-tip fn-hide" id="J_qrPayTip">
    <div class="ui-poptip-shadow">
        <div class="ui-poptip-container qrpay-discount-container">
            <div class="ui-poptip-arrow ui-poptip-arrow-10">
                <em></em>
                <span></span>
            </div>
            <div class="ui-poptip-content">
                <p>使用扫码支付，不可与</p>
                <p>支付宝其他优惠同时使用。</p>
            </div>
        </div>
    </div>
</div>




<div style="height: 0px; width: 0px; overflow: hidden;"><object style="height: 0px; width: 0px; overflow: hidden;" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="https://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab" id="JSocket" width="0" height="0"><param name="allowScriptAccess" value="always"><param name="movie" value="https://acjs.aliyun.com/flash/JSocket.swf"> <embed src="jsocket.swf" name="JSocket" allowscriptaccess="always" type="application/x-shockwave-flash" pluginspage="https://www.adobe.com/go/getflashplayer_cn" width="0" height="0"></object></div><iframe src="index_2.html" style="display: none;" name="iframeGo" id="iframeGo"></iframe><ul data-role="emailListBox" class="email-suggest-out" style="width: 348px; position: absolute; z-index: 1; overflow: hidden; display: none;"></ul><div style="z-index: 2011; position: fixed;" class="ibox" tabindex="-1"><div class="ibox-close" title="关闭" data-role="boxClose" style="display: block;">×</div><div class="ibox-content" style="background-color: rgb(255, 255, 255); height: 100%;"><div style="width: 620px;" class="ibox-title" data-role="boxTitle">支付宝购汇协议</div><div style="width: 620px; height: auto;" data-role="boxContent">
                <div data-role="J_alipayForexprodAgreement" class="alipay-agreement">
        <div class="alipay-agreement-content" style="text-align: left; overflow-y: scroll; border: 1px solid rgb(225, 225, 225);" smartracker="on">
  
  <h3 style="text-indent: 2em;"><b>一、 概述 </b></h3>
<p style="text-indent: 2em;">本协议中将支付宝（中国）网络技术有限公司简称为“本公司”，对使用本公司服务的用户简称为“您”或“支付宝用户”或“用户”。 
您确认，在您注册成为支付宝用户以接受本公司的服务，或您以其他本公司允许的方式实际使用本公司服务前，您已充分阅读、理解并接受本协议的全部内容，一旦您使用支付宝服务，即表示您同意遵循本协议之所有约定。 
  <b>本公司提醒您认真阅读、充分理解本协议各条款，特别是以粗体标注部分。</b>如您不同意接受本协议的任意内容，或者无法准确理解相关条款含义的，请不要进行后续操作。如果您对本协议的条款有疑问的，请通过本公司客服渠道进行询问，本公司将向您解释条款内容。
  </p> 
  <h3 style="text-indent: 2em;"><b>二、 主体内容</b></h3>
<p>
1、购结汇服务，指本公司与本公司合作银行向您提供的代为购买外汇、接受境外汇入外汇并结汇的服务。购汇服务中，本公司代您向本公司合作银行提交购汇信息并将您的款项交付该银行用于购买外汇。结汇服务中，本公司代您向本公司合作银行提交结汇信息并将您来自于境外的外汇款项交付该银行为您转换成人民币。<br>
2、您声明，您满足本公司网站上公布的《支付宝服务协议》中对支付宝用户的身份要求，且按照本公司要求通过了实名验证，具有签订和履行本协议的资格。<br>
<b>3、您同意，发布在支付宝网站支付页面的外汇牌价或本公司另行展示或安排的外汇牌价是您自愿接受的，而不论该外汇牌价是否是最新的或对您最有利的；一旦您确认使用支付宝的购结汇服务，即代表您同意选择该外汇牌价中相应外汇的汇率折算成人民币资金进行支付或收款。<br>
4、目前，您使用本服务时无需承担外汇牌价中标明的汇率与实际购结汇时或支付时或收款时或发生退货时的汇率之间的汇兑损益（不承担汇兑损失也不享有汇兑收益）。您同意，本公司有权要求您自某一时刻起承担该汇兑损益，届时将以支付宝网站发布的公告为准。<br>
5、您确认，您在本公司网站支付页面点击确认购买或在支持本服务的商家处使用本服务购买商品或服务，您即与本公司合作银行间直接形成外汇购买协议，相应的权利义务即由您与本公司合作银行负责履行与承担，本公司不对此提供任何形式的担保或承担与之相关的其他法律责任。同时，本公司有权从您的资产（包括但不限于支付宝账户余额、余额宝、支付宝账户绑定的银行卡内资金等）中扣划（扣款顺序以本公司的规则为准）与您购汇指令等值的人民币资金支付给本公司合作银行。若您的购汇指令超出本公司或相关法律法规或者监管部门或相关银行的额度限制的，则该购汇指令自动无效。<br>
6、您同意，本公司有权将您的身份信息、交易信息、购结汇明细等信息同步给本公司合作银行或为了向您提供购结汇服务而必须获得以上信息的有权机构。<br>
7、您同意，本公司有权就本服务向您收取服务费，服务费收取时间及标准届时以支付宝网站发布的公告为准。<br>
  8、您同意，您使用本服务购结汇的限额等应符合相关法律法规及监管部门或本公司的规定。<br></b>
</p>
</div>
                    <a smartracker="on" seed="alipayAgreement-miButton" href="javascript:;" class="mi-button mi-button-lorange" data-role="J_forexprodConfirmAgreement">
                        <span class="mi-button-text" seed="NewQr_agreement-confirm-btn">确定</span>
                    </a>

                </div>
            </div></div></div><div style="display: none; width: 100%; height: 846px; position: absolute; z-index: 2010; top: 0px; left: 0px; overflow: hidden;"><div style="height: 100%; background-color: rgb(0, 0, 0); opacity: 0.2;"><iframe src="index_3.html" style="width: 100%; height: 100%; position: absolute; top: 0px; left: 0px;"></iframe></div></div><div id="onlineService" seed="online-service" data-sourceid="175"><a href="javascript:void(0)" seed="csc_service_hall" style="position: relative; display: inline-block;"><img style="display: block;" src="images/1tdi7nr70h.png"><span title="关闭" class="J-close-online-service-trigger" style="position: absolute; right: 5px; top: -12px; font-size: 14px; background: rgb(238, 238, 238) none repeat scroll 0% 0%; padding: 1px 2px; border-radius: 3px; font-family: simsun; line-height: normal; color: rgb(172, 89, 63);" seed="pcportal_close_icon_trigger">×</span></a></div></body>
</html>
