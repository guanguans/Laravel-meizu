<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta charset="utf-8">
    <title>会员中心-地址管理</title>
    <meta name="description" content="会员中心，地址管理">
    <meta name="keywords" content="魅族官方在线商店、魅族在线商城、魅族官网在线商店、魅族商城、魅族、魅族手机">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="images/favicon.ico" rel="icon" type="image/x-icon">
    <!-- common css -->
    
    <!--[if lt IE 9]>
    <script>
        var c = ["log","debug","info","warn","exception","assert","dir","dirxml","trace","group","groupCollapsed","groupEnd","profile","profileEnd","count","clear","time","timeEnd","timeStamp","table","error"];
        window.console = {};
        for(var i = 0; i < c.length; i++){
            window.console[c[i]] = function(){

            }
        }
    </script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://store.res.meizu.com/resources/php/store/static/common/lib/html5shiv/dist/html5shiv.min.js"></script>
    <![endif]-->
    
    

<link rel="stylesheet" type="text/css" href="css/address.css" media="all">
</head>
<body>
<!-- common header -->
<div class="site-topbar clearfix">
    <div class="mzcontainer">
        <div class="topbar-nav">
        <a href="http://www.meizu.com/" target="_top" data-mtype="store_index_yt_1" data-mdesc="页头中第1个">魅族官网</a>
        <a href="http://store.meizu.com/index.html" data-mtype="store_index_yt_2" data-mdesc="页头中第2个">魅族商城</a>
        <a href="http://www.flyme.cn/" target="_top" data-mtype="store_index_yt_3" data-mdesc="页头中第3个">Flyme</a>
        <a href="http://retail.meizu.com/index.html" target="_top" data-mtype="store_index_yt_4" data-mdesc="页头中第4个">专卖店</a>
        <a href="http://service.meizu.com/index.html" target="_top" data-mtype="store_index_yt_5" data-mdesc="页头中第5个">服务</a>
        <a href="http://bbs.meizu.cn/" target="_top" data-mtype="store_index_yt_6" data-mdesc="页头中第6个">社区</a>
        </div>
        <div class="topbar-right">
            <ul class="topbar-info">
                <li style="display: list-item;" class="topbar-info-msg" id="MzTopbarMsg">
                    <a class="topbar-link" href="http://me.meizu.com/member/message/index" target="_top">消息</a>
                    <span style="display: inline;" class="msg-tag" id="MzMsgTag"></span>
                </li>
                <li>
                    <a class="topbar-link" href="http://me.meizu.com/member/favorite/index" target="_top">我的收藏<div class="topbar-new">new</div></a>
                </li>
                <li class="topbar-order-msg">
                    <a class="topbar-link" href="http://ordercenter.meizu.com/list/index.html" target="_top">我的订单</a>
                    <span style="display: inline;" class="msg-tag" id="MzOrderMsgTag"></span>
                </li>
                <li style="display: none;" class="mz_login">
                    <a class="topbar-link site-login" href="https://login.flyme.cn/vCodeLogin?sid=unionlogin&amp;service=store&amp;autodirct=true&amp;useruri=http://store.meizu.com/member/login.htm?useruri=http://store.meizu.com" data-href="https://login.flyme.cn/vCodeLogin?sid=unionlogin&amp;service=store&amp;autodirct=true&amp;useruri=http://store.meizu.com/member/login.htm?useruri=">登录</a>
                </li>
                <li style="display: none;" class="mz_login">
                    <a class="topbar-link" href="https://member.meizu.com/register" target="_top">注册</a>
                </li>
                <li class="topbar-info-member" style="">
                    <a class="topbar-link" href="http://me.meizu.com/member/index" target="_top">
                        <span id="MzUserName" class="site-member-name">用户515029636</span>的商城
                    </a>
                    <div class="site-member-items">
                        <a class="site-member-link" href="http://me.meizu.com/member/address/index" target="_top" data-mtype="store_index_yt_9_1" data-mdesc="我的商城下拉框1">地址管理</a>
                        <a class="site-member-link" href="http://me.meizu.com/member/repo_ticket/index" target="_top" data-mtype="store_index_yt_9_2" data-mdesc="我的商城下拉框2">我的回购券</a>
                        <a class="site-member-link" href="http://me.meizu.com/member/advice/index" target="_top" data-mtype="store_index_yt_9_3" data-mdesc="我的商城下拉框3">问题反馈</a>
                        <a class="site-member-link site-logout" href="http://store.meizu.com/member/logout.htm?useruri=http://me.meizu.com/member/address/index" data-href="http://store.meizu.com/member/logout.htm?useruri=" data-mtype="store_index_yt_9_4" data-mdesc="我的商城下拉框4">退出</a>
                    </div>
                </li>
            </ul>
            <div class="topbar-info-pop"><div id="MzMsgPop"><a class="title" href="http://hd.meizu.com/sale/nationalday.html?rc=xx" target="_top" data-mtype="store_index_yt_qp" data-mdesc="气泡">魅蓝手机百元优惠限量领取</a><div class="triangle"></div><div class="close"></div></div></div>
        </div>
    </div>
</div><div class="site-header">
    <div class="mzcontainer">
        <div class="header-logo">
            <a href="http://www.meizu.com/" target="_top">
                <img src="images/logo-header.png" srcset="http://store.res.meizu.com/resources/php/store/images/logo-header@2x.png 2x" alt="魅族科技" data-mtype="store_index_dh_logo" data-mdesc="logo" height="20" width="115">
            </a>
        </div>
        <div class="header-nav">
            <ul class="nav-list">
                    <li class="nav-item">
                        <a class="nav-item-link" href="javascrip:" onclick="return false" target="_top">PRO手机</a>
                        <div class="nav-item-children">
                            <ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_pro6.html?rc=sd" target="_top" data-mtype="store_index_dh_1_1" data-mdesc="导航中第1个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0E/CnQOjVeE2E-AAGZCABHUf4HwKyw117_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族PRO 6</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥2499</span>
                                                    ¥
                                                    <span>2299</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_pro5.html?rc=sd" target="_top" data-mtype="store_index_dh_1_2" data-mdesc="导航中第1个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://storeimg.meizu.com/product/1450928403@126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族PRO 5</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>2199</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-link" href="javascrip:" onclick="return false" target="_top">魅蓝手机</a>
                        <div class="nav-item-children">
                            <ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_u10.html?rc=sd" target="_top" data-mtype="store_index_dh_2_1" data-mdesc="导航中第2个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/6C/Cix_s1fodIqAQprmAAjwkwqC8nw622_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝U10</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>999</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_max1.html" target="_top" data-mtype="store_index_dh_2_2" data-mdesc="导航中第2个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/5C/CnQOjVfZBeOAIVRDAAscQInnNPY084_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 Max</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1699</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilanU20.html?rc=sd" target="_top" data-mtype="store_index_dh_2_3" data-mdesc="导航中第2个下第3个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/3F/Cix_s1fDkdyAb0YGAEeDe0CxIV8257_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 U20</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1099</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_e1.html?rc=sd" target="_top" data-mtype="store_index_dh_2_4" data-mdesc="导航中第2个下第4个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/2C/CnQOjVeq0uGASgUFAAzQ2opb7qI013_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 E</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1299</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan3s.html?rc=sd" target="_top" data-mtype="store_index_dh_2_5" data-mdesc="导航中第2个下第5个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/26/Cix_s1epgR6AD8KbAA2l_exLROk404_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝3s</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>699</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_note3.html?rc=sd" target="_top" data-mtype="store_index_dh_2_6" data-mdesc="导航中第2个下第6个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://storeimg.meizu.com/product/1459927797@126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 note3</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>799</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-link" href="javascrip:" onclick="return false" target="_top">MX手机</a>
                        <div class="nav-item-children">
                            <ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/mx6.html?rc=sd" target="_top" data-mtype="store_index_dh_3_1" data-mdesc="导航中第3个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/19/Cix_s1eN3IiASxVXAA9IpQ8-shg169_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族MX6</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1999</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_mx5.html?rc=sd" target="_top" data-mtype="store_index_dh_3_2" data-mdesc="导航中第3个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/40/CnQOjVfD9pWAS6VZAAutvljEfx8425_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族MX5</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1499</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_mx5e.html?rc=sd" target="_top" data-mtype="store_index_dh_3_3" data-mdesc="导航中第3个下第3个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/4F/Cix_s1fNIqaAPDEJAAutvljEfx8334_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族MX5e 经典版</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1499</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-link" href="javascrip:" onclick="return false" target="_top">精选配件</a>
                        <div class="nav-item-children">
                            <ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_ep51.html?skuid=1122" target="_top" data-mtype="store_index_dh_4_1" data-mdesc="导航中第4个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFrxyAQWpEAAEcHQ8zBWo729_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族 EP51蓝牙运动耳机</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>269</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_hd50.html?skuid=839" target="_top" data-mtype="store_index_dh_4_2" data-mdesc="导航中第4个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0E/CnQOjVeFsA2AMvwBAATZrGgT1ak941_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族 HD50 头戴式耳机</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>399</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_bag.html" target="_top" data-mtype="store_index_dh_4_3" data-mdesc="导航中第4个下第3个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/6E/CnQOjVfqPd-AEjICAAVBTlzLU4U578_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族 休闲旅行双肩包</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>199</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/mpower_m8e.html?skuid=1061" target="_top" data-mtype="store_index_dh_4_4" data-mdesc="导航中第4个下第4个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFsDmAYpT5AAKdyPUHRpQ307_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族移动电源（标准版）</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>99</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/lifeme_bts30.html" target="_top" data-mtype="store_index_dh_4_5" data-mdesc="导航中第4个下第5个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/66/CnQOjVfiK1qABWWMAAULoxmuYLI896_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族 Lifeme BTS30 蓝牙音箱</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>399</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                                    <li class="menu-product-more">
                                        <div class="menu-more-links">
                                            <ul>
                                                    <li class="menu-more-row"><a href="http://lists.meizu.com/page/list?categoryid=79&amp;rc=sdsd" target="_top"><img src="images/1467696166-40112.png" class="menu-more-img" height="28" width="28">耳机 / 音箱</a>
                                                    </li>
                                                    <li class="menu-more-row"><a href="http://lists.meizu.com/page/list?categoryid=80&amp;rc=sd" target="_top"><img src="images/1467696197-95413.png" class="menu-more-img" height="28" width="28">路由器 / 移动电源</a>
                                                    </li>
                                                    <li class="menu-more-row"><a href="http://lists.meizu.com/page/list?categoryid=81&amp;rc=sd" target="_top"><img src="images/1467696220-57637.png" class="menu-more-img" height="28" width="28">保护套 / 后盖 / 贴膜</a>
                                                    </li>
                                                    <li class="menu-more-row"><a href="http://lists.meizu.com/page/list?categoryid=82&amp;rc=sd" target="_top"><img src="images/1467696242-24236.png" class="menu-more-img" height="28" width="28">数据线 / 电源适配器</a>
                                                    </li>
                                                    <li class="menu-more-row"><a href="http://lists.meizu.com/page/list?categoryid=83&amp;rc=sd" target="_top"><img src="images/1467705893-97644.png" class="menu-more-img" height="28" width="28">周边配件</a>
                                                    </li>
                                            </ul>
                                        </div>
                                    </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-link" href="javascrip:" onclick="return false" target="_top">智能硬件</a>
                        <div class="nav-item-children">
                            <ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_luyouqi.html?skuid=175" target="_top" data-mtype="store_index_dh_5_1" data-mdesc="导航中第5个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/56/CnQOjVfQCbCAdFjzAAL9OUJBRPk818_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族智能路由器</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>99</span>
                                                        起
                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/XunLei_TB01.html?skuid=1780" target="_top" data-mtype="store_index_dh_5_2" data-mdesc="导航中第5个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/49/CnQOjVfI2aKAQBNbAAI9fvIkJl0379_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">迅雷下载宝 TB01</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥159</span>
                                                    ¥
                                                    <span>99</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/lexin_mambo.html" target="_top" data-mtype="store_index_dh_5_3" data-mdesc="导航中第5个下第3个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/68/CnQOjVfiPf-ASw6XAAX6eBRrgp8709_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">乐心运动手环mambo</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥99</span>
                                                    ¥
                                                    <span>89</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/lefan_jingdian.html?skuid=380" target="_top" data-mtype="store_index_dh_5_4" data-mdesc="导航中第5个下第4个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/58/Cix_s1fWIo6AY06tAAMbEySAYxc161_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">乐范魔力贴（经典版）</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>299</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/BTP_2185.html" target="_top" data-mtype="store_index_dh_5_5" data-mdesc="导航中第5个下第5个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/6E/Cix_s1fqIsOAeUInAAWX1-z6F08803_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">北通阿修罗游戏手柄</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥158</span>
                                                    ¥
                                                    <span>148</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/ROMOSS_A10.html?skuid=1766" target="_top" data-mtype="store_index_dh_5_6" data-mdesc="导航中第5个下第6个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/6E/CnQOjVfqI1aARrMfAACVRHk1HFM020_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">罗马仕AC10快充适配器</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥39</span>
                                                    ¥
                                                    <span>36</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                            </ul>
                        </div>
                    </li>
            </ul>
        </div>

        <div class="header-cart" id="MzHeaderCart">
            <a href="http://store.meizu.com/cart" target="_top">
                <div class="header-cart-wrap">
                    <span class="header-cart-icon"></span>
                    购物车
                    <span id="MzHeaderCartNum" class="header-cart-num" data-extcls="existence">0</span>
                    <div class="header-cart-spacer"></div>
                </div>
            </a>
            <div class="header-cart-detail"><div class="header-cart-empty" data-load="正在加载购物车信息 ..." data-empty="购物车还没有商品，快购买吧！">购物车还没有商品，快购买吧！</div>
</div>
        </div>
    </div>
    <div style="" id="MzNavMenu" class="header-nav-menu">
        <div class="mzcontainer"><ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_u10.html?rc=sd" target="_top" data-mtype="store_index_dh_2_1" data-mdesc="导航中第2个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/cix_s1fodiqaqprmaajwkwqc8nw622_126x126.png" data-src="http://open.file.meizu.com/group1/M00/00/6C/Cix_s1fodIqAQprmAAjwkwqC8nw622_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝U10</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>999</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_max1.html" target="_top" data-mtype="store_index_dh_2_2" data-mdesc="导航中第2个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/cnqojvfzbeoaivrdaascqinnnpy084_126x126.png" data-src="http://open.file.meizu.com/group1/M00/00/5C/CnQOjVfZBeOAIVRDAAscQInnNPY084_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 Max</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1699</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilanU20.html?rc=sd" target="_top" data-mtype="store_index_dh_2_3" data-mdesc="导航中第2个下第3个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/cix_s1fdkdyab0ygaeede0cxiv8257_126x126.png" data-src="http://open.file.meizu.com/group1/M00/00/3F/Cix_s1fDkdyAb0YGAEeDe0CxIV8257_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 U20</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1099</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_e1.html?rc=sd" target="_top" data-mtype="store_index_dh_2_4" data-mdesc="导航中第2个下第4个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/cnqojveq0ugasgufaazq2opb7qi013_126x126.png" data-src="http://open.file.meizu.com/group1/M00/00/2C/CnQOjVeq0uGASgUFAAzQ2opb7qI013_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 E</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1299</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan3s.html?rc=sd" target="_top" data-mtype="store_index_dh_2_5" data-mdesc="导航中第2个下第5个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/cix_s1epgr6ad8kbaa2l_exlrok404_126x126.png" data-src="http://open.file.meizu.com/group1/M00/00/26/Cix_s1epgR6AD8KbAA2l_exLROk404_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝3s</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>699</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_note3.html?rc=sd" target="_top" data-mtype="store_index_dh_2_6" data-mdesc="导航中第2个下第6个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/1459927797@126x126.png" data-src="http://storeimg.meizu.com/product/1459927797@126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 note3</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>799</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                            </ul></div>
    </div>
</div>

<div class="store-wrap">
    <div class="crumbs">
        <a href="http://store.meizu.com/">首页&gt;</a>
        <a href="http://me.meizu.com/member/index">我的商城&gt;</a>
        <a href="http://me.meizu.com/member/address/index" class="active">地址管理</a>
    </div>
    <div class="main clearfix">
    <div class="left-nav f-fl">
        <div class="nav-main">
            <a href="javascript:;" class="type-title"><i class="iconfont icon-orderCenter"></i>订单中心</a>
            <a href="http://ordercenter.meizu.com/list/index.html" class="ml ">我的订单</a>
            <a href="http://me.meizu.com/member/repo/index" class="ml ">我的回购单</a>
            <a href="http://insurance.meizu.com/list/insurance.html" class="ml ">我的意外保</a>
            <a href="javascript:;" class="type-title"><i class="iconfont icon-selfCenter"></i>个人中心</a>
            <a href="http://me.meizu.com/member/address/index" class="ml active">地址管理</a>
            <a href="http://me.meizu.com/member/favorite/index" class="ml ">我的收藏</a>
            <a href="http://me.meizu.com/member/message/index" class="ml ">消息提醒</a>
            <a href="http://me.meizu.com/member/advice/index" class="ml ">建议反馈</a>
            <a href="javascript:;" class="type-title"><i class="iconfont icon-moneyCenter"></i>资产中心</a>
            <a href="http://me.meizu.com/member/coupon/index" class="ml ">我的优惠券</a>
            <a href="http://me.meizu.com/member/redenvelop/index" class="ml ">我的红包</a>
            <a href="http://me.meizu.com/member/repo_ticket/index" class="ml ">我的回购券</a>
            <a href="javascript:;" class="type-title"><i class="iconfont icon-serverCenter"></i>服务中心</a>
            <a href="http://store.meizu.com/member/returned/index" class="ml" style="width: 105px;">退款/退换货跟踪</a>
            <a href="http://me.meizu.com/member/service/insurance" class="ml ">意外保</a>
            <a href="http://me.meizu.com/member/service/recovery" class="ml ">以旧换新</a>
        </div>
    </div>
        <div class="right-content f-fr">
            <div class="address-main">
                <div class="main">
                    <div class="row">
                        <span class="title rollit">新增收货地址</span>
                        <span class="gray">（您目前已有地址<i class="already">1</i>个，最多还可以增加<i class="settle">9</i>个）</span>
                    </div>
                    <form action="http://me.meizu.com/member/address/index" id="valid-form">
                        <!-- 修改地址时要用的地址id -->
                        <input class="addressId" value="" type="hidden">

                        <div class="content">
                            <div class="row namePhone clearfix">
                                <div class="f-fl addressName">
                                    <span>收货人姓名<i class="mark">*</i></span>
                                    <input value="" id="name" class="varify" name="receiver" placeholder="长度不超过12个字" maxlength="12" type="text">
                                </div>
                                <div class="f-fl addressPhone">
                                    <span>收货人手机号<i class="mark">*</i></span>
                                    <input value="" id="phone" name="telNumber" class="varify" placeholder="请输入11位手机号" maxlength="11" type="text">
                                </div>
                            </div>
                            <div class="row clearfix receverAddress">
                                <span class="f-fl">收货人地址<i class="mark">*</i></span>
                                <div class="mzui_select mzui_select_province f-fl">
                                    <input value="" readonly="true" class="mzui_select_trigger province" placeholder="省/直辖市" type="text">
                                    <select class="provinceId" name="provinceId">
                                        <option value="" style="display: none;"></option>
                                                <option value="2911">北京</option>
                                                <option value="14848">天津</option>
                                                <option value="7450">吉林省</option>
                                                <option value="8452">辽宁省</option>
                                                <option value="16860">浙江省</option>
                                                <option value="1">福建省</option>
                                                <option value="16624">上海</option>
                                                <option value="18482">广东省</option>
                                                <option value="21612">海南省</option>
                                                <option value="41143">湖北省</option>
                                                <option value="42634">湖南省</option>
                                                <option value="36793">安徽省</option>
                                                <option value="15121">江苏省</option>
                                                <option value="1214">江西省</option>
                                                <option value="27304">四川省</option>
                                                <option value="35714">重庆</option>
                                                <option value="34187">云南省</option>
                                                <option value="23366">贵州省</option>
                                                <option value="20238">广西壮族自治区</option>
                                                <option value="32166">西藏自治区</option>
                                                <option value="11241">山东省</option>
                                                <option value="38498">河南省</option>
                                                <option value="21894">甘肃省</option>
                                                <option value="3251">河北省</option>
                                                <option value="10088">内蒙古自治区</option>
                                                <option value="25024">宁夏回族自治区</option>
                                                <option value="25305">青海省</option>
                                                <option value="13289">山西省</option>
                                                <option value="25761">陕西省</option>
                                                <option value="32939">新疆维吾尔自治区</option>
                                                <option value="5700">黑龙江省</option>
                                    </select>
                                </div>
                                <div class="mzui_select mzui_select_city f-fl">
                                    <input value="" readonly="true" class="mzui_select_trigger city" placeholder="城市" type="text">
                                    <select class="cityId" name="cityId">
                                    </select>
                                </div>
                                <div class="mzui_select mzui_select_area f-fl">
                                    <input value="" readonly="true" class="mzui_select_trigger area" placeholder="区/县" type="text">
                                    <select class="areaId" name="areaId">
                                    </select>
                                </div>
                                <div class="mzui_select mzui_select_town f-fl">
                                    <input value="" readonly="true" class="mzui_select_trigger town" placeholder="乡镇/街道" type="text">
                                    <select class="townId" name="townId">
                                    </select>
                                </div>
                                <div class="mzui_select_other hide">
                                    <input value="" class="otherAddress _otherAddress" placeholder="其他" type="text">
                                </div>
                            </div>
                            <div class="row  detailAddress">
                                <span>详细地址<i class="mark">*&nbsp;&nbsp;&nbsp;</i></span>
                                <input value="" id="detailAddress" name="address" class="varify" placeholder="请输入不超过50个字的详细地址，例如：路名，门牌号" maxlength="50" type="text">
                            </div>
                            <div class="opreat">
                                <label for="default">
                                    <input id="default" class="setDefault" name="isDefault" type="checkbox">设为默认
                                </label>
                                <a href="javascript:;" class="saveAddress">保存</a>
                            </div>
                        </div>
                    </form>
					
                    <div class="list">
                        <div class="row">
                            <div class="title">
                                <div>已有地址</div>
                                <div class="upload hide">
                                    <i class="iconfont icon-zhuyi"></i>
                                    <span>因配送地址库数据升级，请点击地址进行升级，让您享受更好的配送服务！</span>
                                </div>
                            </div>
                        </div>
                        <div class="listHead">
                            <span class="center w15">收货人姓名</span><span class="center w35">收货人地址</span><span class="center w25">收货人手机号</span><span class="center w10">操作</span>
                        </div>
                        <ul class="" id="tableList"><li>
    <input class="addressId" value="5902191194000" type="hidden">
    <input class="isOld" value="0" type="hidden">

    <span class="center w15">yzm</span>
    <span class="completeAddress center w35">北京北京市昌平区回龙观地区育荣教育园区</span>
    <span class="center w25">18303417457</span>
    <span class="center w10">
      <a class="edit" href="javascript:;">修改</a>
      <a class="delete" href="javascript:;">删除</a>
    </span>
    <span class="left w15">
        <a class="beDefault hide" href="javascript:;">设为默认</a>
    </span>
</li>
</ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- common footer -->

<div class="site-footer">
    <div class="mzcontainer">
        <div class="site-footer-service">
            <ul class="clearfix">
                <li class="service-item">
                  <span class="service-icon service-icon-seven"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">7天</span>
                    <span class="service-desc-normal">无理由退货</span>
                  </p>
                </li>
                <li class="service-split-line">
                  <span></span>
                </li>
                <li class="service-item">
                  <span class="service-icon service-icon-fifty"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">15天</span>
                    <span class="service-desc-normal">换货保障</span>
                  </p>
                </li>
                <li class="service-split-line">
                  <span></span>
                </li>
                <li class="service-item">
                  <span class="service-icon service-icon-one"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">1年</span>
                    <span class="service-desc-normal">免费保修</span>
                  </p>
                </li>
                <li class="service-split-line">
                  <span></span>
                </li>
                <li class="service-item">
                  <span class="service-icon service-icon-speed"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">百城</span>
                    <span class="service-desc-normal">速达</span>
                  </p>
                </li>
                <li class="service-split-line">
                  <span></span>
                </li>
                <li class="service-item">
                  <span class="service-icon service-icon-by"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">全场</span>
                    <span class="service-desc-normal">包邮</span>
                  </p>
                </li>
                <li class="service-split-line">
                  <span></span>
                </li>
                <li class="service-item">
                  <span class="service-icon service-icon-map"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">2000多家</span>
                    <span class="service-desc-normal">专卖店</span>
                  </p>
                </li>
            </ul>
        </div>
        <div class="site-footer-navs clearfix">
            <div class="footer-nav-item">
                <h4 class="footer-nav-title">帮助说明</h4>
                <ul>
                    <li><a href="http://store.meizu.com/explain/payment_help.html" target="_top">支付方式</a></li>
                    <li><a href="http://store.meizu.com/explain/deliverynote.html" target="_top">配送说明</a></li>
                    <li><a href="http://store.meizu.com/explain/warranty_services.html" target="_top">售后服务</a></li>
                    <li><a href="http://store.meizu.com/explain/payment_helps.html" target="_top">付款帮助</a></li>
                </ul>
            </div>
            <div class="footer-nav-item">
                <h4 class="footer-nav-title">Flyme</h4>
                <ul>
                    <li><a target="_top" href="https://open.flyme.cn/">开放平台</a></li>
                    <li><a target="_top" href="http://www.flyme.cn/firmware.jsp">固件下载</a></li>
                    <li><a target="_top" href="http://app.flyme.cn/">软件商店</a></li>
                    <li><a target="_top" href="http://finder.flyme.cn/">查找手机</a></li>
                </ul>
            </div>
            <div class="footer-nav-item">
                <h4 class="footer-nav-title">关于我们</h4>
                <ul>
                    <li><a target="_top" href="http://www.meizu.com/about.html">关于魅族</a></li>
                    <li><a target="_top" href="http://hr.meizu.com/">加入我们</a></li>
                    <li><a target="_top" href="http://www.meizu.com/contact.html">联系我们</a></li>
                    <li><a target="_top" href="http://www.meizu.com/legalStatement.html">法律声明</a></li>
                </ul>
            </div>
            <div class="footer-nav-item">
                <h4 class="footer-nav-title">关注我们</h4>
                <ul>
                    <li><a target="_top" href="http://weibo.com/meizumobile">新浪微博</a></li>
                    <li><a target="_top" href="http://t.qq.com/meizu_tech">腾讯微博</a></li>
                    <li><a target="_top" href="http://user.qzone.qq.com/2762957059">QQ空间</a></li>
                    <li>
                        <a class="meizu-footer-wechat">
                            官方微信
                            <img src="images/weixin.png" alt="微信二维码">
                        </a>
                    </li>
                </ul>
            </div>
            <div class="footer-nav-custom">
                <h4 class="nav-custom-title">24小时全国服务热线</h4>
                <a href="tel:400-788-3333"><h3 class="nav-custom-number">400-788-3333</h3></a>
                <a class="nav-custom-btn" href="http://live-i.meizu.com/live800/chatClient/chatbox.jsp?companyID=8957&amp;configID=4&amp;enterurl=">
                    <img src="images/20x21xiaoshi.png" height="21" width="20">
                    24小时在线客服
                </a>
            </div>
        </div>
        <div class="site-footer-end">
            <p>
                ©2016 Meizu Telecom Equipment Co., Ltd. All rights reserved.
                <a target="_top" href="http://www.miitbeian.gov.cn/" hidefocus="true">备案号：粤ICP备13003602号-2</a>
                <a target="_top" href="http://www2.res.meizu.com/zh_cn/images/common/icp2.jpg" hidefocus="true">经营许可证编号：粤B2-20130198</a>
                <a target="_top" href="http://www2.res.meizu.com/zh_cn/images/common/com_licence.jpg" hidefocus="true">营业执照</a>
                <a target="_top" rel="nofollow" href="http://210.76.65.188/" hidefocus="true">
                    <img src="images/footer-copy-1.png">
                </a>
                <a target="_top" rel="nofollow" href="http://210.76.65.188/webrecord/innernet/Welcome.jsp?bano=4404013010531" hidefocus="true">
                    <img src="footer-copy-2.png">
                </a>
                <a target="_top" rel="nofollow" href="https://credit.szfw.org/CX20151204012550820380.html" hidefocus="true">
                    <img src="images/trust-icon.png">
                </a>
            </p>
        </div>
    </div>
</div>



    
    
    <div class="mzui_select_content" id="mzuiSelect-provinceId"><ul><li class="mzui_select_item mzui_select_item_selected" data-value="" data-zipcode="undefined"></li><li class="mzui_select_item" data-value="2911" data-zipcode="undefined">北京</li><li class="mzui_select_item" data-value="14848" data-zipcode="undefined">天津</li><li class="mzui_select_item" data-value="7450" data-zipcode="undefined">吉林省</li><li class="mzui_select_item" data-value="8452" data-zipcode="undefined">辽宁省</li><li class="mzui_select_item" data-value="16860" data-zipcode="undefined">浙江省</li><li class="mzui_select_item" data-value="1" data-zipcode="undefined">福建省</li><li class="mzui_select_item" data-value="16624" data-zipcode="undefined">上海</li><li class="mzui_select_item" data-value="18482" data-zipcode="undefined">广东省</li><li class="mzui_select_item" data-value="21612" data-zipcode="undefined">海南省</li><li class="mzui_select_item" data-value="41143" data-zipcode="undefined">湖北省</li><li class="mzui_select_item" data-value="42634" data-zipcode="undefined">湖南省</li><li class="mzui_select_item" data-value="36793" data-zipcode="undefined">安徽省</li><li class="mzui_select_item" data-value="15121" data-zipcode="undefined">江苏省</li><li class="mzui_select_item" data-value="1214" data-zipcode="undefined">江西省</li><li class="mzui_select_item" data-value="27304" data-zipcode="undefined">四川省</li><li class="mzui_select_item" data-value="35714" data-zipcode="undefined">重庆</li><li class="mzui_select_item" data-value="34187" data-zipcode="undefined">云南省</li><li class="mzui_select_item" data-value="23366" data-zipcode="undefined">贵州省</li><li class="mzui_select_item" data-value="20238" data-zipcode="undefined">广西壮族自治区</li><li class="mzui_select_item" data-value="32166" data-zipcode="undefined">西藏自治区</li><li class="mzui_select_item" data-value="11241" data-zipcode="undefined">山东省</li><li class="mzui_select_item" data-value="38498" data-zipcode="undefined">河南省</li><li class="mzui_select_item" data-value="21894" data-zipcode="undefined">甘肃省</li><li class="mzui_select_item" data-value="3251" data-zipcode="undefined">河北省</li><li class="mzui_select_item" data-value="10088" data-zipcode="undefined">内蒙古自治区</li><li class="mzui_select_item" data-value="25024" data-zipcode="undefined">宁夏回族自治区</li><li class="mzui_select_item" data-value="25305" data-zipcode="undefined">青海省</li><li class="mzui_select_item" data-value="13289" data-zipcode="undefined">山西省</li><li class="mzui_select_item" data-value="25761" data-zipcode="undefined">陕西省</li><li class="mzui_select_item" data-value="32939" data-zipcode="undefined">新疆维吾尔自治区</li><li class="mzui_select_item" data-value="5700" data-zipcode="undefined">黑龙江省</li></ul></div><div class="mzui_select_content" id="mzuiSelect-cityId"><ul></ul></div><div class="mzui_select_content" id="mzuiSelect-areaId"><ul></ul></div><div class="mzui_select_content" id="mzuiSelect-townId"><ul></ul></div>
    <!--common js-->







</body>
</html>
