<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta charset="utf-8">
    <title>确认订单-魅族商城</title>
    <meta name="description" content="魅族商城是魅族面向全国服务的官方电子商务平台,提供魅族PRO系列、魅族MX系列和魅蓝系列等产品的预约和购买.官方正品,全国联保.">
    <meta name="keywords" content="魅族官方在线商店、魅族在线商城、魅族官网在线商店、魅族商城">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="favicon.ico" rel="icon" type="image/x-icon">
    <!-- common css -->
    
    <!--[if lt IE 9]>
    <script>
        var c = ["log","debug","info","warn","exception","assert","dir","dirxml","trace","group","groupCollapsed","groupEnd","profile","profileEnd","count","clear","time","timeEnd","timeStamp","table","error"];
        window.console = {};
        for(var i = 0; i < c.length; i++){
            window.console[c[i]] = function(){

            }
        }
    </script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://store.res.meizu.com/resources/php/store/static/common/lib/html5shiv/dist/html5shiv.min.js"></script>
    <![endif]-->
	

<link rel="stylesheet" type="text/css" href="css/indentSccess.css" media="all">
</head>
<body>
<!-- common header -->
<div class="site-topbar clearfix">
    <div class="mzcontainer">
        <div class="topbar-nav">
        <a href="http://www.meizu.com/" target="_top" data-mtype="store_index_yt_1" data-mdesc="页头中第1个">魅族官网</a>
        <a href="http://store.meizu.com/index.html" data-mtype="store_index_yt_2" data-mdesc="页头中第2个">魅族商城</a>
        <a href="http://www.flyme.cn/" target="_top" data-mtype="store_index_yt_3" data-mdesc="页头中第3个">Flyme</a>
        <a href="http://retail.meizu.com/index.html" target="_top" data-mtype="store_index_yt_4" data-mdesc="页头中第4个">专卖店</a>
        <a href="http://service.meizu.com/index.html" target="_top" data-mtype="store_index_yt_5" data-mdesc="页头中第5个">服务</a>
        <a href="http://bbs.meizu.cn/" target="_top" data-mtype="store_index_yt_6" data-mdesc="页头中第6个">社区</a>
        </div>
        <div class="topbar-right">
            <ul class="topbar-info">
                <li style="display: list-item;" class="topbar-info-msg" id="MzTopbarMsg">
                    <a class="topbar-link" href="http://me.meizu.com/member/message/index" target="_top">消息</a>
                    <span style="display: inline;" class="msg-tag" id="MzMsgTag"></span>
                </li>
                <li>
                    <a class="topbar-link" href="http://me.meizu.com/member/favorite/index" target="_top">我的收藏<div class="topbar-new">new</div></a>
                </li>
                <li class="topbar-order-msg">
                    <a class="topbar-link" href="http://ordercenter.meizu.com/list/index.html" target="_top">我的订单</a>
                    <span style="display: inline;" class="msg-tag" id="MzOrderMsgTag"></span>
                </li>
                <li style="display: none;" class="mz_login">
                    <a class="topbar-link site-login" href="https://login.flyme.cn/vCodeLogin?sid=unionlogin&amp;service=store&amp;autodirct=true&amp;useruri=http://store.meizu.com/member/login.htm?useruri=http://store.meizu.com" data-href="https://login.flyme.cn/vCodeLogin?sid=unionlogin&amp;service=store&amp;autodirct=true&amp;useruri=http://store.meizu.com/member/login.htm?useruri=">登录</a>
                </li>
                <li style="display: none;" class="mz_login">
                    <a class="topbar-link" href="https://member.meizu.com/register" target="_top">注册</a>
                </li>
                <li class="topbar-info-member" style="">
                    <a class="topbar-link" href="http://me.meizu.com/member/index" target="_top">
                        <span id="MzUserName" class="site-member-name">用户515029636</span>的商城
                    </a>
                    <div class="site-member-items">
                        <a class="site-member-link" href="http://me.meizu.com/member/address/index" target="_top" data-mtype="store_index_yt_9_1" data-mdesc="我的商城下拉框1">地址管理</a>
                        <a class="site-member-link" href="http://me.meizu.com/member/repo_ticket/index" target="_top" data-mtype="store_index_yt_9_2" data-mdesc="我的商城下拉框2">我的回购券</a>
                        <a class="site-member-link" href="http://me.meizu.com/member/advice/index" target="_top" data-mtype="store_index_yt_9_3" data-mdesc="我的商城下拉框3">问题反馈</a>
                        <a class="site-member-link site-logout" href="http://store.meizu.com/member/logout.htm?useruri=http://ordercenter.meizu.com/order/add.html" data-href="http://store.meizu.com/member/logout.htm?useruri=" data-mtype="store_index_yt_9_4" data-mdesc="我的商城下拉框4">退出</a>
                    </div>
                </li>
            </ul>
            <div class="topbar-info-pop"><div id="MzMsgPop"><a class="title" href="http://hd.meizu.com/sale/nationalday.html?rc=xx" target="_top" data-mtype="store_index_yt_qp" data-mdesc="气泡">魅蓝手机百元优惠限量领取</a><div class="triangle"></div><div class="close"></div></div></div>
        </div>
    </div>
</div>
<div class="site-header">
    <div class="mzcontainer">
        <div class="header-logo">
            <a href="http://www.meizu.com/" target="_top">
                <img src="images/logo-header.png" srcset="http://store.res.meizu.com/resources/php/store/images/logo-header@2x.png 2x" alt="魅族科技" data-mtype="store_index_dh_logo" data-mdesc="logo" height="20" width="115">
            </a>
        </div>
        <div class="header-nav">
            <ul class="nav-list">
                    <li class="nav-item">
                        <a class="nav-item-link" href="javascrip:" onclick="return false" target="_top">PRO手机</a>
                        <div class="nav-item-children">
                            <ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_pro6.html?rc=sd" target="_top" data-mtype="store_index_dh_1_1" data-mdesc="导航中第1个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0E/CnQOjVeE2E-AAGZCABHUf4HwKyw117_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族PRO 6</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥2499</span>
                                                    ¥
                                                    <span>2299</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_pro5.html?rc=sd" target="_top" data-mtype="store_index_dh_1_2" data-mdesc="导航中第1个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://storeimg.meizu.com/product/1450928403@126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族PRO 5</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>2199</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-link" href="javascrip:" onclick="return false" target="_top">魅蓝手机</a>
                        <div class="nav-item-children">
                            <ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_u10.html?rc=sd" target="_top" data-mtype="store_index_dh_2_1" data-mdesc="导航中第2个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/6C/Cix_s1fodIqAQprmAAjwkwqC8nw622_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝U10</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>999</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_max1.html" target="_top" data-mtype="store_index_dh_2_2" data-mdesc="导航中第2个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/5C/CnQOjVfZBeOAIVRDAAscQInnNPY084_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 Max</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1699</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilanU20.html?rc=sd" target="_top" data-mtype="store_index_dh_2_3" data-mdesc="导航中第2个下第3个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/3F/Cix_s1fDkdyAb0YGAEeDe0CxIV8257_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 U20</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1099</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_e1.html?rc=sd" target="_top" data-mtype="store_index_dh_2_4" data-mdesc="导航中第2个下第4个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/2C/CnQOjVeq0uGASgUFAAzQ2opb7qI013_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 E</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1299</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan3s.html?rc=sd" target="_top" data-mtype="store_index_dh_2_5" data-mdesc="导航中第2个下第5个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/26/Cix_s1epgR6AD8KbAA2l_exLROk404_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝3s</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>699</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_note3.html?rc=sd" target="_top" data-mtype="store_index_dh_2_6" data-mdesc="导航中第2个下第6个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://storeimg.meizu.com/product/1459927797@126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 note3</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>799</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-link" href="javascrip:" onclick="return false" target="_top">MX手机</a>
                        <div class="nav-item-children">
                            <ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/mx6.html?rc=sd" target="_top" data-mtype="store_index_dh_3_1" data-mdesc="导航中第3个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/19/Cix_s1eN3IiASxVXAA9IpQ8-shg169_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族MX6</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1999</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_mx5.html?rc=sd" target="_top" data-mtype="store_index_dh_3_2" data-mdesc="导航中第3个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/40/CnQOjVfD9pWAS6VZAAutvljEfx8425_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族MX5</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1499</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_mx5e.html?rc=sd" target="_top" data-mtype="store_index_dh_3_3" data-mdesc="导航中第3个下第3个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/4F/Cix_s1fNIqaAPDEJAAutvljEfx8334_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族MX5e 经典版</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1499</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-link" href="javascrip:" onclick="return false" target="_top">精选配件</a>
                        <div class="nav-item-children">
                            <ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_ep51.html?skuid=1122" target="_top" data-mtype="store_index_dh_4_1" data-mdesc="导航中第4个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFrxyAQWpEAAEcHQ8zBWo729_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族 EP51蓝牙运动耳机</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>269</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_hd50.html?skuid=839" target="_top" data-mtype="store_index_dh_4_2" data-mdesc="导航中第4个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0E/CnQOjVeFsA2AMvwBAATZrGgT1ak941_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族 HD50 头戴式耳机</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>399</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_bag.html" target="_top" data-mtype="store_index_dh_4_3" data-mdesc="导航中第4个下第3个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/6E/CnQOjVfqPd-AEjICAAVBTlzLU4U578_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族 休闲旅行双肩包</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>199</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/mpower_m8e.html?skuid=1061" target="_top" data-mtype="store_index_dh_4_4" data-mdesc="导航中第4个下第4个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFsDmAYpT5AAKdyPUHRpQ307_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族移动电源（标准版）</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>99</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/lifeme_bts30.html" target="_top" data-mtype="store_index_dh_4_5" data-mdesc="导航中第4个下第5个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/66/CnQOjVfiK1qABWWMAAULoxmuYLI896_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族 Lifeme BTS30 蓝牙音箱</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>399</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                                    <li class="menu-product-more">
                                        <div class="menu-more-links">
                                            <ul>
                                                    <li class="menu-more-row"><a href="http://lists.meizu.com/page/list?categoryid=79&amp;rc=sdsd" target="_top"><img src="images/1467696166-40112.png" class="menu-more-img" height="28" width="28">耳机 / 音箱</a>
                                                    </li>
                                                    <li class="menu-more-row"><a href="http://lists.meizu.com/page/list?categoryid=80&amp;rc=sd" target="_top"><img src="images/1467696197-95413.png" class="menu-more-img" height="28" width="28">路由器 / 移动电源</a>
                                                    </li>
                                                    <li class="menu-more-row"><a href="http://lists.meizu.com/page/list?categoryid=81&amp;rc=sd" target="_top"><img src="images/1467696220-57637.png" class="menu-more-img" height="28" width="28">保护套 / 后盖 / 贴膜</a>
                                                    </li>
                                                    <li class="menu-more-row"><a href="http://lists.meizu.com/page/list?categoryid=82&amp;rc=sd" target="_top"><img src="images/1467696242-24236.png" class="menu-more-img" height="28" width="28">数据线 / 电源适配器</a>
                                                    </li>
                                                    <li class="menu-more-row"><a href="http://lists.meizu.com/page/list?categoryid=83&amp;rc=sd" target="_top"><img src="images/1467705893-97644.png" class="menu-more-img" height="28" width="28">周边配件</a>
                                                    </li>
                                            </ul>
                                        </div>
                                    </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-link" href="javascrip:" onclick="return false" target="_top">智能硬件</a>
                        <div class="nav-item-children">
                            <ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_luyouqi.html?skuid=175" target="_top" data-mtype="store_index_dh_5_1" data-mdesc="导航中第5个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/56/CnQOjVfQCbCAdFjzAAL9OUJBRPk818_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族智能路由器</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>99</span>
                                                        起
                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/XunLei_TB01.html?skuid=1780" target="_top" data-mtype="store_index_dh_5_2" data-mdesc="导航中第5个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/49/CnQOjVfI2aKAQBNbAAI9fvIkJl0379_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">迅雷下载宝 TB01</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥159</span>
                                                    ¥
                                                    <span>99</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/lexin_mambo.html" target="_top" data-mtype="store_index_dh_5_3" data-mdesc="导航中第5个下第3个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/68/CnQOjVfiPf-ASw6XAAX6eBRrgp8709_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">乐心运动手环mambo</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥99</span>
                                                    ¥
                                                    <span>89</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/lefan_jingdian.html?skuid=380" target="_top" data-mtype="store_index_dh_5_4" data-mdesc="导航中第5个下第4个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/58/Cix_s1fWIo6AY06tAAMbEySAYxc161_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">乐范魔力贴（经典版）</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>299</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/BTP_2185.html" target="_top" data-mtype="store_index_dh_5_5" data-mdesc="导航中第5个下第5个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/6E/Cix_s1fqIsOAeUInAAWX1-z6F08803_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">北通阿修罗游戏手柄</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥158</span>
                                                    ¥
                                                    <span>148</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/ROMOSS_A10.html?skuid=1766" target="_top" data-mtype="store_index_dh_5_6" data-mdesc="导航中第5个下第6个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/6E/CnQOjVfqI1aARrMfAACVRHk1HFM020_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">罗马仕AC10快充适配器</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥39</span>
                                                    ¥
                                                    <span>36</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                            </ul>
                        </div>
                    </li>
            </ul>
        </div>

        <div class="header-cart" id="MzHeaderCart">
            <a href="http://store.meizu.com/cart" target="_top">
                <div class="header-cart-wrap">
                    <span class="header-cart-icon"></span>
                    购物车
                    <span id="MzHeaderCartNum" class="header-cart-num" data-extcls="existence">0</span>
                    <div class="header-cart-spacer"></div>
                </div>
            </a>
            <div class="header-cart-detail"><div class="header-cart-empty" data-load="正在加载购物车信息 ..." data-empty="购物车还没有商品，快购买吧！">购物车还没有商品，快购买吧！</div>
</div>
        </div>
    </div>
    <div style="" id="MzNavMenu" class="header-nav-menu">
        <div class="mzcontainer"><ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_u10.html?rc=sd" target="_top" data-mtype="store_index_dh_2_1" data-mdesc="导航中第2个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/cix_s1fodiqaqprmaajwkwqc8nw622_126x126.png" data-src="http://open.file.meizu.com/group1/M00/00/6C/Cix_s1fodIqAQprmAAjwkwqC8nw622_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝U10</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>999</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_max1.html" target="_top" data-mtype="store_index_dh_2_2" data-mdesc="导航中第2个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/cnqojvfzbeoaivrdaascqinnnpy084_126x126.png" data-src="http://open.file.meizu.com/group1/M00/00/5C/CnQOjVfZBeOAIVRDAAscQInnNPY084_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 Max</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1699</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilanU20.html?rc=sd" target="_top" data-mtype="store_index_dh_2_3" data-mdesc="导航中第2个下第3个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/cix_s1fdkdyab0ygaeede0cxiv8257_126x126.png" data-src="http://open.file.meizu.com/group1/M00/00/3F/Cix_s1fDkdyAb0YGAEeDe0CxIV8257_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 U20</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1099</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_e1.html?rc=sd" target="_top" data-mtype="store_index_dh_2_4" data-mdesc="导航中第2个下第4个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/cnqojveq0ugasgufaazq2opb7qi013_126x126.png" data-src="http://open.file.meizu.com/group1/M00/00/2C/CnQOjVeq0uGASgUFAAzQ2opb7qI013_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 E</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1299</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan3s.html?rc=sd" target="_top" data-mtype="store_index_dh_2_5" data-mdesc="导航中第2个下第5个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/cix_s1epgr6ad8kbaa2l_exlrok404_126x126.png" data-src="http://open.file.meizu.com/group1/M00/00/26/Cix_s1epgR6AD8KbAA2l_exLROk404_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝3s</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>699</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_note3.html?rc=sd" target="_top" data-mtype="store_index_dh_2_6" data-mdesc="导航中第2个下第6个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/1459927797@126x126.png" data-src="http://storeimg.meizu.com/product/1459927797@126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 note3</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>799</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                            </ul></div>
    </div>
</div>

    <div class="order">
        <div class="mzcontainer order-header clearfix">
            <div class="order-title">确认订单</div>
            <ul class="order-bread clearfix">
                <li class="order-bread-module active">购物车</li>
                <li class="order-bread-module active">确认订单</li>
                <li class="order-bread-module">在线支付</li>
                <li class="order-bread-module">完成</li>
            </ul>
        </div>
        <div class="mzcontainer order-address" id="addressModule">
            <div class="order-address-title">
                收货人信息
                <div class="order-address-title-tips" id="orderAddressHasOldTips">
                    <i class="order-address-tips-icon"></i>因配送地址库升级，部分地址需升级后方可使用！
                </div>
            </div>
            <ul class="order-address-list clearfix" id="addressList"><li class="order-address-checkbox checked"><div class="order-address-checkbox-top"><div class="order-address-checkbox-name" title="yzm">yzm</div><div class="order-address-checkbox-phone">18303417457</div></div><div class="order-address-checkbox-content">北京北京市昌平区回龙观地区育荣教育园区</div><div class="order-address-checkbox-checked"><div class="order-address-checkbox-tick"></div></div><div class="order-address-checkbox-ctrl"><div class="order-address-checkbox-delete"></div><div class="order-address-checkbox-edit"></div></div></li></ul>
            <div style="display: block;" class="order-address-add" id="addressAdd">
                <div class="order-address-add-icon" id="addressOpenBtn">
                    <div class="order-address-add-horizontal"></div>
                    <div class="order-address-add-vertical"></div>
                </div>
                <div class="order-address-form" id="addressForm">
                    <div class="order-address-row clearfix">
                        <div class="order-address-row-title">收件人</div>
                        <div class="order-address-row-content"><input value="" class="order-address-input" placeholder="长度不超过12个字" maxlength="12" id="addressFormName" type="text"></div>
                        <div class="order-address-row-tips"></div>
                    </div>
                    <div class="order-address-row clearfix">
                        <div class="order-address-row-title">手机</div>
                        <div class="order-address-row-content"><input value="" class="order-address-input" placeholder="请输入11位手机号" maxlength="11" id="addressFormPhone" type="text"></div>
                        <div class="order-address-row-tips"></div>
                    </div>
                    <div class="order-address-row clearfix">
                        <div class="order-address-row-title">地址</div>
                        <div class="order-address-row-content mz-citys" id="citys"><div class="mz-selectmenu"><input value="" placeholder="省/直辖市" readonly="readonly" tabindex="-1" type="text"><input name="" value="" tabindex="-1" type="hidden"></div><div class="mz-selectmenu"><input value="" placeholder="城市" readonly="readonly" tabindex="-1" type="text"><input name="" value="" tabindex="-1" type="hidden"></div><div class="mz-selectmenu"><input value="" placeholder="区/县" readonly="readonly" tabindex="-1" type="text"><input name="" value="" tabindex="-1" type="hidden"></div><div class="mz-selectmenu"><input value="" placeholder="乡镇/街道" readonly="readonly" tabindex="-1" type="text"><input name="" value="" tabindex="-1" type="hidden"></div><div style="display: none;" class="mz-selectmenu other"><input value="" name="" placeholder="其他" maxlength="10" type="text"></div></div>
                        <div class="order-address-row-tips"></div>
                    </div>
                    <div class="order-address-row clearfix">
                        <div class="order-address-row-title"></div>
                        <div class="order-address-row-content"><span class="order-address-prefix" data-prefix="" id="addressFormPrefix"></span><input value="" class="order-address-input" placeholder="请输入不超过50个字的详细地址，例如：路名、门牌号" maxlength="50" id="addressFormDetail" type="text"></div>
                        <div class="order-address-row-tips"></div>
                    </div>
                    <div class="order-address-row clearfix">
                        <div class="order-address-row-title"></div>
                        <div class="order-address-row-content">
                            <div class="btn" id="addressFormSave">保存并使用</div>
                            <div class="btn cancel" id="addressFormCancel">取消</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mzcontainer order-product">
            <div class="order-product-title">确认订单信息</div>
            <div class="order-product-list" id="orderProductList"><table cellpadding="0" cellspacing="0"><thead><tr><th class="order-product-table-name"><div class="order-product-supplier">供应商：<span class="order-product-supplier-name">魅族</span><div class="order-product-supplier-tips">由“魅族”负责发货，并提供售后服务</div></div></th><th class="order-product-table-price">单价</th><th class="order-product-table-num">数量</th><th class="order-product-table-total">小计</th><th class="order-product-table-express">配送方式</th></tr></thead><tbody><tr><td class="order-product-table-name"><img class="order-product-image" src="images/cnqojveqmcgay9eyaathg2nryac032.png"><div class="order-product-name"><a target="_top" class="order-product-link" href="http://detail.meizu.com/item/meilan_e1.html">魅蓝 E<br>全网通公开版 月光银 32GB</a></div></td><td class="order-product-table-price"><p></p><span class="order-product-price">1299.00</span></td><td class="order-product-table-num">1</td><td class="order-product-table-total"><p class="order-product-price red">1299.00</p></td><td rowspan="1" class="order-product-table-express"><p>快递配送：运费<span class="order-product-price red">0.00</span></p><div class="order-product-arrival"><p><i class="order-product-arrival-icon"></i>次日达</p><p>19:00前下单并支付，</p><p>预计<strong>明天（09月29日）</strong>送达</p></div></td></tr></tbody><tfoot><tr><td colspan="5"><div class="order-product-info"><div class="order-product-invoice clearfix"><div class="order-product-invoice-type">发票类型：电子发票<div class="order-product-invoice-icon"></div></div><div class="order-product-invoice-title">发票抬头：默认为收货人姓名</div></div><div class="order-product-remark"><span class="order-product-remark-title">备注</span><textarea class="order-product-remark-input" placeholder="备注..." maxlength="45"></textarea></div></div><div class="order-product-total">合计：<span class="order-product-price red">1299.00</span></div></td></tr></tfoot></table></div>
        </div>
        <div class="mzcontainer order-discount" id="discount">
            <div class="order-discount-header">使用优惠\抵扣</div>
            <ul class="order-discount-list" id="discountList"><li class="order-discount-item"><div class="order-discount-item-header"><div class="order-discount-item-title">使用回购券</div><div class="order-discount-item-right"></div></div><div class="order-discount-item-content order-discount-repo"><div class="order-discout-repo-text">已选择<span class="order-discount-repo-selected-num">0</span>张回购券（可多张叠加使用），总抵现<span class="order-discount-repo-selected-price">0.00</span></div><ul class="order-discount-repo-list"></ul><p><span class="order-discount-repo-enter-title">点击输入回购券</span></p><div class="order-discount-repo-enter-content"><div class="order-discount-repo-enter-row">券号：<input value="" class="order-discount-repo-enter-input" type="text">密码：<input class="order-discount-repo-enter-input" type="password"><div class="btn">确定</div><span class="order-discount-repo-tips"></span></div><ul class="order-discount-repo-list"></ul></div></div></li></ul>
        </div>
        <div class="mzcontainer order-total clearfix">
            <div class="order-total-content">
                <div class="order-total-row">
                    总金额
                    <div class="order-total-price" id="totalOrderPrice">1299.00</div>
                </div>
                <div class="order-total-row order-total-discount" id="totalCouponRow">
                    优惠券优惠
                    <div class="order-total-price sub">0.00</div>
                </div>
                <div class="order-total-row order-total-discount" id="totalRedRow">
                    红包抵扣
                    <div class="order-total-price sub">0.00</div>
                </div>
                <div class="order-total-row order-total-discount" id="totalRepoRow">
                    回购券抵扣
                    <div class="order-total-price sub">0.00</div>
                </div>
                <div class="order-total-row">
                    运费
                    <div class="order-total-price" id="totalExpressPrice">0.00</div>
                </div>
                <div class="order-total-line"></div>
                <div class="order-total-row">
                    应付：
                    <div class="order-total-price total" id="totalPayPrice">1299.00</div>
                </div>
                <div class="order-total-row order-total-valid-row">
                    <input value="" class="order-total-valid-input" placeholder="验证码*" maxlength="4" id="validInput" type="text">
                    <img src="images/get" class="order-total-valid-image" id="validCode">
                </div>
                <div class="order-total-row">
                    <div class="btn" id="submitForm">提交订单</div>
                </div>
            </div>
        </div>
    </div>
    <div class="mask einvoice" id="einvoiceDetail">
        <div class="einvoice-detail">
            <div class="einvoice-detail-title">电子发票详细介绍</div>
            <div class="einvoice-detail-main">
                <ul class="einvoice-detail-list">
                    <li class="q">为什么要选择电子发票？</li>
                    <li class="a">电子发票是纳税义务人通过互联网录入数据，然后开具给客户。电子发票获取更便捷，可重复下载，避免丢失，方便查验，售后维权更有保障。发货效率更高，退换货更方便！电子发票低碳环保，节能减排，符合加强环境保护的国家策略。选择电子发票，省力省心，更能为环境保护贡献一份力。</li>
                    <li class="q">确保电子发票具有法律效力的相关规定有哪些?</li>
                    <li class="a">根据国家税务总局关于推行通过增值税电子发票系统开具的增值税电子普通发票有关问题的公告（国家税务总局公告2015年第84号）的规定，自2016年1月1日起启用。增值税电子普通发票的开票方和受票方需要纸质发票的，可以自行打印增值税电子普通发票的版式文件，其法律效力、基本用途、基本使用规定等与税务机关监制的增值税普通发票相同。</li>
                    <li class="q">怎样收到电子发票？</li>
                    <li class="a">2016年1月1日及之后发货的订单，电子发票将在订单发货后，由系统自动发送到收货人的手机，请留意查收；如未收到，请联系客服热线:400-788-3333或进入官网咨询：<a class="einvoice-detail-link" href="http://live-i.meizu.com/live800/chatClient/chatbox.jsp?companyID=8957&amp;configID=4&amp;enterurl=http://store.meizu.com/explain/einvoice_help.html&amp;pagereferrer=&amp;info=&amp;k=1" target="_top">在线客服</a>，无需操作后台；2016年1月1日前发货的订单，可在官网商城用户订单详情中查看发票信息。</li>
                    <li class="q">想要传统纸质发票怎么办？</li>
                    <li class="a">魅族商城正式启用电子发票后，不支持自主更换纸质发票。</li>
                    <li class="q">电子发票短信误删了怎么办？</li>
                    <li class="a">用户可联系客服索取电子发票信息，客服热线:400-788-3333或进入官网咨询：<a class="einvoice-detail-link" href="http://live-i.meizu.com/live800/chatClient/chatbox.jsp?companyID=8957&amp;configID=4&amp;enterurl=http://store.meizu.com/explain/einvoice_help.html&amp;pagereferrer=&amp;info=&amp;k=1" target="_top">在线客服</a>。</li>
                    <li class="q">电子发票是否可以用于报销？</li>
                    <li class="a">电子发票打印后可用于单位报销。</li>
                    <li class="q">电子发票在哪里下载和打印？</li>
                    <li class="a">2016年1月1日及之后发货的订单，电子发票用户可以登录<a class="einvoice-detail-link" href="http://t.cn/R4cdnja" target="_top">http://t.cn/R4cdnja</a> 凭借短信信息查询电票发票。 2016年1月1日前发货的订单，电子发票依旧在<a class="einvoice-detail-link" href="http://t.cn/Rzm3kyC" target="_top">http://t.cn/Rzm3kyC</a> 查询。</li>
                    <li class="q">选择开具电子发票的顾客如何办理退换货流程？</li>
                    <li class="a">和原有退换货流程一样，唯一区别是电子发票的用户在办理退换货时无需用户退回电子发票，我司直接按电子发票冲红处理。</li>
                    <li class="q">电子发票可以修改发票抬头吗？</li>
                    <li class="a">目前电子发票抬头默认为收货人姓名，如需修改，请联系客服热线:400-788-3333或进入官网咨询：<a class="einvoice-detail-link" href="http://live-i.meizu.com/live800/chatClient/chatbox.jsp?companyID=8957&amp;configID=4&amp;enterurl=http://store.meizu.com/explain/einvoice_help.html&amp;pagereferrer=&amp;info=&amp;k=1" target="_top">在线客服</a>。</li>
                </ul>
            </div>
            <div class="einvoice-close"></div>
        </div>
    </div>





    
    <div class="mz-downmenu"></div><div class="mz-downmenu"></div><div class="mz-downmenu"></div><div class="mz-downmenu"></div>
<!-- end content -->

<!-- common footer -->

<div class="site-footer">
    <div class="mzcontainer">
        <div class="site-footer-service">
            <ul class="clearfix">
                <li class="service-item">
                  <span class="service-icon service-icon-seven"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">7天</span>
                    <span class="service-desc-normal">无理由退货</span>
                  </p>
                </li>
                <li class="service-split-line">
                  <span></span>
                </li>
                <li class="service-item">
                  <span class="service-icon service-icon-fifty"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">15天</span>
                    <span class="service-desc-normal">换货保障</span>
                  </p>
                </li>
                <li class="service-split-line">
                  <span></span>
                </li>
                <li class="service-item">
                  <span class="service-icon service-icon-one"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">1年</span>
                    <span class="service-desc-normal">免费保修</span>
                  </p>
                </li>
                <li class="service-split-line">
                  <span></span>
                </li>
                <li class="service-item">
                  <span class="service-icon service-icon-speed"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">百城</span>
                    <span class="service-desc-normal">速达</span>
                  </p>
                </li>
                <li class="service-split-line">
                  <span></span>
                </li>
                <li class="service-item">
                  <span class="service-icon service-icon-by"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">全场</span>
                    <span class="service-desc-normal">包邮</span>
                  </p>
                </li>
                <li class="service-split-line">
                  <span></span>
                </li>
                <li class="service-item">
                  <span class="service-icon service-icon-map"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">2000多家</span>
                    <span class="service-desc-normal">专卖店</span>
                  </p>
                </li>
            </ul>
        </div>
        <div class="site-footer-navs clearfix">
            <div class="footer-nav-item">
                <h4 class="footer-nav-title">帮助说明</h4>
                <ul>
                    <li><a href="http://store.meizu.com/explain/payment_help.html" target="_top">支付方式</a></li>
                    <li><a href="http://store.meizu.com/explain/deliverynote.html" target="_top">配送说明</a></li>
                    <li><a href="http://store.meizu.com/explain/warranty_services.html" target="_top">售后服务</a></li>
                    <li><a href="http://store.meizu.com/explain/payment_helps.html" target="_top">付款帮助</a></li>
                </ul>
            </div>
            <div class="footer-nav-item">
                <h4 class="footer-nav-title">Flyme</h4>
                <ul>
                    <li><a target="_top" href="https://open.flyme.cn/">开放平台</a></li>
                    <li><a target="_top" href="http://www.flyme.cn/firmware.jsp">固件下载</a></li>
                    <li><a target="_top" href="http://app.flyme.cn/">软件商店</a></li>
                    <li><a target="_top" href="http://finder.flyme.cn/">查找手机</a></li>
                </ul>
            </div>
            <div class="footer-nav-item">
                <h4 class="footer-nav-title">关于我们</h4>
                <ul>
                    <li><a target="_top" href="http://www.meizu.com/about.html">关于魅族</a></li>
                    <li><a target="_top" href="http://hr.meizu.com/">加入我们</a></li>
                    <li><a target="_top" href="http://www.meizu.com/contact.html">联系我们</a></li>
                    <li><a target="_top" href="http://www.meizu.com/legalStatement.html">法律声明</a></li>
                </ul>
            </div>
            <div class="footer-nav-item">
                <h4 class="footer-nav-title">关注我们</h4>
                <ul>
                    <li><a target="_top" href="http://weibo.com/meizumobile">新浪微博</a></li>
                    <li><a target="_top" href="http://t.qq.com/meizu_tech">腾讯微博</a></li>
                    <li><a target="_top" href="http://user.qzone.qq.com/2762957059">QQ空间</a></li>
                    <li>
                        <a class="meizu-footer-wechat">
                            官方微信
                            <img src="images/weixin.png" alt="微信二维码">
                        </a>
                    </li>
                </ul>
            </div>
            <div class="footer-nav-custom">
                <h4 class="nav-custom-title">24小时全国服务热线</h4>
                <a href="tel:400-788-3333"><h3 class="nav-custom-number">400-788-3333</h3></a>
                <a class="nav-custom-btn" href="http://live-i.meizu.com/live800/chatClient/chatbox.jsp?companyID=8957&amp;configID=4&amp;enterurl=">
                    <img src="images/20x21xiaoshi.png" height="21" width="20">
                    24小时在线客服
                </a>
            </div>
        </div>
        <div class="site-footer-end">
            <p>
                ©2016 Meizu Telecom Equipment Co., Ltd. All rights reserved.
                <a target="_top" href="http://www.miitbeian.gov.cn/" hidefocus="true">备案号：粤ICP备13003602号-2</a>
                <a target="_top" href="http://www2.res.meizu.com/zh_cn/images/common/icp2.jpg" hidefocus="true">经营许可证编号：粤B2-20130198</a>
                <a target="_top" href="http://www2.res.meizu.com/zh_cn/images/common/com_licence.jpg" hidefocus="true">营业执照</a>
                <a target="_top" rel="nofollow" href="http://210.76.65.188/" hidefocus="true">
                    <img src="images/footer-copy-1.png">
                </a>
                <a target="_top" rel="nofollow" href="http://210.76.65.188/webrecord/innernet/Welcome.jsp?bano=4404013010531" hidefocus="true">
                    <img src="footer-copy-2.png">
                </a>
                <a target="_top" rel="nofollow" href="https://credit.szfw.org/CX20151204012550820380.html" hidefocus="true">
                    <img src="images/trust-icon.png">
                </a>
            </p>
        </div>
    </div>
</div>









</body>
</html>
