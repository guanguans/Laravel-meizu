<!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">

    <meta charset="utf-8">
    <title>魅族商城-提供魅族 PRO 系列、MX系列、魅蓝note系列、魅蓝metal系列的手机，配件和智能硬件的预约和购买。</title>
    <meta name="description" content="魅族商城是魅族面向全国服务的官方电商平台，提供魅族 PRO 系列、MX系列、魅蓝note系列和魅蓝metal系列的手机，配件和智能硬件的预约和购买。官方正品，全国联保，全场包邮，7天无理由退货，15天换货保障。">
    <meta name="keywords" content="魅族官方在线商店、魅族在线商城、魅族官网在线商店、魅族商城、魅族、魅族手机">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="images/favicon.ico" rel="icon" type="image/x-icon">
    <!-- common css -->
    
    <!--[if lt IE 9]>
    <script>
        var c = ["log","debug","info","warn","exception","assert","dir","dirxml","trace","group","groupCollapsed","groupEnd","profile","profileEnd","count","clear","time","timeEnd","timeStamp","table","error"];
        window.console = {};
        for(var i = 0; i < c.length; i++){
            window.console[c[i]] = function(){

            }
        }
    </script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://store.res.meizu.com/resources/php/store/static/common/lib/html5shiv/dist/html5shiv.min.js"></script>
    <![endif]-->


<link media="all" href="css/index.css" type="text/css" rel="stylesheet">
</head>
<body>
<!-- common header -->
<div class="site-topbar clearfix">
    <div class="mzcontainer">
        <div class="topbar-nav">
        <a href="http://www.meizu.com/" target="_top" data-mtype="store_index_yt_1" data-mdesc="页头中第1个">魅族官网</a>
        <a href="http://store.meizu.com/index.html" data-mtype="store_index_yt_2" data-mdesc="页头中第2个">魅族商城</a>
        <a href="http://www.flyme.cn/" target="_top" data-mtype="store_index_yt_3" data-mdesc="页头中第3个">Flyme</a>
        <a href="http://retail.meizu.com/index.html" target="_top" data-mtype="store_index_yt_4" data-mdesc="页头中第4个">专卖店</a>
        <a href="http://service.meizu.com/index.html" target="_top" data-mtype="store_index_yt_5" data-mdesc="页头中第5个">服务</a>
        <a href="http://bbs.meizu.cn/" target="_top" data-mtype="store_index_yt_6" data-mdesc="页头中第6个">社区</a>
        </div>
        <div class="topbar-right">
            <ul class="topbar-info">
                <li class="topbar-info-msg" id="MzTopbarMsg">
                    <a class="topbar-link" href="http://me.meizu.com/member/message/index" target="_top">消息</a>
                    <span class="msg-tag" id="MzMsgTag"></span>
                </li>
                <li>
                    <a class="topbar-link" href="http://me.meizu.com/member/favorite/index" target="_top">我的收藏<div class="topbar-new">new</div></a>
                </li>
                <li class="topbar-order-msg">
                    <a class="topbar-link" href="http://ordercenter.meizu.com/list/index.html" target="_top">我的订单</a>
                    <span class="msg-tag" id="MzOrderMsgTag"></span>
                </li>
                <li class="mz_login">
                    <a class="topbar-link site-login" href="https://login.flyme.cn/vCodeLogin?sid=unionlogin&amp;service=store&amp;autodirct=true&amp;useruri=http://store.meizu.com/member/login.htm?useruri=http://store.meizu.com/index.html" data-href="https://login.flyme.cn/vCodeLogin?sid=unionlogin&amp;service=store&amp;autodirct=true&amp;useruri=http://store.meizu.com/member/login.htm?useruri=">登录</a>
                </li>
                <li class="mz_login">
                    <a class="topbar-link" href="https://member.meizu.com/register" target="_top">注册</a>
                </li>
                <li class="topbar-info-member" style="display: none;">
                    <a class="topbar-link" href="http://me.meizu.com/member/index" target="_top">
                        <span id="MzUserName" class="site-member-name">MEIZU</span>的商城
                    </a>
                    <div class="site-member-items">
                        <a class="site-member-link" href="http://me.meizu.com/member/address/index" target="_top" data-mtype="store_index_yt_9_1" data-mdesc="我的商城下拉框1">地址管理</a>
                        <a class="site-member-link" href="http://me.meizu.com/member/repo_ticket/index" target="_top" data-mtype="store_index_yt_9_2" data-mdesc="我的商城下拉框2">我的回购券</a>
                        <a class="site-member-link" href="http://me.meizu.com/member/advice/index" target="_top" data-mtype="store_index_yt_9_3" data-mdesc="我的商城下拉框3">问题反馈</a>
                        <a class="site-member-link site-logout" href="http://store.meizu.com/member/logout.htm?useruri=http://store.meizu.com" data-href="http://store.meizu.com/member/logout.htm?useruri=" data-mtype="store_index_yt_9_4" data-mdesc="我的商城下拉框4">退出</a>
                    </div>
                </li>
            </ul>
            <div class="topbar-info-pop"></div>
        </div>
    </div>
</div><div class="site-header">
    <div class="mzcontainer">
        <div class="header-logo">
            <a href="http://www.meizu.com/" target="_top">
                <img src="images/logo-header.png" srcset="http://store.res.meizu.com/resources/php/store/images/logo-header@2x.png 2x" alt="魅族科技" data-mtype="store_index_dh_logo" data-mdesc="logo" height="20" width="115">
            </a>
        </div>
        <div class="header-nav">
            <ul class="nav-list">
                    <li class="nav-item">
                        <a class="nav-item-link" href="javascrip:" onclick="return false" target="_top">PRO手机</a>
                        <div class="nav-item-children">
                            <ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_pro6.html?rc=sd" target="_top" data-mtype="store_index_dh_1_1" data-mdesc="导航中第1个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0E/CnQOjVeE2E-AAGZCABHUf4HwKyw117_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族PRO 6</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥2499</span>
                                                    ¥
                                                    <span>2299</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_pro5.html?rc=sd" target="_top" data-mtype="store_index_dh_1_2" data-mdesc="导航中第1个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://storeimg.meizu.com/product/1450928403@126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族PRO 5</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>2199</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-link" href="javascrip:" onclick="return false" target="_top">魅蓝手机</a>
                        <div class="nav-item-children">
                            <ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_u10.html?rc=sd" target="_top" data-mtype="store_index_dh_2_1" data-mdesc="导航中第2个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/6C/Cix_s1fodIqAQprmAAjwkwqC8nw622_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝U10</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>999</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_max1.html" target="_top" data-mtype="store_index_dh_2_2" data-mdesc="导航中第2个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/5C/CnQOjVfZBeOAIVRDAAscQInnNPY084_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 Max</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1699</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilanU20.html?rc=sd" target="_top" data-mtype="store_index_dh_2_3" data-mdesc="导航中第2个下第3个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/3F/Cix_s1fDkdyAb0YGAEeDe0CxIV8257_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 U20</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1099</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_e1.html?rc=sd" target="_top" data-mtype="store_index_dh_2_4" data-mdesc="导航中第2个下第4个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/2C/CnQOjVeq0uGASgUFAAzQ2opb7qI013_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 E</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1299</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan3s.html?rc=sd" target="_top" data-mtype="store_index_dh_2_5" data-mdesc="导航中第2个下第5个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/26/Cix_s1epgR6AD8KbAA2l_exLROk404_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝3s</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>699</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_note3.html?rc=sd" target="_top" data-mtype="store_index_dh_2_6" data-mdesc="导航中第2个下第6个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://storeimg.meizu.com/product/1459927797@126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 note3</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>799</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-link" href="javascrip:" onclick="return false" target="_top">MX手机</a>
                        <div class="nav-item-children">
                            <ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/mx6.html?rc=sd" target="_top" data-mtype="store_index_dh_3_1" data-mdesc="导航中第3个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/19/Cix_s1eN3IiASxVXAA9IpQ8-shg169_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族MX6</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1999</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_mx5.html?rc=sd" target="_top" data-mtype="store_index_dh_3_2" data-mdesc="导航中第3个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/40/CnQOjVfD9pWAS6VZAAutvljEfx8425_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族MX5</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1499</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_mx5e.html?rc=sd" target="_top" data-mtype="store_index_dh_3_3" data-mdesc="导航中第3个下第3个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/4F/Cix_s1fNIqaAPDEJAAutvljEfx8334_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族MX5e 经典版</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1499</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-link" href="javascrip:" onclick="return false" target="_top">精选配件</a>
                        <div class="nav-item-children">
                            <ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_ep51.html?skuid=1122" target="_top" data-mtype="store_index_dh_4_1" data-mdesc="导航中第4个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFrxyAQWpEAAEcHQ8zBWo729_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族 EP51蓝牙运动耳机</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>269</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_hd50.html?skuid=839" target="_top" data-mtype="store_index_dh_4_2" data-mdesc="导航中第4个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0E/CnQOjVeFsA2AMvwBAATZrGgT1ak941_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族 HD50 头戴式耳机</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>399</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_bag.html" target="_top" data-mtype="store_index_dh_4_3" data-mdesc="导航中第4个下第3个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/6E/CnQOjVfqPd-AEjICAAVBTlzLU4U578_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族 休闲旅行双肩包</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>199</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/mpower_m8e.html?skuid=1061" target="_top" data-mtype="store_index_dh_4_4" data-mdesc="导航中第4个下第4个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFsDmAYpT5AAKdyPUHRpQ307_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族移动电源（标准版）</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>99</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/lifeme_bts30.html" target="_top" data-mtype="store_index_dh_4_5" data-mdesc="导航中第4个下第5个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/66/CnQOjVfiK1qABWWMAAULoxmuYLI896_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族 Lifeme BTS30 蓝牙音箱</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>399</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                                    <li class="menu-product-more">
                                        <div class="menu-more-links">
                                            <ul>
                                                    <li class="menu-more-row"><a href="http://lists.meizu.com/page/list?categoryid=79&amp;rc=sdsd" target="_top"><img src="images/1467696166-40112.png" class="menu-more-img" height="28" width="28">耳机 / 音箱</a>
                                                    </li>
                                                    <li class="menu-more-row"><a href="http://lists.meizu.com/page/list?categoryid=80&amp;rc=sd" target="_top"><img src="images/1467696197-95413.png" class="menu-more-img" height="28" width="28">路由器 / 移动电源</a>
                                                    </li>
                                                    <li class="menu-more-row"><a href="http://lists.meizu.com/page/list?categoryid=81&amp;rc=sd" target="_top"><img src="images/1467696220-57637.png" class="menu-more-img" height="28" width="28">保护套 / 后盖 / 贴膜</a>
                                                    </li>
                                                    <li class="menu-more-row"><a href="http://lists.meizu.com/page/list?categoryid=82&amp;rc=sd" target="_top"><img src="images/1467696242-24236.png" class="menu-more-img" height="28" width="28">数据线 / 电源适配器</a>
                                                    </li>
                                                    <li class="menu-more-row"><a href="http://lists.meizu.com/page/list?categoryid=83&amp;rc=sd" target="_top"><img src="images/1467705893-97644.png" class="menu-more-img" height="28" width="28">周边配件</a>
                                                    </li>
                                            </ul>
                                        </div>
                                    </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-link" href="javascrip:" onclick="return false" target="_top">智能硬件</a>
                        <div class="nav-item-children">
                            <ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_luyouqi.html?skuid=175" target="_top" data-mtype="store_index_dh_5_1" data-mdesc="导航中第5个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/56/CnQOjVfQCbCAdFjzAAL9OUJBRPk818_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族智能路由器</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>99</span>
                                                        起
                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/XunLei_TB01.html?skuid=1780" target="_top" data-mtype="store_index_dh_5_2" data-mdesc="导航中第5个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/49/CnQOjVfI2aKAQBNbAAI9fvIkJl0379_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">迅雷下载宝 TB01</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥159</span>
                                                    ¥
                                                    <span>99</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/lexin_mambo.html" target="_top" data-mtype="store_index_dh_5_3" data-mdesc="导航中第5个下第3个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/68/CnQOjVfiPf-ASw6XAAX6eBRrgp8709_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">乐心运动手环mambo</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥99</span>
                                                    ¥
                                                    <span>89</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/lefan_jingdian.html?skuid=380" target="_top" data-mtype="store_index_dh_5_4" data-mdesc="导航中第5个下第4个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/58/Cix_s1fWIo6AY06tAAMbEySAYxc161_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">乐范魔力贴（经典版）</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>299</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/BTP_2185.html" target="_top" data-mtype="store_index_dh_5_5" data-mdesc="导航中第5个下第5个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/6E/Cix_s1fqIsOAeUInAAWX1-z6F08803_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">北通阿修罗游戏手柄</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥158</span>
                                                    ¥
                                                    <span>148</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/ROMOSS_A10.html?skuid=1766" target="_top" data-mtype="store_index_dh_5_6" data-mdesc="导航中第5个下第6个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/6E/CnQOjVfqI1aARrMfAACVRHk1HFM020_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">罗马仕AC10快充适配器</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥39</span>
                                                    ¥
                                                    <span>36</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                            </ul>
                        </div>
                    </li>
            </ul>
        </div>

        <div class="header-cart" id="MzHeaderCart">
            <a href="http://store.meizu.com/cart" target="_top">
                <div class="header-cart-wrap">
                    <span class="header-cart-icon"></span>
                    购物车
                    <span id="MzHeaderCartNum" class="header-cart-num" data-extcls="existence">0</span>
                    <div class="header-cart-spacer"></div>
                </div>
            </a>
            <div class="header-cart-detail"><div class="header-cart-empty" data-load="正在加载购物车信息 ..." data-empty="购物车还没有商品，快购买吧！">购物车还没有商品，快购买吧！</div>
</div>
        </div>
    </div>
    <div id="MzNavMenu" class="header-nav-menu" style="">
        <div class="mzcontainer"><ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_pro6.html?rc=sd" target="_top" data-mtype="store_index_dh_1_1" data-mdesc="导航中第1个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/cnqojvee2e-aagzcabhuf4hwkyw117_126x126.png" data-src="http://open.file.meizu.com/group1/M00/00/0E/CnQOjVeE2E-AAGZCABHUf4HwKyw117_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族PRO 6</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥2499</span>
                                                    ¥
                                                    <span>2299</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_pro5.html?rc=sd" target="_top" data-mtype="store_index_dh_1_2" data-mdesc="导航中第1个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/1450928403@126x126.png" data-src="http://storeimg.meizu.com/product/1450928403@126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族PRO 5</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>2199</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                            </ul></div>
    </div>
</div>
<div class="home-category-wrap">
    <div class="mzcontainer home-category-position">
        <div class="home-category-list">
            <ul class="home-category-nav">
                    <li class="home-category-nav-item">
                        <a class="category-nav-link" data-mtype="store_index_cdh_1" data-mdesc="侧边导航中第1个分类" href="http://lists.meizu.com/page/list?categoryid=73&amp;rc=cb">
                            <span>全部商品分类</span>
                        </a>
                    </li>
                    <li class="home-category-nav-item">
                        <a class="category-nav-link" data-mtype="store_index_cdh_2" data-mdesc="侧边导航中第2个分类" href="http://lists.meizu.com/page/list?categoryid=76&amp;rc=cb">
                            <span>手机</span>
                        </a>
                            <div class="category-nav-children nav-wrap-col-2">
                                <div class="nav-children-wrap">
                                        <ul class="nav-children-left">
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/meilan_u10.html?rc=cd" target="_top" data-mtype="store_index_cdh_2_1" data-mdesc="侧边导航中第2个分类第1个产品">
                                                            <img src="images/cnqojvfodl2aecfuaajwkwqc8nw363_180x180.png" data-src="images/http://open.file.meizu.com/group1/M00/00/6C/CnQOjVfodL2AECfUAAjwkwqC8nw363_180x180.png" alt="魅蓝U10" height="50" width="50">
                                                            <span>魅蓝U10</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/meilan_max1.html?rc=cd" target="_top" data-mtype="store_index_cdh_2_2" data-mdesc="侧边导航中第2个分类第2个产品">
                                                            <img src="images/cix_s1ffqdmaf50oaascqinnnpy988_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/60/Cix_s1ffQDmAf50oAAscQInnNPY988_180x180.png" alt="魅蓝 Max" height="50" width="50">
                                                            <span>魅蓝 Max</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/meilanU20.html?rc=cd" target="_top" data-mtype="store_index_cdh_2_3" data-mdesc="侧边导航中第2个分类第3个产品">
                                                            <img src="images/cnqojvfdkigafu-baeede0cxiv8968_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/3E/CnQOjVfDkiGAFU-BAEeDe0CxIV8968_180x180.png" alt="魅蓝 U20" height="50" width="50">
                                                            <span>魅蓝 U20</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/meilan_e1.html?rc=cd" target="_top" data-mtype="store_index_cdh_2_4" data-mdesc="侧边导航中第2个分类第4个产品">
                                                            <img src="images/cnqojveq03-ag1suaazq2opb7qi306_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/2C/CnQOjVeq03-AG1suAAzQ2opb7qI306_180x180.png" alt="魅蓝 E" height="50" width="50">
                                                            <span>魅蓝 E</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/mx6.html?rc=cd" target="_top" data-mtype="store_index_cdh_2_5" data-mdesc="侧边导航中第2个分类第5个产品">
                                                            <img src="images/cnqojven3n2ap8swaa9ipq8-shg954_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/18/CnQOjVeN3N2AP8SWAA9IpQ8-shg954_180x180.png" alt="魅族MX6" height="50" width="50">
                                                            <span>魅族MX6</span>
                                                        </a>
                                                    </li>
                                        </ul>
                                        <ul class="nav-children-left">
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/meilan3s.html?rc=cd" target="_top" data-mtype="store_index_cdh_2_6" data-mdesc="侧边导航中第2个分类第6个产品">
                                                            <img src="images/cix_s1eyhscarorgaa2l_exlrok267_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/31/Cix_s1eyhSCARORGAA2l_exLROk267_180x180.png" alt="魅蓝3s" height="50" width="50">
                                                            <span>魅蓝3s</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/meilan_note3.html?rc=cd" target="_top" data-mtype="store_index_cdh_2_7" data-mdesc="侧边导航中第2个分类第7个产品">
                                                            <img src="images/1460456848@180x180.png" data-src="http://storeimg.meizu.com/product/1460456848@180x180.png" alt="魅蓝 note3" height="50" width="50">
                                                            <span>魅蓝 note3</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/meilan3.html?rc=cd" target="_top" data-mtype="store_index_cdh_2_8" data-mdesc="侧边导航中第2个分类第8个产品">
                                                            <img src="images/1461569261@180x180.png" data-src="http://storeimg.meizu.com/product/1461569261@180x180.png" alt="魅蓝3" height="50" width="50">
                                                            <span>魅蓝3</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/meilan_metal.html?rc=cd" target="_top" data-mtype="store_index_cdh_2_9" data-mdesc="侧边导航中第2个分类第9个产品">
                                                            <img src="images/cix_s1fd9dqal_o4aabqolcyaco197_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/41/Cix_s1fD9dqAL_O4AAbqolcyaco197_180x180.png" alt="魅蓝 metal" height="50" width="50">
                                                            <span>魅蓝 metal</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/meilan_e_xingji.html?rc=cd" target="_top" data-mtype="store_index_cdh_2_10" data-mdesc="侧边导航中第2个分类第10个产品">
                                                            <img src="images/cix_s1fodpgaox7aaarxgz2dbhg605_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/6C/Cix_s1fodPGAOx7aAARxgZ2dBHg605_180x180.png" alt="魅蓝 E星际迷航版" height="50" width="50">
                                                            <span>魅蓝 E星际迷航版</span>
                                                        </a>
                                                    </li>
                                        </ul>
                                </div>
                                    <div class="category-item-ad">
                                        <a href="http://detail.meizu.com/item/meizu_pro6.html?rc=cda" target="_top" data-mtype="store_index_cdh_2_gg" data-mdesc="侧边导航中第2个分类广告">
                                            <img src="images/cix_s1fjiheatnvjaacvv0hgvja542.jpg" height="480" width="296">
                                        </a>
                                    </div>
                            </div>
                    </li>
                    <li class="home-category-nav-item">
                        <a class="category-nav-link" data-mtype="store_index_cdh_3" data-mdesc="侧边导航中第3个分类" href="http://lists.meizu.com/page/list?categoryid=77&amp;rc=cb">
                            <span>智能硬件</span>
                        </a>
                            <div class="category-nav-children nav-wrap-col-2">
                                <div class="nav-children-wrap">
                                        <ul class="nav-children-left">
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/CILINE_2_5LKP.html?skuid=632" target="_top" data-mtype="store_index_cdh_3_1" data-mdesc="侧边导航中第3个分类第1个产品">
                                                            <img src="images/cix_s1efshoaav_jaapz4wq7lzg676_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFshOAaV_JAAPz4Wq7lzg676_180x180.png" alt="沁麟2.5L智能靠谱煲" height="50" width="50">
                                                            <span>沁麟2.5L智能靠谱煲</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/lefan_AirPet.html?skuid=1055" target="_top" data-mtype="store_index_cdh_3_2" data-mdesc="侧边导航中第3个分类第2个产品">
                                                            <img src="images/cnqojvefsgmar3p2aael_agh-x4620_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/0F/CnQOjVeFsgmAR3p2AAEl_AGH-x4620_180x180.png" alt="乐范AirPet 空气净化器" height="50" width="50">
                                                            <span>乐范AirPet 空气净化器</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/WeLoop_now2.html" target="_top" data-mtype="store_index_cdh_3_3" data-mdesc="侧边导航中第3个分类第3个产品">
                                                            <img src="images/cix_s1e1bw2aww7taamkl1d_i2o290.png" data-src="http://open.file.meizu.com/group1/M00/00/37/Cix_s1e1bw2AWW7TAAMKL1D_I2o290.png" alt="WeLoop唯乐now2智能手环" height="50" width="50">
                                                            <span>WeLoop唯乐now2智能手环</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/lexin_mambo_watch.html?skuid=968" target="_top" data-mtype="store_index_cdh_3_4" data-mdesc="侧边导航中第3个分类第4个产品">
                                                            <img src="images/cix_s1efsjiadguwaasuheqwolm555_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFsjiADguwAASuHeqwOlM555_180x180.png" alt="乐心mambo watch手表" height="50" width="50">
                                                            <span>乐心mambo watch手表</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/lexingtianxia_V5.html?skuid=1228" target="_top" data-mtype="store_index_cdh_3_5" data-mdesc="侧边导航中第3个分类第5个产品">
                                                            <img src="images/cix_s1efskeabxpiaakvorzevao265_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFskeABXpiAAKVoRzEvAo265_180x180.png" alt="乐行天下V5智能电动独轮车" height="50" width="50">
                                                            <span>乐行天下V5智能电动独轮车</span>
                                                        </a>
                                                    </li>
                                        </ul>
                                        <ul class="nav-children-left">
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/GWID_AW.html?skuid=816" target="_top" data-mtype="store_index_cdh_3_6" data-mdesc="侧边导航中第3个分类第6个产品">
                                                            <img src="images/cnqojvefsleawphaaahb3bvbtte050_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/0F/CnQOjVeFsleAWphAAAHb3BVBttE050_180x180.png" alt="公子小白情感智能机器人" height="50" width="50">
                                                            <span>公子小白情感智能机器人</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/Moikit_Cuptime2.html?skuid=1196" target="_top" data-mtype="store_index_cdh_3_7" data-mdesc="侧边导航中第3个分类第7个产品">
                                                            <img src="images/cix_s1efsnaag2b3aairrbwmqse096_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFsnaAG2b3AAIRrbWmQSE096_180x180.png" alt="麦开智能水杯Cuptime2" height="50" width="50">
                                                            <span>麦开智能水杯Cuptime2</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/KK_SS3.html?skuid=1343" target="_top" data-mtype="store_index_cdh_3_8" data-mdesc="侧边导航中第3个分类第8个产品">
                                                            <img src="images/cix_s1en_jyaz4lcaasodywx6i8375_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/19/Cix_s1eN_jyAZ4lcAASOdyWx6i8375_180x180.png" alt="控客立体拓展智能排插主体" height="50" width="50">
                                                            <span>控客立体拓展智能排插主体</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/Goluk_G1.html" target="_top" data-mtype="store_index_cdh_3_9" data-mdesc="侧边导航中第3个分类第9个产品">
                                                            <img src="images/cix_s1fjtpiadz7vaagowe9q8b0266.png" data-src="http://open.file.meizu.com/group1/M00/00/4A/Cix_s1fJTpiAdZ7VAAGowe9Q8b0266.png" alt="极路客G1智能行车记录仪" height="50" width="50">
                                                            <span>极路客G1智能行车记录仪</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/xinwan_Q1.html" target="_top" data-mtype="store_index_cdh_3_10" data-mdesc="侧边导航中第3个分类第10个产品">
                                                            <img src="images/cix_s1epjwoajypuaart0clcdnw175_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/26/Cix_s1epjWOAJypuAART0CLcdNw175_180x180.png" alt="新游智能游戏手柄 新玩Q1" height="50" width="50">
                                                            <span>新游智能游戏手柄 新玩Q1</span>
                                                        </a>
                                                    </li>
                                        </ul>
                                </div>
                                    <div class="category-item-ad">
                                        <a href="http://detail.meizu.com/item/Withings_Activite_pop.html?rc=cb" target="_top" data-mtype="store_index_cdh_3_gg" data-mdesc="侧边导航中第3个分类广告">
                                            <img src="images/cix_s1fog32acgsvaacii2rvmeo909.jpg" height="480" width="296">
                                        </a>
                                    </div>
                            </div>
                    </li>
                    <li class="home-category-nav-item">
                        <a class="category-nav-link" data-mtype="store_index_cdh_4" data-mdesc="侧边导航中第4个分类" href="http://lists.meizu.com/page/list?categoryid=79&amp;rc=cb">
                            <span>耳机 / 音箱</span>
                        </a>
                            <div class="category-nav-children nav-wrap-col-2">
                                <div class="nav-children-wrap">
                                        <ul class="nav-children-left">
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/meizu_ep51.html?skuid=696" target="_top" data-mtype="store_index_cdh_4_1" data-mdesc="侧边导航中第4个分类第1个产品">
                                                            <img src="images/init-1x1_001.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFspmAIXWzAAHOwfpsRUs892_180x180.png" alt="魅族 EP51蓝牙运动耳机" height="50" width="50">
                                                            <span>魅族 EP51蓝牙运动耳机</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/meizu_hd50.html?skuid=141" target="_top" data-mtype="store_index_cdh_4_2" data-mdesc="侧边导航中第4个分类第2个产品">
                                                            <img src="images/init-1x1_001.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0F/CnQOjVeFsqGAD3OVAAPYuKXCruA359_180x180.png" alt="魅族HD50 头戴式耳机" height="50" width="50">
                                                            <span>魅族HD50 头戴式耳机</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/EP31.html?skuid=142" target="_top" data-mtype="store_index_cdh_4_3" data-mdesc="侧边导航中第4个分类第3个产品">
                                                            <img src="images/init-1x1_001.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFsqmAK20iAAG11JKsOMI192_180x180.png" alt="魅族EP-31耳机" height="50" width="50">
                                                            <span>魅族EP-31耳机</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/SE_CL51S.html?skuid=352" target="_top" data-mtype="store_index_cdh_4_4" data-mdesc="侧边导航中第4个分类第4个产品">
                                                            <img src="images/init-1x1_001.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0F/CnQOjVeFsrKAcIrYAAN-FTgo9io848_180x180.png" alt="先锋  SE-CL51S" height="50" width="50">
                                                            <span>先锋  SE-CL51S</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/beats_urbeats.html?skuid=503" target="_top" data-mtype="store_index_cdh_4_5" data-mdesc="侧边导航中第4个分类第5个产品">
                                                            <img src="images/init-1x1_001.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFsruAUeRXAALLD1alUHw460_180x180.png" alt="beats Urbeats 入耳耳机" height="50" width="50">
                                                            <span>beats Urbeats 入耳耳机</span>
                                                        </a>
                                                    </li>
                                        </ul>
                                        <ul class="nav-children-left">
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/Skullcandy_hesh.html?skuid=574" target="_top" data-mtype="store_index_cdh_4_6" data-mdesc="侧边导航中第4个分类第6个产品">
                                                            <img src="images/init-1x1_001.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0F/CnQOjVeFstCAHf9lAAU6wJuHLos523_180x180.png" alt="Skullcandy hesh头戴耳机" height="50" width="50">
                                                            <span>Skullcandy hesh头戴耳机</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/ATH_CLR100.html?skuid=406" target="_top" data-mtype="store_index_cdh_4_7" data-mdesc="侧边导航中第4个分类第7个产品">
                                                            <img src="images/init-1x1_001.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFstiAKsS7AALI3W25870695_180x180.png" alt="铁三角  ATH-CLR100" height="50" width="50">
                                                            <span>铁三角  ATH-CLR100</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/Skullcandy_smokin.html?skuid=593" target="_top" data-mtype="store_index_cdh_4_8" data-mdesc="侧边导航中第4个分类第8个产品">
                                                            <img src="images/init-1x1_001.jpg" data-src="http://open.file.meizu.com/group1/M00/00/28/Cix_s1eqjeGAZKQEAAYLx43w1n0102_180x180.png" alt="Skullcandy smokin buds" height="50" width="50">
                                                            <span>Skullcandy smokin buds</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/FiiO_M3.html?skuid=1247" target="_top" data-mtype="store_index_cdh_4_9" data-mdesc="侧边导航中第4个分类第9个产品">
                                                            <img src="images/init-1x1_001.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0F/CnQOjVeF06mAK7nfAAMJ5sb7-M8169_180x180.png" alt="FiiO飞傲M3无损音乐播放器" height="50" width="50">
                                                            <span>FiiO飞傲M3无损音乐播放器</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/APS_BA202S.html?skuid=1026" target="_top" data-mtype="store_index_cdh_4_10" data-mdesc="侧边导航中第4个分类第10个产品">
                                                            <img src="images/init-1x1_001.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0F/CnQOjVeFsx2ASFAJAAfsM-JmlBA442_180x180.png" alt="先锋APS-BA202S蓝牙音箱" height="50" width="50">
                                                            <span>先锋APS-BA202S蓝牙音箱</span>
                                                        </a>
                                                    </li>
                                        </ul>
                                </div>
                                    <div class="category-item-ad">
                                        <a href="http://detail.meizu.com/item/FiiO_X1.html?rc=cb" target="_top" data-mtype="store_index_cdh_4_gg" data-mdesc="侧边导航中第4个分类广告">
                                            <img src="images/cnqojvfog32aapnoaacj6mhktoy731.jpg" height="480" width="296">
                                        </a>
                                    </div>
                            </div>
                    </li>
                    <li class="home-category-nav-item">
                        <a class="category-nav-link" data-mtype="store_index_cdh_5" data-mdesc="侧边导航中第5个分类" href="http://lists.meizu.com/page/list?categoryid=80&amp;rc=cb">
                            <span>路由器 / 移动电源</span>
                        </a>
                            <div class="category-nav-children nav-wrap-col-1">
                                <div class="nav-children-wrap">
                                        <ul class="nav-children-left">
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/meizu_luyouqi.html?skuid=175" target="_top" data-mtype="store_index_cdh_5_1" data-mdesc="侧边导航中第5个分类第1个产品">
                                                            <img src="images/cix_s1efs7sapfataal9oujbrpk642_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFs7SAPfATAAL9OUJBRPk642_180x180.png" alt="魅族智能路由器" height="50" width="50">
                                                            <span>魅族智能路由器</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/mpower_m8e.html?skuid=1061" target="_top" data-mtype="store_index_cdh_5_2" data-mdesc="侧边导航中第5个分类第2个产品">
                                                            <img src="images/cnqojveftg6ay_fpaakdypuhrpq152_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/0F/CnQOjVeFtG6AY_FpAAKdyPUHRpQ152_180x180.png" alt="魅族移动电源（标准版）" height="50" width="50">
                                                            <span>魅族移动电源（标准版）</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/m10_dianyuan.html?skuid=314" target="_top" data-mtype="store_index_cdh_5_3" data-mdesc="侧边导航中第5个分类第3个产品">
                                                            <img src="images/cix_s1efs8mapfj_aasc1x7hvrg598_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFs8mAPfJ_AASc1X7HVrg598_180x180.png" alt="魅族移动电源 快充版" height="50" width="50">
                                                            <span>魅族移动电源 快充版</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/router_f1.html?skuid=1112" target="_top" data-mtype="store_index_cdh_5_4" data-mdesc="侧边导航中第5个分类第4个产品">
                                                            <img src="images/cix_s1efthmakzjjaabxvrwlvgu637_180x180.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFtHmAKzjJAABXVrwLVgU637_180x180.jpg" alt="魅族路由器 极速版" height="50" width="50">
                                                            <span>魅族路由器 极速版</span>
                                                        </a>
                                                    </li>
                                        </ul>
                                </div>
                                    <div class="category-item-ad">
                                        <a href="http://detail.meizu.com/item/router_f1.html" target="_top" data-mtype="store_index_cdh_5_gg" data-mdesc="侧边导航中第5个分类广告">
                                            <img src="images/1465800410-90918.png" height="480" width="296">
                                        </a>
                                    </div>
                            </div>
                    </li>
                    <li class="home-category-nav-item">
                        <a class="category-nav-link" data-mtype="store_index_cdh_6" data-mdesc="侧边导航中第6个分类" href="http://lists.meizu.com/page/list?categoryid=81&amp;rc=cb">
                            <span>保护套 / 后盖 / 贴膜</span>
                        </a>
                            <div class="category-nav-children nav-wrap-col-2">
                                <div class="nav-children-wrap">
                                        <ul class="nav-children-left">
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/meilan_E_tpu.html" target="_top" data-mtype="store_index_cdh_6_1" data-mdesc="侧边导航中第6个分类第1个产品">
                                                            <img src="images/init-1x1_001.jpg" data-src="http://open.file.meizu.com/group1/M00/00/3D/Cix_s1e7tmqAWncLAAHxAekM4dY735_180x180.png" alt="魅蓝E 纤格纹保护壳" height="50" width="50">
                                                            <span>魅蓝E 纤格纹保护壳</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/mx6_loop.html?" target="_top" data-mtype="store_index_cdh_6_2" data-mdesc="侧边导航中第6个分类第2个产品">
                                                            <img src="images/init-1x1_001.jpg" data-src="http://open.file.meizu.com/group1/M00/00/19/CnQOjVeN7JmARIdHAAKkhCmlgOs675_180x180.png" alt="MX6智能保护套" height="50" width="50">
                                                            <span>MX6智能保护套</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/meilannote3_tpu.html" target="_top" data-mtype="store_index_cdh_6_3" data-mdesc="侧边导航中第6个分类第3个产品">
                                                            <img src="images/init-1x1_001.jpg" data-src="http://open.file.meizu.com/group1/M00/00/47/CnQOjVfH1HOAK9gkAARJiQJn9AA145_180x180.png" alt="魅蓝note3 软胶保护壳" height="50" width="50">
                                                            <span>魅蓝note3 软胶保护壳</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/note3_loop.html" target="_top" data-mtype="store_index_cdh_6_4" data-mdesc="侧边导航中第6个分类第4个产品">
                                                            <img src="images/init-1x1_001.jpg" data-src="http://open.file.meizu.com/group1/M00/00/47/CnQOjVfH1L-AcHIUAAKzNe6JVvs467_180x180.png" alt="魅蓝note3 智能保护套" height="50" width="50">
                                                            <span>魅蓝note3 智能保护套</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/meilan3_pasting.html" target="_top" data-mtype="store_index_cdh_6_5" data-mdesc="侧边导航中第6个分类第5个产品">
                                                            <img src="images/init-1x1_001.jpg" data-src="http://open.file.meizu.com/group1/M00/00/57/CnQOjVfWCV6AH_R5AABmuD2rhcU806_180x180.png" alt="魅蓝3 高透保护膜" height="50" width="50">
                                                            <span>魅蓝3 高透保护膜</span>
                                                        </a>
                                                    </li>
                                        </ul>
                                        <ul class="nav-children-left">
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/pro6_loop.html" target="_top" data-mtype="store_index_cdh_6_6" data-mdesc="侧边导航中第6个分类第6个产品">
                                                            <img src="images/init-1x1_001.jpg" data-src="http://open.file.meizu.com/group1/M00/00/20/Cix_s1ehVAyACMIkAALN7IQAbIM494_180x180.png" alt="PRO 6智能保护套" height="50" width="50">
                                                            <span>PRO 6智能保护套</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/meilan_E_pasting.html" target="_top" data-mtype="store_index_cdh_6_7" data-mdesc="侧边导航中第6个分类第7个产品">
                                                            <img src="images/init-1x1_001.jpg" data-src="http://open.file.meizu.com/group1/M00/00/2D/Cix_s1evz-qACJ77AADXGdYFg6Y878_180x180.png" alt="魅蓝E 高透保护膜" height="50" width="50">
                                                            <span>魅蓝E 高透保护膜</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/meilan3s_pasting.html" target="_top" data-mtype="store_index_cdh_6_8" data-mdesc="侧边导航中第6个分类第8个产品">
                                                            <img src="images/init-1x1_001.jpg" data-src="http://open.file.meizu.com/group1/M00/00/1C/CnQOjVeYZeCAKJlWAAPduqMl9J8158_180x180.png" alt="魅蓝3s 高透保护膜" height="50" width="50">
                                                            <span>魅蓝3s 高透保护膜</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/meilannote3_pasting.html" target="_top" data-mtype="store_index_cdh_6_9" data-mdesc="侧边导航中第6个分类第9个产品">
                                                            <img src="images/init-1x1_001.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0F/CnQOjVeFtM-AGUBRAABiyeWlTHc607_180x180.png" alt="魅蓝note3 高透保护膜" height="50" width="50">
                                                            <span>魅蓝note3 高透保护膜</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/max_pasting.html" target="_top" data-mtype="store_index_cdh_6_10" data-mdesc="侧边导航中第6个分类第10个产品">
                                                            <img src="images/init-1x1_001.jpg" data-src="http://open.file.meizu.com/group1/M00/00/57/Cix_s1fWCNGAA6bvAAHjh-SvOeM580_180x180.png" alt="魅蓝Max高透保护膜" height="50" width="50">
                                                            <span>魅蓝Max高透保护膜</span>
                                                        </a>
                                                    </li>
                                        </ul>
                                </div>
                                    <div class="category-item-ad">
                                        <a href="http://detail.meizu.com/item/meilan3_color.html" target="_top" data-mtype="store_index_cdh_6_gg" data-mdesc="侧边导航中第6个分类广告">
                                            <img src="images/1462246403-45996.jpg" height="480" width="296">
                                        </a>
                                    </div>
                            </div>
                    </li>
                    <li class="home-category-nav-item">
                        <a class="category-nav-link" data-mtype="store_index_cdh_7" data-mdesc="侧边导航中第7个分类" href="http://lists.meizu.com/page/list?categoryid=82&amp;rc=cb">
                            <span>数据线 / 电源适配器</span>
                        </a>
                            <div class="category-nav-children nav-wrap-col-2">
                                <div class="nav-children-wrap">
                                        <ul class="nav-children-left">
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/metal_usb.html" target="_top" data-mtype="store_index_cdh_7_1" data-mdesc="侧边导航中第7个分类第1个产品">
                                                            <img src="images/cnqojvfect6aekbmaailzk346xy256_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/60/CnQOjVfeCT6AekBmAAIlZk346XY256_180x180.png" alt="魅族二合一金属数据线" height="50" width="50">
                                                            <span>魅族二合一金属数据线</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/micro_usb.html" target="_top" data-mtype="store_index_cdh_7_2" data-mdesc="侧边导航中第7个分类第2个产品">
                                                            <img src="images/cix_s1fecgwaathbaaikgpy2hvq947_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/60/Cix_s1feCGWAaThbAAIkGpY2hVQ947_180x180.png" alt="魅族Micro USB金属数据线" height="50" width="50">
                                                            <span>魅族Micro USB金属数据线</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/type_c_adapter.html" target="_top" data-mtype="store_index_cdh_7_3" data-mdesc="侧边导航中第7个分类第3个产品">
                                                            <img src="images/cnqojvfecd2agpfiaak3xr-97nw766_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/60/CnQOjVfeCd2AGPfIAAK3xr-97Nw766_180x180.png" alt="魅族 Type-C转接头" height="50" width="50">
                                                            <span>魅族 Type-C转接头</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/meizu_mx4_usb_power.html" target="_top" data-mtype="store_index_cdh_7_4" data-mdesc="侧边导航中第7个分类第4个产品">
                                                            <img src="images/cnqojveftssafiphaafrl2yvhzy367_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/0F/CnQOjVeFtSSAFIpHAAFrL2yvhZY367_180x180.png" alt="电源适配器" height="50" width="50">
                                                            <span>电源适配器</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/type_c_1m.html" target="_top" data-mtype="store_index_cdh_7_5" data-mdesc="侧边导航中第7个分类第5个产品">
                                                            <img src="images/cnqojveftvuaey8iaaiww8giygc307_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/0F/CnQOjVeFtVuAey8IAAIWW8GIYgc307_180x180.png" alt="魅族Type-C USB数据线" height="50" width="50">
                                                            <span>魅族Type-C USB数据线</span>
                                                        </a>
                                                    </li>
                                        </ul>
                                        <ul class="nav-children-left">
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/usbline033.html" target="_top" data-mtype="store_index_cdh_7_6" data-mdesc="侧边导航中第7个分类第6个产品">
                                                            <img src="images/cix_s1eftxgay_duaaciung1sqg766_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFtXGAY_DuAACiUNg1SQg766_180x180.png" alt="魅族USB 数据线" height="50" width="50">
                                                            <span>魅族USB 数据线</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/type_c.html" target="_top" data-mtype="store_index_cdh_7_7" data-mdesc="侧边导航中第7个分类第7个产品">
                                                            <img src="images/cix_s1fecy6abj80aahxid4twta427_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/60/Cix_s1feCY6ABJ80AAHXId4TWtA427_180x180.png" alt="魅族Type-C 金属数据线" height="50" width="50">
                                                            <span>魅族Type-C 金属数据线</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/ROMOSS_A10.html?skuid=1275" target="_top" data-mtype="store_index_cdh_7_8" data-mdesc="侧边导航中第7个分类第8个产品">
                                                            <img src="images/cix_s1eftb-aynd3aacvrhk1hfm250_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFtb-AYnD3AACVRHk1HFM250_180x180.png" alt="罗马仕AC10快充适配器" height="50" width="50">
                                                            <span>罗马仕AC10快充适配器</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/ORICO_UCH_Q3.html" target="_top" data-mtype="store_index_cdh_7_9" data-mdesc="侧边导航中第7个分类第9个产品">
                                                            <img src="images/cix_s1enonuafah3aakxg1efddm792_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/17/Cix_s1eNoNuAfAH3AAKxg1EfddM792_180x180.png" alt="ORICO QC3.0多口车充头" height="50" width="50">
                                                            <span>ORICO QC3.0多口车充头</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/ORICO_QSE_4U.html" target="_top" data-mtype="store_index_cdh_7_10" data-mdesc="侧边导航中第7个分类第10个产品">
                                                            <img src="images/cnqojvenormadcaxaadko3acmru993_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/16/CnQOjVeNoRmADcaXAADkO3acMRU993_180x180.png" alt="ORICO 4口USB快充充电器" height="50" width="50">
                                                            <span>ORICO 4口USB快充充电器</span>
                                                        </a>
                                                    </li>
                                        </ul>
                                </div>
                                    <div class="category-item-ad">
                                        <a href="http://detail.meizu.com/item/meizu_mx5_power.html" target="_top" data-mtype="store_index_cdh_7_gg" data-mdesc="侧边导航中第7个分类广告">
                                            <img src="images/1467254278-66658.jpg" height="480" width="296">
                                        </a>
                                    </div>
                            </div>
                    </li>
                    <li class="home-category-nav-item">
                        <a class="category-nav-link" data-mtype="store_index_cdh_8" data-mdesc="侧边导航中第8个分类" href="http://lists.meizu.com/page/list?categoryid=83&amp;rc=cb">
                            <span>周边配件</span>
                        </a>
                            <div class="category-nav-children nav-wrap-col-2">
                                <div class="nav-children-wrap">
                                        <ul class="nav-children-left">
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/LQ_011.html?skuid=1328" target="_top" data-mtype="store_index_cdh_8_1" data-mdesc="侧边导航中第8个分类第1个产品">
                                                            <img src="images/cnqojvezd-aaczulafq1dubt8za231_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/1C/CnQOjVeZd-aACZuLAFQ1DuBT8ZA231_180x180.png" alt="猎奇(LIEQI)手机镜头" height="50" width="50">
                                                            <span>猎奇(LIEQI)手机镜头</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/Toshiba_v8.html?skuid=994" target="_top" data-mtype="store_index_cdh_8_2" data-mdesc="侧边导航中第8个分类第2个产品">
                                                            <img src="images/cnqojvexn1oabajgaayzgoh7sfs835_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/1B/CnQOjVeXN1OAbajGAAYZGoh7sfs835_180x180.png" alt="东芝v8 2.5英寸移动硬盘" height="50" width="50">
                                                            <span>东芝v8 2.5英寸移动硬盘</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/keycase.html?skuid=1053" target="_top" data-mtype="store_index_cdh_8_3" data-mdesc="侧边导航中第8个分类第3个产品">
                                                            <img src="images/cix_s1exn4eaqlckaajyj_xcl5i669_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/1C/Cix_s1eXN4eAQlCKAAJYJ_xCl5I669_180x180.png" alt="魅族豌豆钥匙包" height="50" width="50">
                                                            <span>魅族豌豆钥匙包</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/HDTB305YK3AA.html?skuid=1019" target="_top" data-mtype="store_index_cdh_8_4" data-mdesc="侧边导航中第8个分类第4个产品">
                                                            <img src="images/cix_s1eftieaeey_aap3kv3ja9o161_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFtieAEEY_AAP3Kv3jA9o161_180x180.png" alt="东芝黑甲虫2.5寸移动硬盘" height="50" width="50">
                                                            <span>东芝黑甲虫2.5寸移动硬盘</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/SDDD2.html?skuid=883" target="_top" data-mtype="store_index_cdh_8_5" data-mdesc="侧边导航中第8个分类第5个产品">
                                                            <img src="images/cix_s1eftjqamymuaauuuzgkac0878_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFtjqAMymuAAUUUZgKAC0878_180x180.png" alt="闪迪至尊高速OTG手机U盘" height="50" width="50">
                                                            <span>闪迪至尊高速OTG手机U盘</span>
                                                        </a>
                                                    </li>
                                        </ul>
                                        <ul class="nav-children-left">
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/SAMSUNG_SSD750.html?skuid=1102" target="_top" data-mtype="store_index_cdh_8_6" data-mdesc="侧边导航中第8个分类第6个产品">
                                                            <img src="images/cix_s1ezen-agpsfaaxq3fgmkrq159_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/1C/Cix_s1eZeN-AGPsFAAXQ3fgMKRQ159_180x180.png" alt="三星750 SATA3 固态硬盘" height="50" width="50">
                                                            <span>三星750 SATA3 固态硬盘</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-children-item">
                                                        <a href="http://detail.meizu.com/item/keychain.html?skuid=1051" target="_top" data-mtype="store_index_cdh_8_7" data-mdesc="侧边导航中第8个分类第7个产品">
                                                            <img src="images/cnqojvevfeyayvw8aaidait89pm797_180x180.png" data-src="http://open.file.meizu.com/group1/M00/00/1A/CnQOjVeVfeyAYvw8AAIdait89PM797_180x180.png" alt="魅族熊猫金属钥匙扣" height="50" width="50">
                                                            <span>魅族熊猫金属钥匙扣</span>
                                                        </a>
                                                    </li>
                                        </ul>
                                </div>
                                    <div class="category-item-ad">
                                        <a href="http://detail.meizu.com/item/SRB01_Luxury.html?rc=cb" target="_top" data-mtype="store_index_cdh_8_gg" data-mdesc="侧边导航中第8个分类广告">
                                            <img src="images/cnqojvfqolsabmsuaabpbce1rv0846.jpg" height="480" width="296">
                                        </a>
                                    </div>
                            </div>
                    </li>
            </ul>
        </div>
    </div>
</div>
<div id="MzHomeSlider" class="home-slider">
    <div class="home-slider-items">
        <div class="bx-wrapper" style="max-width: 100%;"><div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: 480px;"><ul class="bxslider" style="width: auto; position: relative;">
                <li class="home-slider-box" data-bgcolor="#b4d0ea" style="background: rgb(180, 208, 234) none repeat scroll 0% 0%; float: none; list-style: outside none none; position: absolute; width: 1263px; z-index: 0; display: none;">
                    <a class="home-slider-image" style="width: 1920px; margin-left: -960px;" href="http://hd.meizu.com/sale/nationalday.html?rc=lb1" target="_top" data-mtype="store_index_ba_1" data-mdesc="第1屏banner">
                        <img src="images/cnqojvfqfx2ayqroaacqcemovbq025.png" height="480" width="1920">
                    </a>
                </li>
                <li class="home-slider-box" data-bgcolor="#e33e48" style="background: rgb(227, 62, 72) none repeat scroll 0% 0%; float: none; list-style: outside none none; position: absolute; width: 1263px; z-index: 0; display: none;">
                    <a class="home-slider-image" style="width: 1920px; margin-left: -960px;" href="http://detail.meizu.com/item/mx6.html?rc=lb2" target="_top" data-mtype="store_index_ba_2" data-mdesc="第2屏banner">
                        <img src="images/cix_s1frhcgayd7iaa45ihd4vpk381.png" height="480" width="1920">
                    </a>
                </li>
                <li class="home-slider-box" data-bgcolor="#13161f" style="background: rgb(19, 22, 31) none repeat scroll 0% 0%; float: none; list-style: outside none none; position: absolute; width: 1263px; z-index: 50;">
                    <a class="home-slider-image" style="width: 1240px; margin-left: -620px;" href="http://detail.meizu.com/item/lexin_mambo.html?rc=lb3" target="_top" data-mtype="store_index_ba_3" data-mdesc="第3屏banner">
                        <img src="images/cnqojvfio5uamyygaaghnx9dp1y307.jpg" height="480" width="1240">
                    </a>
                </li>
                <li class="home-slider-box" data-bgcolor="#1b1b1b" style="background: rgb(27, 27, 27) none repeat scroll 0% 0%; float: none; list-style: outside none none; position: absolute; width: 1263px; z-index: 0; display: none;">
                    <a class="home-slider-image" style="width: 1920px; margin-left: -960px;" href="http://detail.meizu.com/item/meilan_u10.html?rc=lb4" target="_top" data-mtype="store_index_ba_4" data-mdesc="第4屏banner">
                        <img src="images/cnqojvfp266ahtxraatbzqitla0372.jpg" height="480" width="1920">
                    </a>
                </li>
                <li class="home-slider-box" data-bgcolor="#FFFFFF" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; float: none; list-style: outside none none; position: absolute; width: 1263px; z-index: 0; display: none;">
                    <a class="home-slider-image" style="width: 1920px; margin-left: -960px;" href="http://detail.meizu.com/item/meizu_hd50.html?rc=lb5" target="_top" data-mtype="store_index_ba_5" data-mdesc="第5屏banner">
                        <img src="images/cnqojvfqkseadlgbaaelj5hlaas640.jpg" height="480" width="1920">
                    </a>
                </li>
        </ul></div><div class="bx-controls bx-has-pager bx-has-controls-direction"><div class="bx-pager bx-default-pager"><div class="bx-pager-item"><a href="http://store.meizu.com/index.html" data-slide-index="0" class="bx-pager-link">1</a></div><div class="bx-pager-item"><a href="http://store.meizu.com/index.html" data-slide-index="1" class="bx-pager-link">2</a></div><div class="bx-pager-item"><a href="http://store.meizu.com/index.html" data-slide-index="2" class="bx-pager-link active">3</a></div><div class="bx-pager-item"><a href="http://store.meizu.com/index.html" data-slide-index="3" class="bx-pager-link">4</a></div><div class="bx-pager-item"><a href="http://store.meizu.com/index.html" data-slide-index="4" class="bx-pager-link">5</a></div></div><div class="bx-controls-direction"><a class="bx-prev" href="http://store.meizu.com/index.html">Prev</a><a class="bx-next" href="http://store.meizu.com/index.html">Next</a></div></div></div>
    </div>
</div>
        <div class="mzcontainer">

<div class="home-mzad">
  <div class="home-box home-service">
      <div class="service-module-2">
        <a href="http://store.meizu.com/mformy/index" target="_top" data-mtype="store_index_gd_1" data-mdesc="固定资源位中第1个">
          <span class="home-service-icon service-icon-mentry">
            <img src="images/1450855246-61162.png" alt="M码通道" height="24" width="24">
          </span>
          <p>M码通道</p>
        </a>
      </div>
      <div class="service-module-2">
        <a href="http://mcycle.meizu.com/" target="_top" data-mtype="store_index_gd_2" data-mdesc="固定资源位中第2个">
          <span class="home-service-icon service-icon-mentry">
            <img src="images/1450855279-64872.png" alt="以旧换新" height="24" width="24">
          </span>
          <p>以旧换新</p>
        </a>
      </div>
      <div class="service-module-2">
        <a href="http://www.meizu.com/products/insu.html" target="_top" data-mtype="store_index_gd_3" data-mdesc="固定资源位中第3个">
          <span class="home-service-icon service-icon-mentry">
            <img src="images/1450855198-58056.png" alt="魅族意外保" height="24" width="24">
          </span>
          <p>魅族意外保</p>
        </a>
      </div>
      <div class="service-module-2">
        <a href="http://me.meizu.com/member/repo/index" target="_top" data-mtype="store_index_gd_4" data-mdesc="固定资源位中第4个">
          <span class="home-service-icon service-icon-mentry">
            <img src="images/1451960447-93534.png" alt="回购单查询" height="24" width="24">
          </span>
          <p>回购单查询</p>
        </a>
      </div>
  </div>

    <div class="home-box home-ad-box">
      <a href="http://hd.meizu.com/activity/meizu.html?rc=xj" target="_top" data-mtype="store_index_xj_1" data-mdesc="小焦中第1个">
        <img src="images/1469442560-64371.jpg" height="140" width="244">
      </a>
    </div>
    <div class="home-box home-ad-box">
      <a href="http://detail.meizu.com/item/meilanU20.html?rc=xj" target="_top" data-mtype="store_index_xj_2" data-mdesc="小焦中第2个">
        <img src="images/cix_s1ftqbqaigcpaabcn90ze-8433.jpg" height="140" width="244">
      </a>
    </div>
    <div class="home-box home-ad-box">
      <a href="http://detail.meizu.com/item/Toshiba_v8.html?rc=xj" target="_top" data-mtype="store_index_xj_3" data-mdesc="小焦中第3个">
        <img src="images/cix_s1fdlpyahas_aaa3pf0evgy879.jpg" height="140" width="244">
      </a>
    </div>
    <div class="home-box home-ad-box">
      <a href="http://detail.meizu.com/item/mpower_m8e.html?rc=xj" target="_top" data-mtype="store_index_xj_4" data-mdesc="小焦中第4个">
        <img src="images/cnqojvfhda2aktdyaaatkjihe5e114.jpg" height="140" width="244">
      </a>
    </div>
</div>
<div class="home-panel home-rmd home-hot">
    <div class="home-panel-topbar">
        <h2 class="home-panel-title">热品推荐</h2>
            <div class="home-panel-tools">
        <span id="MzRmdSliderLeft" class="panel-slider panel-slider-left" data-mtype="store_main_rp_a" data-mdesc="">
          <span class="more-arrow"> </span>
        </span>
        <span id="MzRmdSliderRight" class="panel-slider panel-slider-right panel-slider-disabled" data-mtype="store_main_rp_b" data-mdesc="">
          <span class="more-arrow"></span>
        </span>
            </div>
    </div>
    <div class="home-rmd-main">
        <div class="home-rmd-cotent">
            <div class="rm-box-25">
                <div id="MzRmdList" class="rmd-content-list" style="width: 2480px; left: -1240px;">
                            <div class="rmd-box rmd-box-product">
                                <a href="http://detail.meizu.com/item/mx6.html?skuid=1612" target="_top" data-mtype="store_index_rp_1_1" data-mdesc="热品推荐第1屏第1个坑">
                                    <div class="rmd-product-detail">
                                        <img src="images/cix_s1fahtqacmpbaa-eha0uf7a884_180x180.png" alt="魅族 MX6" height="180" width="180">
                                        <div class="rmd-product-desc">
                                            <h4 class="rmd-product-title">魅族 MX6</h4>
                                                <h6 class="rmd-product-subtitle red"><span class="rmd-product-subtitle-label">分期</span>12期免息 赠环窗智能保护套
                                                </h6>
                                            <p class="rmd-product-price">
                                                ¥
                                                <span>1999</span>
                                            
                                            </p>
                                        </div>
                                    </div>
                                        <div class="box-product-sign" style="background-color: rgb(0, 175, 190);">
                                        赠品
                                        </div>
                                </a>
                            </div>
                            <div class="rmd-box rmd-box-product">
                                <a href="http://detail.meizu.com/item/meilan_max1.html?skuid=1980" target="_top" data-mtype="store_index_rp_1_2" data-mdesc="热品推荐第1屏第2个坑">
                                    <div class="rmd-product-detail">
                                        <img src="images/cnqojvfk_a6atpmraascqinnnpy778_180x180.png" alt="魅蓝 Max" height="180" width="180">
                                        <div class="rmd-product-desc">
                                            <h4 class="rmd-product-title">魅蓝 Max</h4>
                                                <h6 class="rmd-product-subtitle red"><span class="rmd-product-subtitle-label">分期</span>12期免息 月供低至141.58元
                                                </h6>
                                            <p class="rmd-product-price">
                                                ¥
                                                <span>1699</span>
                                            
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="rmd-box rmd-box-product">
                                <a href="http://detail.meizu.com/item/HANGJIA_MX.html" target="_top" data-mtype="store_index_rp_1_3" data-mdesc="热品推荐第1屏第3个坑">
                                    <div class="rmd-product-detail">
                                        <img src="images/cnqojvfn2ccaap8caabxgxnywce246_180x180.png" alt="航嘉小优USB智能快充排插" height="180" width="180">
                                        <div class="rmd-product-desc">
                                            <h4 class="rmd-product-title">航嘉小优USB智能快充排插</h4>
                                                <h6 class="rmd-product-subtitle">智能插座 支持快充</h6>
                                            <p class="rmd-product-price">
                                                ¥
                                                <span>48</span>
                                            
                                            </p>
                                        </div>
                                    </div>
                                        <div class="box-product-sign" style="background-color: rgb(0, 195, 245);">
                                        新品
                                        </div>
                                </a>
                            </div>
                            <div class="rmd-box rmd-box-product">
                                <a href="http://detail.meizu.com/item/SEC_CL35S.html" target="_top" data-mtype="store_index_rp_1_4" data-mdesc="热品推荐第1屏第4个坑">
                                    <div class="rmd-product-detail">
                                        <img src="images/cix_s1fogg-atwbraaiacs5-3nw554_180x180.png" alt="先锋  SE CL35S" height="180" width="180">
                                        <div class="rmd-product-desc">
                                            <h4 class="rmd-product-title">先锋  SE CL35S</h4>
                                                <h6 class="rmd-product-subtitle">强劲低频 还原纯净音质</h6>
                                            <p class="rmd-product-price">
                                                ¥
                                                <span>269</span>
                                            
                                            </p>
                                        </div>
                                    </div>
                                        <div class="box-product-sign" style="background-color: rgb(245, 150, 70);">
                                        热销
                                        </div>
                                </a>
                            </div>
                            <div class="rmd-box rmd-box-product">
                                <a href="http://detail.meizu.com/item/meizu_hd50.html" target="_top" data-mtype="store_index_rp_1_5" data-mdesc="热品推荐第1屏第5个坑">
                                    <div class="rmd-product-detail">
                                        <img src="images/cnqojvfogh-aajgoaatzrggt1ak391_180x180.png" alt="魅族HD50 头戴式耳机" height="180" width="180">
                                        <div class="rmd-product-desc">
                                            <h4 class="rmd-product-title">魅族HD50 头戴式耳机</h4>
                                                <h6 class="rmd-product-subtitle">附赠防水耳机包</h6>
                                            <p class="rmd-product-price">
                                                ¥
                                                <span>399</span>
                                            
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="rmd-box rmd-box-product">
                                <a href="http://detail.meizu.com/item/SEC_CL31_HIFI.html" target="_top" data-mtype="store_index_rp_2_1" data-mdesc="热品推荐第2屏第1个坑">
                                    <div class="rmd-product-detail">
                                        <img src="images/cnqojvfn2fuaupvaaamnjcd2ide424_180x180.png" alt="先锋SEC-CL31 HIFI耳机" height="180" width="180">
                                        <div class="rmd-product-desc">
                                            <h4 class="rmd-product-title">先锋SEC-CL31 HIFI耳机</h4>
                                                <h6 class="rmd-product-subtitle">防缠绕 无线控</h6>
                                            <p class="rmd-product-price">
                                                ¥
                                                <span>79</span>
                                            
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="rmd-box rmd-box-product">
                                <a href="http://detail.meizu.com/item/meizu_pro6.html?skuid=772" target="_top" data-mtype="store_index_rp_2_2" data-mdesc="热品推荐第2屏第2个坑">
                                    <div class="rmd-product-detail">
                                        <img src="images/cnqojveva6qah7k1abhuf4hwkyw782_180x180.png" alt="魅族PRO 6" height="180" width="180">
                                        <div class="rmd-product-desc">
                                            <h4 class="rmd-product-title">魅族PRO 6</h4>
                                                <h6 class="rmd-product-subtitle">12期免息 月供低至191.59元</h6>
                                                <p class="rmd-product-oldprice"> ¥ 2499 </p>
                                            <p class="rmd-product-price">
                                                ¥
                                                <span>2299</span>
                                            
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="rmd-box rmd-box-product">
                                <a href="http://detail.meizu.com/item/lexin_mambo_HR.html" target="_top" data-mtype="store_index_rp_2_3" data-mdesc="热品推荐第2屏第3个坑">
                                    <div class="rmd-product-detail">
                                        <img src="images/cix_s1fogjmadofjaaolpmsbz5s811_180x180.png" alt="乐心运动手环mambo HR" height="180" width="180">
                                        <div class="rmd-product-desc">
                                            <h4 class="rmd-product-title">乐心运动手环mambo HR</h4>
                                                <h6 class="rmd-product-subtitle">心率检测 震动亮屏</h6>
                                            <p class="rmd-product-price">
                                                ¥
                                                <span>169</span>
                                            
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="rmd-box rmd-box-product">
                                <a href="http://detail.meizu.com/item/huanxiang_B10.html" target="_top" data-mtype="store_index_rp_2_4" data-mdesc="热品推荐第2屏第4个坑">
                                    <div class="rmd-product-detail">
                                        <img src="images/cnqojvfqjneavkxkaai-1-j_z7g805_180x180.png" alt="幻响B10无线运动蓝牙耳机" height="180" width="180">
                                        <div class="rmd-product-desc">
                                            <h4 class="rmd-product-title">幻响B10无线运动蓝牙耳机</h4>
                                                <h6 class="rmd-product-subtitle">立体声 磁力吸附</h6>
                                            <p class="rmd-product-price">
                                                ¥
                                                <span>199</span>
                                            
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="rmd-box rmd-box-product">
                                <a href="http://detail.meizu.com/item/BF4S_RIO.html" target="_top" data-mtype="store_index_rp_2_5" data-mdesc="热品推荐第2屏第5个坑">
                                    <div class="rmd-product-detail">
                                        <img src="images/cnqojvfn2isaqi5faal3mhgf4gy546_180x180.png" alt="暴风魔镜4S-RIO安卓版VR眼镜" height="180" width="180">
                                        <div class="rmd-product-desc">
                                            <h4 class="rmd-product-title">暴风魔镜4S-RIO安卓版VR眼镜</h4>
                                                <h6 class="rmd-product-subtitle">虚拟现实3D眼镜</h6>
                                            <p class="rmd-product-price">
                                                ¥
                                                <span>199</span>
                                            
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                </div>
            </div>
        </div>
    </div>
</div>
        </div>

<div class="site-gotop" style="display: none;">
 <a class="gotop-suggest" title="建议反馈" href="http://me.meizu.com/member/advice/index" target="_top"></a>
 <div class="gotop-arrow" title="回到顶部"></div>
</div>

        <div class="home-full-box white">
            <div class="mzcontainer">
                <div class="home-floor-ad">
                    <img src="images/cix_s1fpzeuazw2xaadl5a9iju0736.jpg">
                        <a href="http://detail.meizu.com/item/meilan_metal.html?rc=yl" class="hot-point" style="width: 1240px; height: 120px; left: 0px; top: 0px;" target="_top" title="魅蓝metal" data-mtype="store_index_yl_1_1" data-mdesc="第1个腰栏的第1个热区"></a>
                </div>
            </div>
        </div>

    <div class="home-full-box" style="margin-top: 30px;">
        <div class="mzcontainer">
            <div class="home-panel home-rmd home-floor">
                <div class="home-panel-topbar">
                    <h2 class="home-panel-title">手机</h2>
                    <h6 class="home-panel-subtitle">
                    </h6>
                            <div class="home-panel-tools home-floor-tools">
                                <a class="home-tool-more" href="http://lists.meizu.com/page/list?categoryid=76&amp;rc=lc" target="_top" data-mtype="store_index_kwd_1_g" data-mdesc="第1个楼层上方小导航的“更多”按钮">
                                更多
                                    <span class="more-arrow more-icon"> </span>
                                </a>
                            </div>
                </div>
                <div class="home-rmd-main">
                    <div class="home-rmd-cotent">
                                <div class="rmd-box rmd-box-ad rmd-box-ad-1">
                                    <a href="http://detail.meizu.com/item/meilan3.html?rc=lcg" target="_top" data-mtype="store_index_kw_1_1" data-mdesc="第1个楼层第1个坑">
                                        <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/6C/CnQOjVfp1EyATachAADUiySVz90410.jpg" src="images/cnqojvfp1eyatachaaduiysvz90410.jpg" alt="魅蓝3" style="display: inline;">
                                    </a>
                                        <div class="rmd-box-ad-module">
                                            <a class="rmd-box-ad-btn" target="_top" href="http://detail.meizu.com/item/meilan3.html?rc=lcg">
                                            立即购买
                                            </a>
                                        </div>
                                </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/meilan_u10.html?rc=lc" target="_top" data-mtype="store_index_kw_1_2" data-mdesc="第1个楼层第2个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/6C/Cix_s1fodxWACn7tAAjwkwqC8nw736_180x180.png" alt="魅蓝U10" src="images/cix_s1fodxwacn7taajwkwqc8nw736_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">魅蓝U10</h4>
                                                        <h6 class="rmd-product-subtitle red"><span class="rmd-product-subtitle-label">分期</span>3期免息 月供低至333元
                                                        </h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>999</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                                <div class="box-product-sign" style="background-color: rgb(0, 195, 245);">
                                                新品
                                                </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/meilan_max1.html?skuid=1980" target="_top" data-mtype="store_index_kw_1_3" data-mdesc="第1个楼层第3个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/60/Cix_s1fd_gyAFpn2AAscQInnNPY885_180x180.png" alt="魅蓝 Max" src="images/cix_s1fd_gyafpn2aascqinnnpy885_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">魅蓝 Max</h4>
                                                        <h6 class="rmd-product-subtitle red"><span class="rmd-product-subtitle-label">分期</span>12期免息 月供低至141.58元
                                                        </h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>1699</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/meizu_pro6.html?rc=lc" target="_top" data-mtype="store_index_kw_1_4" data-mdesc="第1个楼层第4个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/6C/Cix_s1fpzL-ADgAYABHUf4HwKyw986_180x180.png" alt="魅族PRO 6" src="images/cix_s1fpzl-adgayabhuf4hwkyw986_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">魅族PRO 6</h4>
                                                        <h6 class="rmd-product-subtitle red"><span class="rmd-product-subtitle-label">分期</span>12免息 月供低至191.58元
                                                        </h6>
                                                    <!-- TODO 替换为内容-->
                                                        <p class="rmd-product-oldprice"> ¥ 2499 </p>
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>2299</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/mx6.html?rc=lc" target="_top" data-mtype="store_index_kw_1_5" data-mdesc="第1个楼层第5个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/60/Cix_s1ffRIeADHxjAA9IpQ8-shg663_180x180.png" alt="魅族 MX6" src="images/cix_s1ffrieadhxjaa9ipq8-shg663_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">魅族 MX6</h4>
                                                        <h6 class="rmd-product-subtitle red"><span class="rmd-product-subtitle-label">赠品</span>12期免息 赠环窗智能保护套
                                                        </h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>1999</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                                <div class="box-product-sign" style="background-color: rgb(0, 175, 190);">
                                                分期
                                                </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/metal_lihe.html?rc=lc" target="_top" data-mtype="store_index_kw_1_6" data-mdesc="第1个楼层第6个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/6C/CnQOjVfp1MuAWtDeAASEwGfY-ws231_180x180.png" alt="魅蓝metal礼盒套装" src="images/cnqojvfp1muawtdeaasewgfy-ws231_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">魅蓝metal礼盒套装</h4>
                                                        <h6 class="rmd-product-subtitle">领券立减100元 支持花呗分期</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>1099</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                                <div class="box-product-sign" style="background-color: rgb(240, 65, 95);">
                                                特惠
                                                </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/meizu_pro5.html?rc=lc" target="_top" data-mtype="store_index_kw_1_7" data-mdesc="第1个楼层第7个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/62/CnQOjVfgmw-ANx4rAAsp3jBYBlE631_180x180.png" alt="魅族PRO 5" src="images/cnqojvfgmw-anx4raasp3jbyble631_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">魅族PRO 5</h4>
                                                        <h6 class="rmd-product-subtitle red"><span class="rmd-product-subtitle-label">赠品</span>12期免息 赠EP-31耳机
                                                        </h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>2199</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/meilan_3s_lihe.html?rc=lc" target="_top" data-mtype="store_index_kw_1_8" data-mdesc="第1个楼层第8个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/4D/Cix_s1fM0muAPUD5AAWEBWo3WHU863_180x180.png" alt="魅蓝3s 礼盒版" src="images/cix_s1fm0muapud5aawebwo3whu863_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">魅蓝3s 礼盒版</h4>
                                                        <h6 class="rmd-product-subtitle">礼盒含防爆膜 缤纷保护壳</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>799</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/meilan_e1.html?rc=lc" target="_top" data-mtype="store_index_kw_1_9" data-mdesc="第1个楼层第9个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/61/CnQOjVffSHOAaRkmAA0Rkx2XjKE222_180x180.png" alt="魅蓝 E" src="images/cnqojvffshoaarkmaa0rkx2xjke222_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">魅蓝 E</h4>
                                                        <h6 class="rmd-product-subtitle">全网通 多彩金属机身</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>1299</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/meilannote3_lihe.html?rc=lc" target="_top" data-mtype="store_index_kw_1_10" data-mdesc="第1个楼层第10个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/6E/Cix_s1frKieAD1K3AARA9yXl908616_180x180.png" alt="魅蓝note3 礼盒版" src="images/cix_s1frkiead1k3aara9yxl908616_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">魅蓝note3 礼盒版</h4>
                                                        <h6 class="rmd-product-subtitle">礼盒含防爆膜 缤纷保护壳</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>899</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="home-full-box ">
            <div class="mzcontainer">
                <div class="home-floor-ad">
                    <img src="images/cix_s1fqo7aayr-paadk_4ppakm244.jpg">
                        <a href="http://detail.meizu.com/item/SEC_CL31_HIFI.html?rc=yl" class="hot-point" style="width: 1240px; height: 120px; left: 0px; top: 0px;" target="_top" title="先锋CL31" data-mtype="store_index_yl_2_1" data-mdesc="第2个腰栏的第1个热区"></a>
                </div>
            </div>
        </div>

    <div class="home-full-box" style="">
        <div class="mzcontainer">
            <div class="home-panel home-rmd home-floor">
                <div class="home-panel-topbar">
                    <h2 class="home-panel-title">精选配件</h2>
                    <h6 class="home-panel-subtitle">
                                <a href="http://lists.meizu.com/page/list?categoryid=79&amp;rc=lc" target="_top" data-mtype="store_index_kwd_2_2" data-mdesc="第2个楼层上方小导航第2个按钮	">耳机音箱</a>
                                <a href="http://lists.meizu.com/page/list?categoryid=80&amp;rc=lc" target="_top" data-mtype="store_index_kwd_2_2" data-mdesc="第2个楼层上方小导航第2个按钮	">路由器/移动电源</a>
                                <a href="http://lists.meizu.com/page/list?categoryid=83&amp;rc=lc" target="_top" data-mtype="store_index_kwd_2_2" data-mdesc="第2个楼层上方小导航第2个按钮	">移动存储</a>
                    </h6>
                </div>
                <div class="home-rmd-main">
                    <div class="home-rmd-cotent">
                                <div class="rmd-box rmd-box-ad rmd-box-ad-1">
                                    <a href="http://detail.meizu.com/item/SDDD2.html?rc=lc" target="_top" data-mtype="store_index_kw_2_1" data-mdesc="第2个楼层第1个坑">
                                        <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/6E/Cix_s1fqNP2AM3q5AACIyw0u2Vg181.jpg" src="images/cix_s1fqnp2am3q5aaciyw0u2vg181.jpg" alt="闪迪至尊高速OTG手机U盘" style="display: inline;">
                                    </a>
                                        <div class="rmd-box-ad-module">
                                            <a class="rmd-box-ad-btn" target="_top" href="http://detail.meizu.com/item/SDDD2.html?rc=lc">
                                            立即购买
                                            </a>
                                        </div>
                                </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/APS_BH501.html?skuid=1286" target="_top" data-mtype="store_index_kw_2_2" data-mdesc="第2个楼层第2个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/6E/Cix_s1fqJLCAMeYGAAFgR67pnIk567_180x180.png" alt="先锋单边蓝牙耳机APS-BH50" src="images/cix_s1fqjlcameygaafgr67pnik567_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">先锋单边蓝牙耳机APS-BH50</h4>
                                                        <h6 class="rmd-product-subtitle">直降+5元优惠券</h6>
                                                    <!-- TODO 替换为内容-->
                                                        <p class="rmd-product-oldprice"> ¥ 169 </p>
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>99</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                                <div class="box-product-sign" style="background-color: rgb(240, 65, 95);">
                                                直降
                                                </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/SONY_MDR_XB650BT.html" target="_top" data-mtype="store_index_kw_2_3" data-mdesc="第2个楼层第3个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/66/CnQOjVfiKYaADRT_AAWkp1DW7mc596_180x180.png" alt="索尼SONY MDR-XB650BT" src="images/cnqojvfikyaadrt_aawkp1dw7mc596_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">索尼SONY MDR-XB650BT</h4>
                                                        <h6 class="rmd-product-subtitle">头戴便携蓝牙重低音耳机</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>799</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                                <div class="box-product-sign" style="background-color: rgb(240, 65, 95);">
                                                优惠
                                                </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/JBL_JBLGO.html?skuid=425" target="_top" data-mtype="store_index_kw_2_4" data-mdesc="第2个楼层第4个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/60/Cix_s1ffQVOAHa_1ABC1HrcbMc0473_180x180.png" alt="JBL GO音乐金砖 无线蓝牙音响" src="images/cix_s1ffqvoaha_1abc1hrcbmc0473_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">JBL GO音乐金砖 无线蓝牙音响</h4>
                                                        <h6 class="rmd-product-subtitle">将音乐进行到底</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>299</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/FiiO_M3.html?skuid=1247" target="_top" data-mtype="store_index_kw_2_5" data-mdesc="第2个楼层第5个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/5A/CnQOjVfWasqAOprvAAOvCPCH1u8414_180x180.png" alt="FiiO飞傲M3 无损音乐播放器" src="images/cnqojvfwasqaoprvaaovcpch1u8414_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">FiiO飞傲M3 无损音乐播放器</h4>
                                                        <h6 class="rmd-product-subtitle">便携式数字音乐播放器</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>298</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/ROMOSS_A10.html?skuid=1766" target="_top" data-mtype="store_index_kw_2_6" data-mdesc="第2个楼层第6个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/58/Cix_s1fWHNWAXBhTAACVRHk1HFM053_180x180.png" alt="罗马仕AC10快充适配器" src="images/cix_s1fwhnwaxbhtaacvrhk1hfm053_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">罗马仕AC10快充适配器</h4>
                                                        <h6 class="rmd-product-subtitle">支持MTK快充模式</h6>
                                                    <!-- TODO 替换为内容-->
                                                        <p class="rmd-product-oldprice"> ¥ 39 </p>
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>36</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/SAMSUNG_SSD750.html" target="_top" data-mtype="store_index_kw_2_7" data-mdesc="第2个楼层第7个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/6E/CnQOjVfqJaCARDtGAAXQ3fgMKRQ760_180x180.png" alt="三星 750 固态硬盘" src="images/cnqojvfqjacardtgaaxq3fgmkrq760_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">三星 750 固态硬盘</h4>
                                                        <h6 class="rmd-product-subtitle">买即送铭摩印象自拍杆</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>328</span>
                                                    起
                                                    </p>
                                                </div>
                                            </div>
                                                <div class="box-product-sign" style="background-color: rgb(0, 175, 190);">
                                                买赠
                                                </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/MX80.html?skuid=1836" target="_top" data-mtype="store_index_kw_2_8" data-mdesc="第2个楼层第8个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/6B/Cix_s1fkmVCASjF_AAJhriaz9F8039_180x180.png" alt="森海塞尔MX80 简约入门型耳塞" src="images/cix_s1fkmvcasjf_aajhriaz9f8039_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">森海塞尔MX80 简约入门型耳塞</h4>
                                                        <h6 class="rmd-product-subtitle">音质三频均衡 高韧性耳机线</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>69</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/SanDisk_CA10SD80S.html" target="_top" data-mtype="store_index_kw_2_9" data-mdesc="第2个楼层第9个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/6E/CnQOjVfqJ_2ADuOhAAh0zKATX0Q140_180x180.png" alt="闪迪 存储卡" src="images/cnqojvfqj_2aduohaah0zkatx0q140_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">闪迪 存储卡</h4>
                                                        <h6 class="rmd-product-subtitle">Class10 读速80Mb/s</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>39.9</span>
                                                    起
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/HGST_Touro_Mobile.html" target="_top" data-mtype="store_index_kw_2_10" data-mdesc="第2个楼层第10个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/66/Cix_s1fiKiKAftOsAANWjSJkH78818_180x180.png" alt="HGST昱科Touro Mobile移动硬盘" src="images/cix_s1fikikaftosaanwjsjkh78818_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">HGST昱科Touro Mobile移动硬盘</h4>
                                                        <h6 class="rmd-product-subtitle">轻薄小巧 高速稳定</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>309</span>
                                                    起
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="home-full-box ">
            <div class="mzcontainer">
                <div class="home-floor-ad">
                    <img src="images/1469408953-98411.jpg">
                        <a href="http://detail.meizu.com/item/meizu_ep51.html?rc=yl" class="hot-point" style="width: 1240px; height: 120px; left: 0px; top: 0px;" target="_top" title="EP51" data-mtype="store_index_yl_3_1" data-mdesc="第3个腰栏的第1个热区"></a>
                </div>
            </div>
        </div>

    <div class="home-full-box" style="">
        <div class="mzcontainer">
            <div class="home-panel home-rmd home-floor">
                <div class="home-panel-topbar">
                    <h2 class="home-panel-title">智能硬件</h2>
                    <h6 class="home-panel-subtitle">
                                <a href="http://lists.meizu.com/page/list?categoryid=88&amp;rc=lc" target="_top" data-mtype="store_index_kwd_3_2" data-mdesc="第3个楼层上方小导航第2个按钮	">智能家居</a>
                                <a href="http://lists.meizu.com/page/list?categoryid=89&amp;rc=lc" target="_top" data-mtype="store_index_kwd_3_2" data-mdesc="第3个楼层上方小导航第2个按钮	">智能出行</a>
                                <a href="http://lists.meizu.com/page/list?categoryid=90&amp;rc=lc" target="_top" data-mtype="store_index_kwd_3_2" data-mdesc="第3个楼层上方小导航第2个按钮	">智能娱乐</a>
                                <a href="http://lists.meizu.com/page/list?categoryid=87&amp;rc=lc" target="_top" data-mtype="store_index_kwd_3_2" data-mdesc="第3个楼层上方小导航第2个按钮	">智能健康</a>
                    </h6>
                </div>
                <div class="home-rmd-main">
                    <div class="home-rmd-cotent">
                                <div class="rmd-box rmd-box-ad rmd-box-ad-1">
                                    <a href="http://detail.meizu.com/item/misfit_flash.html?rc=lc" target="_top" data-mtype="store_index_kw_3_1" data-mdesc="第3个楼层第1个坑">
                                        <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/6E/Cix_s1fqNwWABpyNAADReMmJZp8618.jpg" src="images/cix_s1fqnwwabpynaadremmjzp8618.jpg" alt="Misfit Flash 智能手环" style="display: inline;">
                                    </a>
                                        <div class="rmd-box-ad-module">
                                            <a class="rmd-box-ad-btn" target="_top" href="http://detail.meizu.com/item/misfit_flash.html?rc=lc">
                                            立即购买
                                            </a>
                                        </div>
                                </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/CILINE_2_5LKP.html" target="_top" data-mtype="store_index_kw_3_2" data-mdesc="第3个楼层第2个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/6C/CnQOjVfp6j-AZATYAAPz4Wq7lzg770_180x180.png" alt="CILINE-沁麟2.5L智能靠谱煲" src="images/cnqojvfp6j-azatyaapz4wq7lzg770_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">CILINE-沁麟2.5L智能靠谱煲</h4>
                                                        <h6 class="rmd-product-subtitle">智能便携 买即送靠谱袋</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>249</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                                <div class="box-product-sign" style="background-color: rgb(0, 175, 190);">
                                                赠品
                                                </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/Goluk_G1.html" target="_top" data-mtype="store_index_kw_3_3" data-mdesc="第3个楼层第3个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/6E/Cix_s1fqKzGAWQBUAAGowe9Q8b0357_180x180.png" alt="极路客 G1 行车记录仪" src="images/cix_s1fqkzgawqbuaagowe9q8b0357_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">极路客 G1 行车记录仪</h4>
                                                        <h6 class="rmd-product-subtitle">精彩抓拍、一键分享、随时直播</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>299</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/T1502.html?skuid=1610" target="_top" data-mtype="store_index_kw_3_4" data-mdesc="第3个楼层第4个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/5A/CnQOjVfWa8OATQ4qAAWJdBWs3Pc208_180x180.png" alt="阿巴町t1502儿童电话手表" src="images/cnqojvfwa8oatq4qaawjdbws3pc208_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">阿巴町t1502儿童电话手表</h4>
                                                        <h6 class="rmd-product-subtitle">拥有高性价比的小傲娇</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>299</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/Gowild_xiaobaiyoung.html?skuid=1664" target="_top" data-mtype="store_index_kw_3_5" data-mdesc="第3个楼层第5个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/5A/CnQOjVfWbGmAXniLACcf6e-q3pc202_180x180.png" alt="公子小白智能机器人 青春版" src="images/cnqojvfwbgmaxnilaccf6e-q3pc202_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">公子小白智能机器人 青春版</h4>
                                                        <h6 class="rmd-product-subtitle">云脑记忆 智能对答 蓝牙音箱</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>999</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                                <div class="box-product-sign" style="background-color: rgb(0, 195, 245);">
                                                新品
                                                </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/BroadLink_RM_mini3.html?skuid=1117" target="_top" data-mtype="store_index_kw_3_6" data-mdesc="第3个楼层第6个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/5A/Cix_s1fWbAuAJD_mAAQ1xFqEUUA996_180x180.png" alt="BroadLink黑豆智能wifi遥控" src="images/cix_s1fwbauajd_maaq1xfqeuua996_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">BroadLink黑豆智能wifi遥控</h4>
                                                        <h6 class="rmd-product-subtitle">真智能 预约清凉 远离空调病</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>79</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/yunmai_mini.html?skuid=1741" target="_top" data-mtype="store_index_kw_3_7" data-mdesc="第3个楼层第7个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/5A/Cix_s1fWa5iASMvOAAMArHJ1NOs243_180x180.png" alt="云麦好轻mini智能体脂秤" src="images/cix_s1fwa5iasmvoaamarhj1nos243_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">云麦好轻mini智能体脂秤</h4>
                                                        <h6 class="rmd-product-subtitle">喝杯水都能感知的精准</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>99</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/lf_kuxuan.html" target="_top" data-mtype="store_index_kw_3_8" data-mdesc="第3个楼层第8个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/6E/CnQOjVfqLbqACV_aAALRn19d2x0125_180x180.png" alt="乐范魔力贴酷炫版" src="images/cnqojvfqlbqacv_aaalrn19d2x0125_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">乐范魔力贴酷炫版</h4>
                                                        <h6 class="rmd-product-subtitle">随身按摩 精致小巧 品质升级</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>129</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/lexin_Melody.html?skuid=969" target="_top" data-mtype="store_index_kw_3_9" data-mdesc="第3个楼层第9个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/5A/Cix_s1fWbVOAef1kAAQmvH3SsIE893_180x180.png" alt="乐心体脂秤Melody wifi传输" src="images/cix_s1fwbvoaef1kaaqmvh3ssie893_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">乐心体脂秤Melody wifi传输</h4>
                                                        <h6 class="rmd-product-subtitle">一体化全闭合机身 精准感应</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>199</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/HANGJIA_pro3s.html" target="_top" data-mtype="store_index_kw_3_10" data-mdesc="第3个楼层第10个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/6E/Cix_s1fqLUuAHgI7AAQ_HSEFJ-c658_180x180.png" alt="航嘉智慧云USB充电站" src="images/cix_s1fqluuahgi7aaq_hsefj-c658_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">航嘉智慧云USB充电站</h4>
                                                        <h6 class="rmd-product-subtitle">快充 防雷 极速更安全</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>78</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                                <div class="box-product-sign" style="background-color: rgb(0, 195, 245);">
                                                新品
                                                </div>
                                        </a>
                                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="home-full-box" style="">
        <div class="mzcontainer">
            <div class="home-panel home-rmd home-floor">
                <div class="home-panel-topbar">
                    <h2 class="home-panel-title">手机周边</h2>
                    <h6 class="home-panel-subtitle">
                                <a href="http://lists.meizu.com/page/list?categoryid=81&amp;rc=lc" target="_top" data-mtype="store_index_kwd_4_2" data-mdesc="第4个楼层上方小导航第2个按钮	">保护套/后盖/贴膜</a>
                                <a href="http://lists.meizu.com/page/list?categoryid=82&amp;rc=lc" target="_top" data-mtype="store_index_kwd_4_2" data-mdesc="第4个楼层上方小导航第2个按钮	">数据线/电源适配器</a>
                    </h6>
                </div>
                <div class="home-rmd-main">
                    <div class="home-rmd-cotent">
                                <div class="rmd-box rmd-box-ad rmd-box-ad-1">
                                    <a href="http://detail.meizu.com/item/mx6_loop.html" target="_top" data-mtype="store_index_kw_4_1" data-mdesc="第4个楼层第1个坑">
                                        <img class="home-img-lazy" data-original="http://storeimg.meizu.com/product/1468915931-14085.png" src="images/1468915931-14085.png" alt="Loop Jacket智能保护套（MX6）" style="display: inline;">
                                    </a>
                                        <div class="rmd-box-ad-module">
                                            <a class="rmd-box-ad-btn" target="_top" href="http://detail.meizu.com/item/mx6_loop.html">
                                            立即购买
                                            </a>
                                        </div>
                                </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/meilannote3_tpu.html" target="_top" data-mtype="store_index_kw_4_2" data-mdesc="第4个楼层第2个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/19/Cix_s1eN6ZKAaP7AAARJiQJn9AA510_180x180.png" alt="魅蓝note3 缤纷软胶保护壳" src="images/cix_s1en6zkaap7aaarjiqjn9aa510_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">魅蓝note3 缤纷软胶保护壳</h4>
                                                        <h6 class="rmd-product-subtitle">多彩缤纷  拜耳TPU材质</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>49</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/type_c.html" target="_top" data-mtype="store_index_kw_4_3" data-mdesc="第4个楼层第3个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/60/Cix_s1feCxCATgWnAAHXId4TWtA603_180x180.png" alt="魅族Type-C 金属数据线" src="images/cix_s1fecxcatgwnaahxid4twta603_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">魅族Type-C 金属数据线</h4>
                                                        <h6 class="rmd-product-subtitle">支持2A快速充电 耐用又放心</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>39</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                                <div class="box-product-sign" style="background-color: rgb(0, 195, 245);">
                                                新品
                                                </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/type_c_adapter.html" target="_top" data-mtype="store_index_kw_4_4" data-mdesc="第4个楼层第4个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/60/CnQOjVfeC22AfT-XAAK3xr-97Nw634_180x180.png" alt="魅族 Type-C转接头" src="images/cnqojvfec22aft-xaak3xr-97nw634_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">魅族 Type-C转接头</h4>
                                                        <h6 class="rmd-product-subtitle">小巧便携 随心转换</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>19</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                                <div class="box-product-sign" style="background-color: rgb(0, 195, 245);">
                                                新品
                                                </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/pro6_colour.html" target="_top" data-mtype="store_index_kw_4_5" data-mdesc="第4个楼层第5个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/0D/CnQOjVeEp8-AZ6OzAAYG7LEKpLA174_180x180.png" alt="PRO 6 原装保护壳" src="images/cnqojveep8-az6ozaayg7lekpla174_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">PRO 6 原装保护壳</h4>
                                                        <h6 class="rmd-product-subtitle">全面保护 原机手感</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>168</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/metal_usb.html" target="_top" data-mtype="store_index_kw_4_6" data-mdesc="第4个楼层第6个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/60/Cix_s1feC8eAYv5UAAIlZk346XY373_180x180.png" alt="魅族二合一金属数据线" src="images/cix_s1fec8eayv5uaailzk346xy373_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">魅族二合一金属数据线</h4>
                                                        <h6 class="rmd-product-subtitle">二合一 全兼容</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>49</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                                <div class="box-product-sign" style="background-color: rgb(0, 195, 245);">
                                                新品
                                                </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/mx6_pasting.html" target="_top" data-mtype="store_index_kw_4_7" data-mdesc="第4个楼层第7个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/1E/Cix_s1eeo-6AOHVYAACLZCLhgmY372_180x180.png" alt="MX6高透保护膜" src="images/cix_s1eeo-6aohvyaaclzclhgmy372_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">MX6高透保护膜</h4>
                                                        <h6 class="rmd-product-subtitle">高清透亮 抗刮耐磨</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>29</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/note3_loop.html" target="_top" data-mtype="store_index_kw_4_8" data-mdesc="第4个楼层第8个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/1D/CnQOjVecte-AN9BBAAKzNe6JVvs491_180x180.png" alt="魅蓝note3 智能保护套" src="images/cnqojvecte-an9bbaakzne6jvvs491_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">魅蓝note3 智能保护套</h4>
                                                        <h6 class="rmd-product-subtitle">环窗操作 便捷高效</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>99</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/max_pasting.html" target="_top" data-mtype="store_index_kw_4_9" data-mdesc="第4个楼层第9个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/57/CnQOjVfWCdCAJ6CJAAHjh-SvOeM021_180x180.png" alt="魅蓝Max高透保护膜" src="images/cnqojvfwcdcaj6cjaahjh-svoem021_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">魅蓝Max高透保护膜</h4>
                                                        <h6 class="rmd-product-subtitle">更懂屏幕的保护膜</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>29</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                                <div class="box-product-sign" style="background-color: rgb(0, 195, 245);">
                                                新品
                                                </div>
                                        </a>
                                    </div>

                                    <div class="rmd-box rmd-box-product">
                                        <a href="http://detail.meizu.com/item/pro6_loop.html?skuid=1917" target="_top" data-mtype="store_index_kw_4_10" data-mdesc="第4个楼层第10个坑">
                                            <div class="rmd-product-detail">
                                                <img class="home-img-lazy" data-original="http://open.file.meizu.com/group1/M00/00/5C/CnQOjVfXbSeARD4tAAZYK3KJlH8271_180x180.png" alt="PRO 6环窗智能保护套" src="images/cnqojvfxbseard4taazyk3kjlh8271_180x180.png" style="display: inline;" height="180" width="180">
                                                <div class="rmd-product-desc">
                                                    <h4 class="rmd-product-title">PRO 6环窗智能保护套</h4>
                                                        <h6 class="rmd-product-subtitle">个性表盘 随心搭配</h6>
                                                    <!-- TODO 替换为内容-->
                                                    <p class="rmd-product-price">
                                                        ¥
                                                        <span>99</span>
                                                    
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="home-full-box" style="height: 80px;"></div>
<!-- common footer -->

<div class="site-footer">
    <div class="mzcontainer">
        <div class="site-footer-service">
            <ul class="clearfix">
                <li class="service-item">
                  <span class="service-icon service-icon-seven"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">7天</span>
                    <span class="service-desc-normal">无理由退货</span>
                  </p>
                </li>
                <li class="service-split-line">
                  <span></span>
                </li>
                <li class="service-item">
                  <span class="service-icon service-icon-fifty"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">15天</span>
                    <span class="service-desc-normal">换货保障</span>
                  </p>
                </li>
                <li class="service-split-line">
                  <span></span>
                </li>
                <li class="service-item">
                  <span class="service-icon service-icon-one"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">1年</span>
                    <span class="service-desc-normal">免费保修</span>
                  </p>
                </li>
                <li class="service-split-line">
                  <span></span>
                </li>
                <li class="service-item">
                  <span class="service-icon service-icon-speed"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">百城</span>
                    <span class="service-desc-normal">速达</span>
                  </p>
                </li>
                <li class="service-split-line">
                  <span></span>
                </li>
                <li class="service-item">
                  <span class="service-icon service-icon-by"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">全场</span>
                    <span class="service-desc-normal">包邮</span>
                  </p>
                </li>
                <li class="service-split-line">
                  <span></span>
                </li>
                <li class="service-item">
                  <span class="service-icon service-icon-map"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">2000多家</span>
                    <span class="service-desc-normal">专卖店</span>
                  </p>
                </li>
            </ul>
        </div>
        <div class="site-footer-navs clearfix">
            <div class="footer-nav-item">
                <h4 class="footer-nav-title">帮助说明</h4>
                <ul>
                    <li><a href="http://store.meizu.com/explain/payment_help.html" target="_top">支付方式</a></li>
                    <li><a href="http://store.meizu.com/explain/deliverynote.html" target="_top">配送说明</a></li>
                    <li><a href="http://store.meizu.com/explain/warranty_services.html" target="_top">售后服务</a></li>
                    <li><a href="http://store.meizu.com/explain/payment_helps.html" target="_top">付款帮助</a></li>
                </ul>
            </div>
            <div class="footer-nav-item">
                <h4 class="footer-nav-title">Flyme</h4>
                <ul>
                    <li><a target="_top" href="https://open.flyme.cn/">开放平台</a></li>
                    <li><a target="_top" href="http://www.flyme.cn/firmware.jsp">固件下载</a></li>
                    <li><a target="_top" href="http://app.flyme.cn/">软件商店</a></li>
                    <li><a target="_top" href="http://finder.flyme.cn/">查找手机</a></li>
                </ul>
            </div>
            <div class="footer-nav-item">
                <h4 class="footer-nav-title">关于我们</h4>
                <ul>
                    <li><a target="_top" href="http://www.meizu.com/about.html">关于魅族</a></li>
                    <li><a target="_top" href="http://hr.meizu.com/">加入我们</a></li>
                    <li><a target="_top" href="http://www.meizu.com/contact.html">联系我们</a></li>
                    <li><a target="_top" href="http://www.meizu.com/legalStatement.html">法律声明</a></li>
                </ul>
            </div>
            <div class="footer-nav-item">
                <h4 class="footer-nav-title">关注我们</h4>
                <ul>
                    <li><a target="_top" href="http://weibo.com/meizumobile">新浪微博</a></li>
                    <li><a target="_top" href="http://t.qq.com/meizu_tech">腾讯微博</a></li>
                    <li><a target="_top" href="http://user.qzone.qq.com/2762957059">QQ空间</a></li>
                    <li>
                        <a class="meizu-footer-wechat">
                            官方微信
                            <img src="weixin.png" alt="微信二维码">
                        </a>
                    </li>
                </ul>
            </div>
            <div class="footer-nav-custom">
                <h4 class="nav-custom-title">24小时全国服务热线</h4>
                <a href="tel:400-788-3333"><h3 class="nav-custom-number">400-788-3333</h3></a>
                <a class="nav-custom-btn" href="http://live-i.meizu.com/live800/chatClient/chatbox.jsp?companyID=8957&amp;configID=4&amp;enterurl=">
                    <img src="images/20x21xiaoshi.png" height="21" width="20">
                    24小时在线客服
                </a>
            </div>
        </div>
        <div class="site-footer-end">
            <p>
                ©2016 Meizu Telecom Equipment Co., Ltd. All rights reserved.
                <a target="_top" href="http://www.miitbeian.gov.cn/" hidefocus="true">备案号：粤ICP备13003602号-2</a>
                <a target="_top" href="http://www2.res.meizu.com/zh_cn/images/common/icp2.jpg" hidefocus="true">经营许可证编号：粤B2-20130198</a>
                <a target="_top" href="http://www2.res.meizu.com/zh_cn/images/common/com_licence.jpg" hidefocus="true">营业执照</a>
                <a target="_top" rel="nofollow" href="http://210.76.65.188/" hidefocus="true">
                    <img src="images/footer-copy-1.png">
                </a>
                <a target="_top" rel="nofollow" href="http://210.76.65.188/webrecord/innernet/Welcome.jsp?bano=4404013010531" hidefocus="true">
                    <img src="images/footer-copy-2.png">
                </a>
                <a target="_top" rel="nofollow" href="https://credit.szfw.org/CX20151204012550820380.html" hidefocus="true">
                    <img src="images/trust-icon.png">
                </a>
            </p>
        </div>
    </div>
</div>
<script charset="utf-8" src="js/v.js"></script><script src="hm.js"></script><script src="js/flow.js"></script><script src="js/analytics-min.js"></script><script charset="utf-8" src="js/site-lib.js"></script>
<script type="text/javascript" src="js/site-base.js"></script>
<script>var $=require("common:lib/jquery/jquery-1.11.3");require("common:lib/jquery/jquery-migrate-1.2.1"),window.$=$,window.jQuery=$,window.jquery=$,$(function(){require("common:widgets/site-topbar/topbar"),require("common:widgets/site-header/header")});</script><script type="text/javascript" src="pkg-home.js"></script>
<script charset="utf-8">var $=require("common:lib/jquery/jquery-1.11.3");
$(function(){
    require("home:widgets/settlement-ad/settlement-ad"),
    require("home:widgets/home-slider/home-slider"),
    require("home:widgets/home-recommend/home-recommend"),
    require("home:widgets/home-category/home-category"),
    require("home:widgets/home-gotop/home-gotop"),
    require("home:js/jquery.lazyload/jquery.lazyload"),
    $("img.home-img-lazy").lazyload({})
});
</script>
<script>
    var __mzt = __mzt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "http://sccom.res.meizu.com/js/analytics-min.js";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8">
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = ('https:' == document.location.protocol ? 'https://tongji-res.meizu.com' : 'http://tongji-res1.meizu.com') + '/resources/tongji/flow.js';
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript">
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?2a0c04774115b182994cfcacf4c122e9";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>


</body>
</html>
