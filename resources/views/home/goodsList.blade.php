<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta charset="utf-8">
    <title>手机-魅族商城</title>
    <meta name="description" content="魅族商城提供各种手机产品，包括，品质保障，可按推荐、商品上架时间，价格等放心购买，魅族商城将为您提供最好的子类目名称产品，全场包邮，7天无理由退货，15天换货保障。">
    <meta name="keywords" content="手机,，魅族，魅族手机，魅族商城">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="favicon.ico" rel="icon" type="image/x-icon">
    <!-- common css -->
    
    <!--[if lt IE 9]>
    <script>
        var c = ["log","debug","info","warn","exception","assert","dir","dirxml","trace","group","groupCollapsed","groupEnd","profile","profileEnd","count","clear","time","timeEnd","timeStamp","table","error"];
        window.console = {};
        for(var i = 0; i < c.length; i++){
            window.console[c[i]] = function(){

            }
        }
    </script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://store.res.meizu.com/resources/php/store/static/common/lib/html5shiv/dist/html5shiv.min.js"></script>
    <![endif]-->
    
    
<link rel="stylesheet" type="text/css" href="css/goodsList.css" media="all">
</head>
<body>
        <!-- common header -->
<div class="site-topbar clearfix">
    <div class="mzcontainer">
        <div class="topbar-nav">
        <a href="http://www.meizu.com/" target="_top" data-mtype="store_index_yt_1" data-mdesc="页头中第1个">魅族官网</a>
        <a href="http://store.meizu.com/index.html" data-mtype="store_index_yt_2" data-mdesc="页头中第2个">魅族商城</a>
        <a href="http://www.flyme.cn/" target="_top" data-mtype="store_index_yt_3" data-mdesc="页头中第3个">Flyme</a>
        <a href="http://retail.meizu.com/index.html" target="_top" data-mtype="store_index_yt_4" data-mdesc="页头中第4个">专卖店</a>
        <a href="http://service.meizu.com/index.html" target="_top" data-mtype="store_index_yt_5" data-mdesc="页头中第5个">服务</a>
        <a href="http://bbs.meizu.cn/" target="_top" data-mtype="store_index_yt_6" data-mdesc="页头中第6个">社区</a>
        </div>
        <div class="topbar-right">
            <ul class="topbar-info">
                <li class="topbar-info-msg" id="MzTopbarMsg">
                    <a class="topbar-link" href="http://me.meizu.com/member/message/index" target="_top">消息</a>
                    <span class="msg-tag" id="MzMsgTag"></span>
                </li>
                <li>
                    <a class="topbar-link" href="http://me.meizu.com/member/favorite/index" target="_top">我的收藏<div class="topbar-new">new</div></a>
                </li>
                <li class="topbar-order-msg">
                    <a class="topbar-link" href="http://ordercenter.meizu.com/list/index.html" target="_top">我的订单</a>
                    <span class="msg-tag" id="MzOrderMsgTag"></span>
                </li>
                <li class="mz_login">
                    <a class="topbar-link site-login" href="https://login.flyme.cn/vCodeLogin?sid=unionlogin&amp;service=store&amp;autodirct=true&amp;useruri=http://store.meizu.com/member/login.htm?useruri=http://lists.meizu.com/page/list?categoryid=76&amp;rc=cb" data-href="https://login.flyme.cn/vCodeLogin?sid=unionlogin&amp;service=store&amp;autodirct=true&amp;useruri=http://store.meizu.com/member/login.htm?useruri=">登录</a>
                </li>
                <li class="mz_login">
                    <a class="topbar-link" href="https://member.meizu.com/register" target="_top">注册</a>
                </li>
                <li class="topbar-info-member" style="display: none;">
                    <a class="topbar-link" href="http://me.meizu.com/member/index" target="_top">
                        <span id="MzUserName" class="site-member-name">MEIZU</span>的商城
                    </a>
                    <div class="site-member-items">
                        <a class="site-member-link" href="http://me.meizu.com/member/address/index" target="_top" data-mtype="store_index_yt_9_1" data-mdesc="我的商城下拉框1">地址管理</a>
                        <a class="site-member-link" href="http://me.meizu.com/member/repo_ticket/index" target="_top" data-mtype="store_index_yt_9_2" data-mdesc="我的商城下拉框2">我的回购券</a>
                        <a class="site-member-link" href="http://me.meizu.com/member/advice/index" target="_top" data-mtype="store_index_yt_9_3" data-mdesc="我的商城下拉框3">问题反馈</a>
                        <a class="site-member-link site-logout" href="http://store.meizu.com/member/logout.htm?useruri=http://store.meizu.com" data-href="http://store.meizu.com/member/logout.htm?useruri=" data-mtype="store_index_yt_9_4" data-mdesc="我的商城下拉框4">退出</a>
                    </div>
                </li>
            </ul>
            <div class="topbar-info-pop"></div>
        </div>
    </div>
</div><div class="site-header">
    <div class="mzcontainer">
        <div class="header-logo">
            <a href="http://www.meizu.com/" target="_top">
                <img src="images/logo-header.png" srcset="http://store.res.meizu.com/resources/php/store/images/logo-header@2x.png 2x" alt="魅族科技" data-mtype="store_index_dh_logo" data-mdesc="logo" height="20" width="115">
            </a>
        </div>
        <div class="header-nav">
            <ul class="nav-list">
                    <li class="nav-item">
                        <a class="nav-item-link" href="javascrip:" onclick="return false" target="_top">PRO手机</a>
                        <div class="nav-item-children">
                            <ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_pro6.html?rc=sd" target="_top" data-mtype="store_index_dh_1_1" data-mdesc="导航中第1个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0E/CnQOjVeE2E-AAGZCABHUf4HwKyw117_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族PRO 6</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥2499</span>
                                                    ¥
                                                    <span>2299</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_pro5.html?rc=sd" target="_top" data-mtype="store_index_dh_1_2" data-mdesc="导航中第1个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://storeimg.meizu.com/product/1450928403@126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族PRO 5</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>2199</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-link" href="javascrip:" onclick="return false" target="_top">魅蓝手机</a>
                        <div class="nav-item-children">
                            <ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_u10.html?rc=sd" target="_top" data-mtype="store_index_dh_2_1" data-mdesc="导航中第2个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/6C/Cix_s1fodIqAQprmAAjwkwqC8nw622_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝U10</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>999</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_max1.html" target="_top" data-mtype="store_index_dh_2_2" data-mdesc="导航中第2个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/5C/CnQOjVfZBeOAIVRDAAscQInnNPY084_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 Max</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1699</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilanU20.html?rc=sd" target="_top" data-mtype="store_index_dh_2_3" data-mdesc="导航中第2个下第3个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/3F/Cix_s1fDkdyAb0YGAEeDe0CxIV8257_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 U20</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1099</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_e1.html?rc=sd" target="_top" data-mtype="store_index_dh_2_4" data-mdesc="导航中第2个下第4个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/2C/CnQOjVeq0uGASgUFAAzQ2opb7qI013_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 E</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1299</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan3s.html?rc=sd" target="_top" data-mtype="store_index_dh_2_5" data-mdesc="导航中第2个下第5个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/26/Cix_s1epgR6AD8KbAA2l_exLROk404_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝3s</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>699</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meilan_note3.html?rc=sd" target="_top" data-mtype="store_index_dh_2_6" data-mdesc="导航中第2个下第6个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://storeimg.meizu.com/product/1459927797@126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅蓝 note3</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>799</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-link" href="javascrip:" onclick="return false" target="_top">MX手机</a>
                        <div class="nav-item-children">
                            <ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/mx6.html?rc=sd" target="_top" data-mtype="store_index_dh_3_1" data-mdesc="导航中第3个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/19/Cix_s1eN3IiASxVXAA9IpQ8-shg169_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族MX6</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1999</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_mx5.html?rc=sd" target="_top" data-mtype="store_index_dh_3_2" data-mdesc="导航中第3个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/40/CnQOjVfD9pWAS6VZAAutvljEfx8425_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族MX5</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1499</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_mx5e.html?rc=sd" target="_top" data-mtype="store_index_dh_3_3" data-mdesc="导航中第3个下第3个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/4F/Cix_s1fNIqaAPDEJAAutvljEfx8334_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族MX5e 经典版</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>1499</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-link" href="javascrip:" onclick="return false" target="_top">精选配件</a>
                        <div class="nav-item-children">
                            <ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_ep51.html?skuid=1122" target="_top" data-mtype="store_index_dh_4_1" data-mdesc="导航中第4个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFrxyAQWpEAAEcHQ8zBWo729_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族 EP51蓝牙运动耳机</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>269</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_hd50.html?skuid=839" target="_top" data-mtype="store_index_dh_4_2" data-mdesc="导航中第4个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0E/CnQOjVeFsA2AMvwBAATZrGgT1ak941_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族 HD50 头戴式耳机</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>399</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_bag.html" target="_top" data-mtype="store_index_dh_4_3" data-mdesc="导航中第4个下第3个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/6E/CnQOjVfqPd-AEjICAAVBTlzLU4U578_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族 休闲旅行双肩包</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>199</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/mpower_m8e.html?skuid=1061" target="_top" data-mtype="store_index_dh_4_4" data-mdesc="导航中第4个下第4个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/0F/Cix_s1eFsDmAYpT5AAKdyPUHRpQ307_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族移动电源（标准版）</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>99</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/lifeme_bts30.html" target="_top" data-mtype="store_index_dh_4_5" data-mdesc="导航中第4个下第5个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/66/CnQOjVfiK1qABWWMAAULoxmuYLI896_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族 Lifeme BTS30 蓝牙音箱</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>399</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                                    <li class="menu-product-more">
                                        <div class="menu-more-links">
                                            <ul>
                                                    <li class="menu-more-row"><a href="http://lists.meizu.com/page/list?categoryid=79&amp;rc=sdsd" target="_top"><img src="images/1467696166-40112.png" class="menu-more-img" height="28" width="28">耳机 / 音箱</a>
                                                    </li>
                                                    <li class="menu-more-row"><a href="http://lists.meizu.com/page/list?categoryid=80&amp;rc=sd" target="_top"><img src="images/1467696197-95413.png" class="menu-more-img" height="28" width="28">路由器 / 移动电源</a>
                                                    </li>
                                                    <li class="menu-more-row"><a href="http://lists.meizu.com/page/list?categoryid=81&amp;rc=sd" target="_top"><img src="images/1467696220-57637.png" class="menu-more-img" height="28" width="28">保护套 / 后盖 / 贴膜</a>
                                                    </li>
                                                    <li class="menu-more-row"><a href="http://lists.meizu.com/page/list?categoryid=82&amp;rc=sd" target="_top"><img src="images/1467696242-24236.png" class="menu-more-img" height="28" width="28">数据线 / 电源适配器</a>
                                                    </li>
                                                    <li class="menu-more-row"><a href="http://lists.meizu.com/page/list?categoryid=83&amp;rc=sd" target="_top"><img src="images/1467705893-97644.png" class="menu-more-img" height="28" width="28">周边配件</a>
                                                    </li>
                                            </ul>
                                        </div>
                                    </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item-link" href="javascrip:" onclick="return false" target="_top">智能硬件</a>
                        <div class="nav-item-children">
                            <ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_luyouqi.html?skuid=175" target="_top" data-mtype="store_index_dh_5_1" data-mdesc="导航中第5个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/56/CnQOjVfQCbCAdFjzAAL9OUJBRPk818_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族智能路由器</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>99</span>
                                                        起
                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/XunLei_TB01.html?skuid=1780" target="_top" data-mtype="store_index_dh_5_2" data-mdesc="导航中第5个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/49/CnQOjVfI2aKAQBNbAAI9fvIkJl0379_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">迅雷下载宝 TB01</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥159</span>
                                                    ¥
                                                    <span>99</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/lexin_mambo.html" target="_top" data-mtype="store_index_dh_5_3" data-mdesc="导航中第5个下第3个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/68/CnQOjVfiPf-ASw6XAAX6eBRrgp8709_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">乐心运动手环mambo</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥99</span>
                                                    ¥
                                                    <span>89</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/lefan_jingdian.html?skuid=380" target="_top" data-mtype="store_index_dh_5_4" data-mdesc="导航中第5个下第4个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/58/Cix_s1fWIo6AY06tAAMbEySAYxc161_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">乐范魔力贴（经典版）</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>299</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/BTP_2185.html" target="_top" data-mtype="store_index_dh_5_5" data-mdesc="导航中第5个下第5个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/6E/Cix_s1fqIsOAeUInAAWX1-z6F08803_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">北通阿修罗游戏手柄</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥158</span>
                                                    ¥
                                                    <span>148</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/ROMOSS_A10.html?skuid=1766" target="_top" data-mtype="store_index_dh_5_6" data-mdesc="导航中第5个下第6个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/init-1x1.jpg" data-src="http://open.file.meizu.com/group1/M00/00/6E/CnQOjVfqI1aARrMfAACVRHk1HFM020_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">罗马仕AC10快充适配器</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥39</span>
                                                    ¥
                                                    <span>36</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                            </ul>
                        </div>
                    </li>
            </ul>
        </div>

        <div class="header-cart" id="MzHeaderCart">
            <a href="http://store.meizu.com/cart" target="_top">
                <div class="header-cart-wrap">
                    <span class="header-cart-icon"></span>
                    购物车
                    <span id="MzHeaderCartNum" class="header-cart-num" data-extcls="existence">0</span>
                    <div class="header-cart-spacer"></div>
                </div>
            </a>
            <div class="header-cart-detail"><div class="header-cart-empty" data-load="正在加载购物车信息 ..." data-empty="购物车还没有商品，快购买吧！">购物车还没有商品，快购买吧！</div>
</div>
        </div>
    </div>
    <div style="display: none;" id="MzNavMenu" class="header-nav-menu">
        <div class="mzcontainer"><ul class="menu-product-list">
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_pro6.html?rc=sd" target="_top" data-mtype="store_index_dh_1_1" data-mdesc="导航中第1个下第1个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/cnqojvee2e-aagzcabhuf4hwkyw117_126x126.png" data-src="http://open.file.meizu.com/group1/M00/00/0E/CnQOjVeE2E-AAGZCABHUf4HwKyw117_126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族PRO 6</p>
                                                <p class="menu-product-price">
                                                        <span class="originPrice">¥2499</span>
                                                    ¥
                                                    <span>2299</span>

                                                </p>
                                            </a>
                                        </li>
                                        <li class="menu-product-item">
                                            <a href="http://detail.meizu.com/item/meizu_pro5.html?rc=sd" target="_top" data-mtype="store_index_dh_1_2" data-mdesc="导航中第1个下第2个坑">
                                                <div class="menu-product-figure">
                                                    <img src="images/1450928403@126x126.png" data-src="http://storeimg.meizu.com/product/1450928403@126x126.png" height="150" width="100">
                                                </div>
                                                <p class="menu-product-name">魅族PRO 5</p>
                                                <p class="menu-product-price">

                                                    ¥
                                                    <span>2199</span>

                                                </p>
                                            </a>
                                        </li>
                                <!-- more -->
                            </ul></div>
    </div>
</div>

<!--下面是模板标签，不可删除-->

<main class="wrapper" id="main">
    <div class="container">
        <section class="crumbs clearfix">
            <a data-mtype="store_list_mbx_1" href="http://store.meizu.com/index.html">首页</a> &nbsp;&gt;&nbsp;
            <span class="crumbs-level" id="crumbsLevel"><a href="#" data-mtype="store_list_mbx_2" data-cateid="73">全部商品</a>&nbsp;&gt;&nbsp;手机</span>
        </section>

        <section class="selector" id="selector">
            <!--  分类部分 start -->
                <div class="sl-category" id="slCategory">
                </div>
            <div class="sl-prop" id="slProp">
                        <div class="sl-line-wrap">
                            <div class="mod-key"><span>网络：</span></div>
                            <div class="mod-value">
                                <div class="mod-value-list">
                                    <ul>
                                           <li class="vm-all">
                                                <a title="全部" data-value="79:18238,18239,43,40,41,42" href="#">全部</a>
                                           </li>
                                                <li>
                                                    <a data-value="79:18238,18239" data-mtype="store_list_xf_1_1" title="全网通版" href="#">全网通版</a>
                                                </li>
                                                <li>
                                                    <a data-value="79:43" data-mtype="store_list_xf_1_2" title="移动/联通双4G版" href="#">移动/联通双4G版</a>
                                                </li>
                                                <li>
                                                    <a data-value="79:43,40" data-mtype="store_list_xf_1_3" title="移动4G版" href="#">移动4G版</a>
                                                </li>
                                                <li>
                                                    <a data-value="79:41,43" data-mtype="store_list_xf_1_4" title="联通4G版" href="#">联通4G版</a>
                                                </li>
                                                <li>
                                                    <a data-value="79:42" data-mtype="store_list_xf_1_5" title="电信4G版" href="#">电信4G版</a>
                                                </li>
                                    </ul>
                                </div>
                            </div>
                                <div class="mod-ext">
                                    <a class="sl-e-more J_extMore" href="javascript:;" style="">更多<i></i></a>
                                </div>

                        </div>
                        <div class="sl-line-wrap">
                            <div class="mod-key"><span>系列：</span></div>
                            <div class="mod-value">
                                <div class="mod-value-list">
                                    <ul>
                                           <li class="vm-all">
                                                <a title="全部" data-value="80:18778,9,12529,18382,3,19475,5,10,11760,868,1133,18237,19242,19243,20374,20100,20579,20580,18732,8,18136,20098" href="#">全部</a>
                                           </li>
                                                <li>
                                                    <a data-value="80:18778,9,12529,18382" data-mtype="store_list_xf_2_1" title="PRO系列" href="#">PRO系列</a>
                                                </li>
                                                <li>
                                                    <a data-value="80:3,19475,5,10,11760,868,1133,18237,19242,19243,20374,20100,20579,20580,18732" data-mtype="store_list_xf_2_2" title="魅蓝系列" href="#">魅蓝系列</a>
                                                </li>
                                                <li>
                                                    <a data-value="80:8,18136,20098" data-mtype="store_list_xf_2_3" title="MX系列" href="#">MX系列</a>
                                                </li>
                                    </ul>
                                </div>
                            </div>
                                <div class="mod-ext">
                                    <a class="sl-e-more J_extMore" href="javascript:;" style="">更多<i></i></a>
                                </div>

                        </div>
            </div>
        </section>

    <section class="filter clearfix" id="filter">
        <div class="filter-order" id="J_filterOrder">
            <a class="active" data-tag="0" data-mtype="store_list_sx_1" href="#">
                推荐
            </a>
            <a data-tag="1" data-mtype="store_list_sx_2" href="#">
                新品
            </a>
            <a data-tag="2" data-mtype="store_list_sx_3" href="#">
                价格<i class="icon-arrow-down"></i>
            </a>
        </div>
        <div class="filter-condition" id="J_filterCondition">
            <label class="bs-checkbox" data-mtype="store_list_sx_xz">
                <i></i>仅显示有货商品
            </label>
        </div>
    </section>

    <section class="goods-list" id="goodsList">
        <ul class="goods-list-wrap clearfix" id="goodsListWrap">
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_1" target="_top" href="http://detail.meizu.com/item/meilan_max1.html" title="">
                                <img src="images/cix_s1fm2_iasnmvaascqinnnpy866.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_1" target="_top" href="http://detail.meizu.com/item/meilan_max1.html" title="">
                            魅蓝 Max
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                1699
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_2" target="_top" href="http://detail.meizu.com/item/meilan_e1.html" title="">
                                <img src="images/cix_s1eqmsmaba98aa0rkx2xjke895.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_2" target="_top" href="http://detail.meizu.com/item/meilan_e1.html" title="">
                            魅蓝 E
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                1299
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_3" target="_top" href="http://detail.meizu.com/item/meilan3s.html" title="">
                                <img src="images/cnqojvfh7hsamdqfaa1zp1bmria429.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_3" target="_top" href="http://detail.meizu.com/item/meilan3s.html" title="">
                            魅蓝3s
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                699
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_4" target="_top" href="http://detail.meizu.com/item/mx6.html" title="">
                                <img src="images/cix_s1envciafnfyaa9ipq8-shg689.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_4" target="_top" href="http://detail.meizu.com/item/mx6.html" title="">
                            魅族 MX6
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                1799
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_5" target="_top" href="http://detail.meizu.com/item/meilan_note3.html" title="">
                                <img src="images/cnqojvfiqsgab6kaaat5u4gtfze690.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_5" target="_top" href="http://detail.meizu.com/item/meilan_note3.html" title="">
                            魅蓝note3
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                799
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_6" target="_top" href="http://detail.meizu.com/item/meilannote3.html" title="">
                                <img src="images/1459826197-26324.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_6" target="_top" href="http://detail.meizu.com/item/meilannote3.html" title="">
                            魅蓝note3
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                799
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_7" target="_top" href="http://detail.meizu.com/item/meizu_pro6.html" title="">
                                <img src="images/1460439990-20010.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_7" target="_top" href="http://detail.meizu.com/item/meizu_pro6.html" title="">
                            魅族PRO 6
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                2299
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_8" target="_top" href="http://detail.meizu.com/item/meilan_u10.html" title="">
                                <img src="images/cnqojvfjruwafzq1aajwkwqc8nw575.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_8" target="_top" href="http://detail.meizu.com/item/meilan_u10.html" title="">
                            魅蓝U10
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                999
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_9" target="_top" href="http://detail.meizu.com/item/meilan_3s_lihe.html" title="">
                                <img src="images/cnqojvemraiafnktaawhufu3qdi233.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_9" target="_top" href="http://detail.meizu.com/item/meilan_3s_lihe.html" title="">
                            魅蓝3s 礼盒版
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                799
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_10" target="_top" href="http://detail.meizu.com/item/meilan3.html" title="">
                                <img src="images/cnqojvfisvuab-uraaf5c4nunpi462.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_10" target="_top" href="http://detail.meizu.com/item/meilan3.html" title="">
                            魅蓝3
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                599
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_11" target="_top" href="http://detail.meizu.com/item/meilannote3_lihe.html" title="">
                                <img src="images/1464344073-89074.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_11" target="_top" href="http://detail.meizu.com/item/meilannote3_lihe.html" title="">
                            魅蓝note3 礼盒版
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                899
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_12" target="_top" href="http://detail.meizu.com/item/meilanU20.html" title="">
                                <img src="images/cnqojvfc0gkamaw4aeede0cxiv8386.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_12" target="_top" href="http://detail.meizu.com/item/meilanU20.html" title="">
                            魅蓝 U20
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                1099
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_13" target="_top" href="http://detail.meizu.com/item/meilan3_lihe.html" title="">
                                <img src="images/1464343960-34269.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_13" target="_top" href="http://detail.meizu.com/item/meilan3_lihe.html" title="">
                            魅蓝3 礼盒版
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                899
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_14" target="_top" href="http://detail.meizu.com/item/meilan_metal.html" title="">
                                <img src="images/1452573120-88051.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_14" target="_top" href="http://detail.meizu.com/item/meilan_metal.html" title="">
                            魅蓝 metal
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                999
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_15" target="_top" href="http://detail.meizu.com/item/meizu_pro5.html" title="">
                                <img src="images/1450083282-62207.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_15" target="_top" href="http://detail.meizu.com/item/meizu_pro5.html" title="">
                            魅族PRO 5
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                2199
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_16" target="_top" href="http://detail.meizu.com/item/meizu_mx5e.html" title="">
                                <img src="images/1452572130-53664.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_16" target="_top" href="http://detail.meizu.com/item/meizu_mx5e.html" title="">
                            魅族MX5e 经典版
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                1499
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_17" target="_top" href="http://detail.meizu.com/item/meizu_mx5.html" title="">
                                <img src="images/1452572014-69588.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_17" target="_top" href="http://detail.meizu.com/item/meizu_mx5.html" title="">
                            魅族 MX5
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                1499
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_18" target="_top" href="http://detail.meizu.com/item/metal_lihe.html" title="">
                                <img src="images/1454552324-82391.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_18" target="_top" href="http://detail.meizu.com/item/metal_lihe.html" title="">
                            魅蓝metal礼盒套装
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                1099
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_19" target="_top" href="http://detail.meizu.com/item/pro5_music.html" title="">
                                <img src="images/1461749056-33170.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_19" target="_top" href="http://detail.meizu.com/item/pro5_music.html" title="">
                            魅族PRO 5音·悦·人套装
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                2899
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_20" target="_top" href="http://detail.meizu.com/item/pro5_gold.html" title="">
                                <img src="images/1454470438-82142.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_20" target="_top" href="http://detail.meizu.com/item/pro5_gold.html" title="">
                            魅族PRO 5金色套装
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                2399
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_21" target="_top" href="http://detail.meizu.com/item/meilan_e_xingji.html" title="">
                                <img src="images/cix_s1fd9xoaj_huaarxgz2dbhg353.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_21" target="_top" href="http://detail.meizu.com/item/meilan_e_xingji.html" title="">
                            魅蓝 E星际迷航版限量收藏版
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                1499
                            </i>
                        </dd>
                    </dl>
                </li>
                <li class="gl-item">
                    <dl class="gl-item-wrap">
                        <dd class="mod-pic">
                            <a data-mtype="store_list_kw_22" target="_top" href="http://detail.meizu.com/item/meilan_note2.html" title="">
                                <img src="images/1454470847-34651.png@240x240.png" height="220" width="220">
                            </a>
                        </dd>
                        <dd class="mod-name">
                            <a data-mtype="store_list_kw_22" target="_top" href="http://detail.meizu.com/item/meilan_note2.html" title="">
                            魅蓝 note2
                            </a>
                        </dd>
                        <dd class="mod-price">
                            <em>￥</em>
                            <i>
                                799
                            </i>
                        </dd>
                    </dl>
                </li>
        </ul>
    </section>
    <section class="empty clearfix" id="empty" style="display: none;">
        <div class="empty-bd">
            <div class="empty-bd-pic"></div>
            <div class="empty-bd-info">
                <h4 class="yahei">抱歉没有找到相关商品</h4>
                <p>建议您：<br>
                    1.适当减少筛选条件<br>
                    2.尝试其他关键字
                </p>
            </div>
        </div>
    </section>

    <section style="display: none;" class="pages" id="pages">
    <div class="ui-paginate"><span class="p-prev disabled p-elem"></span><span class="current p-elem">1</span><span class="p-next disabled p-elem"></span></div></section>
    <section class="recommend" id="recommend">
        <div class="recommend-hd">
            <h2 class="mod-title">为您推荐</h2>
            <div class="mod-control" id="recommendDirectionNav">
                <a tabindex="-1" class="vm-prev flex-prev flex-disabled" data-mtype="store_list_tj_a" href="#" id="J_recommendPrev">
                    <i> </i>
                </a>
                <a class="vm-next flex-next" data-mtype="store_list_tj_b" href="#" id="J_recommendNext">
                    <i></i>
                </a>
            </div>
        </div>
        <div class="recommend-slider" id="J_recommendSlider">
            
        <div style="overflow: hidden; position: relative;" class="flex-viewport"><ul style="width: 2000%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);" class="recommend-slider-wrap">
                        <li style="width: 245.6px; margin-right: 5px; float: left; display: block;" class="rs-item">
                            <a class="rs-item-wrap" data-mtype="sotre_list_tj_1" href="http://detail.meizu.com/item/meizu_pro6.html" target="_top">
                                <div class="mod-pic">
                                    <img draggable="false" src="images/1460440311-70719.png" height="180" width="180">
                                </div>
                                <div class="mod-desc">
                                    <h4 class="vm-title">魅族PRO 6 </h4>
                                    <h6 class="vm-subtitle">12免息 月供低至191.59元</h6>
                                    <p class="vm-price">
                                            <em>¥2499</em>
                                        ¥<span>2299</span>
                                    </p>
                                </div>
                                    <span class="mod-sign" style="background-color: rgb(240, 65, 95);">
                                         特惠
                                    </span>
                            </a>
                        </li>
                        <li style="width: 245.6px; margin-right: 5px; float: left; display: block;" class="rs-item">
                            <a class="rs-item-wrap" data-mtype="sotre_list_tj_2" href="http://detail.meizu.com/item/meilan3s.html" target="_top">
                                <div class="mod-pic">
                                    <img draggable="false" src="images/1465869600-76415.png" height="180" width="180">
                                </div>
                                <div class="mod-desc">
                                    <h4 class="vm-title">魅蓝3s </h4>
                                    <h6 class="vm-subtitle">轻巧真金属 指纹全网通</h6>
                                    <p class="vm-price">
                                        ¥<span>699</span>
                                    </p>
                                </div>
                            </a>
                        </li>
                        <li style="width: 245.6px; margin-right: 5px; float: left; display: block;" class="rs-item">
                            <a class="rs-item-wrap" data-mtype="sotre_list_tj_3" href="http://detail.meizu.com/item/ROMOSS_A10.html" target="_top">
                                <div class="mod-pic">
                                    <img draggable="false" src="images/1468070888-40234.png" height="180" width="180">
                                </div>
                                <div class="mod-desc">
                                    <h4 class="vm-title">罗马仕AC10快充适配器 </h4>
                                    <h6 class="vm-subtitle">支持MTK快充模式</h6>
                                    <p class="vm-price">
                                        ¥<span>36</span>
                                    </p>
                                </div>
                            </a>
                        </li>
                        <li style="width: 245.6px; margin-right: 5px; float: left; display: block;" class="rs-item">
                            <a class="rs-item-wrap" data-mtype="sotre_list_tj_4" href="http://detail.meizu.com/item/NM_VR01.html" target="_top">
                                <div class="mod-pic">
                                    <img draggable="false" src="images/1459478957-78823.png" height="180" width="180">
                                </div>
                                <div class="mod-desc">
                                    <h4 class="vm-title">纽曼VR智能3D眼镜 </h4>
                                    <h6 class="vm-subtitle">NM-VR01</h6>
                                    <p class="vm-price">
                                        ¥<span>168</span>
                                    </p>
                                </div>
                            </a>
                        </li>
                        <li style="width: 245.6px; margin-right: 5px; float: left; display: block;" class="rs-item">
                            <a class="rs-item-wrap" data-mtype="sotre_list_tj_5" href="http://detail.meizu.com/item/XunLei_TB01.html" target="_top">
                                <div class="mod-pic">
                                    <img draggable="false" src="images/cnqojvfh3quahla6aahcswlv3os130.png" height="180" width="180">
                                </div>
                                <div class="mod-desc">
                                    <h4 class="vm-title">迅雷下载宝 TB01 </h4>
                                    <h6 class="vm-subtitle">路由器伴侣 追剧不等待 智能设备</h6>
                                    <p class="vm-price">
                                        ¥<span>99</span>
                                    </p>
                                </div>
                            </a>
                        </li>
                        <li style="width: 245.6px; margin-right: 5px; float: left; display: block;" class="rs-item">
                            <a class="rs-item-wrap" data-mtype="sotre_list_tj_6" href="http://detail.meizu.com/item/mx6.html?skuid=1870" target="_top">
                                <div class="mod-pic">
                                    <img draggable="false" src="images/cix_s1envciafnfyaa9ipq8-shg689.png" height="180" width="180">
                                </div>
                                <div class="mod-desc">
                                    <h4 class="vm-title">魅族 MX6 </h4>
                                    <h6 class="vm-subtitle">12期免息 赠智能保护套</h6>
                                    <p class="vm-price">
                                        ¥<span>1999</span>
                                    </p>
                                </div>
                                    <span class="mod-sign" style="background-color: rgb(0, 175, 190);">
                                         赠品
                                    </span>
                            </a>
                        </li>
                        <li style="width: 245.6px; margin-right: 5px; float: left; display: block;" class="rs-item">
                            <a class="rs-item-wrap" data-mtype="sotre_list_tj_7" href="http://detail.meizu.com/item/selfie_stick.html" target="_top">
                                <div class="mod-pic">
                                    <img draggable="false" src="images/cix_s1fgc5yajhrlaaiydzvwfak306.png" height="180" width="180">
                                </div>
                                <div class="mod-desc">
                                    <h4 class="vm-title">魅族 线控自拍杆 </h4>
                                    <h6 class="vm-subtitle"></h6>
                                    <p class="vm-price">
                                        ¥<span>69</span>
                                    </p>
                                </div>
                            </a>
                        </li>
                        <li style="width: 245.6px; margin-right: 5px; float: left; display: block;" class="rs-item">
                            <a class="rs-item-wrap" data-mtype="sotre_list_tj_8" href="http://detail.meizu.com/item/beats_urbeats.html" target="_top">
                                <div class="mod-pic">
                                    <img draggable="false" src="images/1455787917-60952.png" height="180" width="180">
                                </div>
                                <div class="mod-desc">
                                    <h4 class="vm-title">Beats Urbeats 入耳耳机 </h4>
                                    <h6 class="vm-subtitle">强韧经久 声而震撼</h6>
                                    <p class="vm-price">
                                        ¥<span>558</span>
                                    </p>
                                </div>
                            </a>
                        </li>
                        <li style="width: 245.6px; margin-right: 5px; float: left; display: block;" class="rs-item">
                            <a class="rs-item-wrap" data-mtype="sotre_list_tj_9" href="http://detail.meizu.com/item/SanDisk_CA10SD80S.html" target="_top">
                                <div class="mod-pic">
                                    <img draggable="false" src="images/1458639317-50797.png" height="180" width="180">
                                </div>
                                <div class="mod-desc">
                                    <h4 class="vm-title">闪迪存储卡Class10 读速80Mb/s </h4>
                                    <h6 class="vm-subtitle">高性能畅快存储无极限</h6>
                                    <p class="vm-price">
                                        ¥<span>39.9</span>
                                    </p>
                                </div>
                            </a>
                        </li>
                        <li style="width: 245.6px; margin-right: 5px; float: left; display: block;" class="rs-item">
                            <a class="rs-item-wrap" data-mtype="sotre_list_tj_10" href="http://detail.meizu.com/item/lexin_mambo_HR.html" target="_top">
                                <div class="mod-pic">
                                    <img draggable="false" src="images/1463117434-82105.png" height="180" width="180">
                                </div>
                                <div class="mod-desc">
                                    <h4 class="vm-title">乐心运动手环mambo HR </h4>
                                    <h6 class="vm-subtitle">心率检测 震动亮屏</h6>
                                    <p class="vm-price">
                                        ¥<span>169</span>
                                    </p>
                                </div>
                            </a>
                        </li>
            </ul></div></div>
    </section>
    </div>
</main>



<div class="site-footer">
    <div class="mzcontainer">
        <div class="site-footer-service">
            <ul class="clearfix">
                <li class="service-item">
                  <span class="service-icon service-icon-seven"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">7天</span>
                    <span class="service-desc-normal">无理由退货</span>
                  </p>
                </li>
                <li class="service-split-line">
                  <span></span>
                </li>
                <li class="service-item">
                  <span class="service-icon service-icon-fifty"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">15天</span>
                    <span class="service-desc-normal">换货保障</span>
                  </p>
                </li>
                <li class="service-split-line">
                  <span></span>
                </li>
                <li class="service-item">
                  <span class="service-icon service-icon-one"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">1年</span>
                    <span class="service-desc-normal">免费保修</span>
                  </p>
                </li>
                <li class="service-split-line">
                  <span></span>
                </li>
                <li class="service-item">
                  <span class="service-icon service-icon-speed"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">百城</span>
                    <span class="service-desc-normal">速达</span>
                  </p>
                </li>
                <li class="service-split-line">
                  <span></span>
                </li>
                <li class="service-item">
                  <span class="service-icon service-icon-by"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">全场</span>
                    <span class="service-desc-normal">包邮</span>
                  </p>
                </li>
                <li class="service-split-line">
                  <span></span>
                </li>
                <li class="service-item">
                  <span class="service-icon service-icon-map"></span>
                  <p class="service-desc">
                    <span class="service-desc-bold">2000多家</span>
                    <span class="service-desc-normal">专卖店</span>
                  </p>
                </li>
            </ul>
        </div>
        <div class="site-footer-navs clearfix">
            <div class="footer-nav-item">
                <h4 class="footer-nav-title">帮助说明</h4>
                <ul>
                    <li><a href="http://store.meizu.com/explain/payment_help.html" target="_top">支付方式</a></li>
                    <li><a href="http://store.meizu.com/explain/deliverynote.html" target="_top">配送说明</a></li>
                    <li><a href="http://store.meizu.com/explain/warranty_services.html" target="_top">售后服务</a></li>
                    <li><a href="http://store.meizu.com/explain/payment_helps.html" target="_top">付款帮助</a></li>
                </ul>
            </div>
            <div class="footer-nav-item">
                <h4 class="footer-nav-title">Flyme</h4>
                <ul>
                    <li><a target="_top" href="https://open.flyme.cn/">开放平台</a></li>
                    <li><a target="_top" href="http://www.flyme.cn/firmware.jsp">固件下载</a></li>
                    <li><a target="_top" href="http://app.flyme.cn/">软件商店</a></li>
                    <li><a target="_top" href="http://finder.flyme.cn/">查找手机</a></li>
                </ul>
            </div>
            <div class="footer-nav-item">
                <h4 class="footer-nav-title">关于我们</h4>
                <ul>
                    <li><a target="_top" href="http://www.meizu.com/about.html">关于魅族</a></li>
                    <li><a target="_top" href="http://hr.meizu.com/">加入我们</a></li>
                    <li><a target="_top" href="http://www.meizu.com/contact.html">联系我们</a></li>
                    <li><a target="_top" href="http://www.meizu.com/legalStatement.html">法律声明</a></li>
                </ul>
            </div>
            <div class="footer-nav-item">
                <h4 class="footer-nav-title">关注我们</h4>
                <ul>
                    <li><a target="_top" href="http://weibo.com/meizumobile">新浪微博</a></li>
                    <li><a target="_top" href="http://t.qq.com/meizu_tech">腾讯微博</a></li>
                    <li><a target="_top" href="http://user.qzone.qq.com/2762957059">QQ空间</a></li>
                    <li>
                        <a class="meizu-footer-wechat">
                            官方微信
                            <img src="images/weixin.png" alt="微信二维码">
                        </a>
                    </li>
                </ul>
            </div>
            <div class="footer-nav-custom">
                <h4 class="nav-custom-title">24小时全国服务热线</h4>
                <a href="tel:400-788-3333"><h3 class="nav-custom-number">400-788-3333</h3></a>
                <a class="nav-custom-btn" href="http://live-i.meizu.com/live800/chatClient/chatbox.jsp?companyID=8957&amp;configID=4&amp;enterurl=">
                    <img src="images/20x21xiaoshi.png" height="21" width="20">
                    24小时在线客服
                </a>
            </div>
        </div>
        <div class="site-footer-end">
            <p>
                ©2016 Meizu Telecom Equipment Co., Ltd. All rights reserved.
                <a target="_top" href="http://www.miitbeian.gov.cn/" hidefocus="true">备案号：粤ICP备13003602号-2</a>
                <a target="_top" href="http://www2.res.meizu.com/zh_cn/images/common/icp2.jpg" hidefocus="true">经营许可证编号：粤B2-20130198</a>
                <a target="_top" href="http://www2.res.meizu.com/zh_cn/images/common/com_licence.jpg" hidefocus="true">营业执照</a>
                <a target="_top" rel="nofollow" href="http://210.76.65.188/" hidefocus="true">
                    <img src="images/footer-copy-1.png">
                </a>
                <a target="_top" rel="nofollow" href="http://210.76.65.188/webrecord/innernet/Welcome.jsp?bano=4404013010531" hidefocus="true">
                    <img src="images/footer-copy-2.png">
                </a>
                <a target="_top" rel="nofollow" href="https://credit.szfw.org/CX20151204012550820380.html" hidefocus="true">
                    <img src="images/trust-icon.png">
                </a>
            </p>
        </div>
    </div>
</div>












    
    
<div class="site-gotop" id="siteGotop"><a class="gotop-suggest" title="建议反馈" href="http://me.meizu.com/member/advice/index" target="_top"></a><div class="gotop-arrow" title="回到顶部"></div></div></body>
</html>
